package com.aham.patientsignoutapp.component;

import java.util.Date;

public interface DeletableItem {
	public boolean isDeleted();
	public void setDeleted(boolean deleted);
	public Date getDeleteDate();
	public void setDeleteDate(Date deleteDate);
}
