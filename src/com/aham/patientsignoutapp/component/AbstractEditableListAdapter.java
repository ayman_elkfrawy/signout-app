package com.aham.patientsignoutapp.component;

import java.util.List;


import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.activity.signoutpage.LabHistoryDialog;
import com.aham.patientsignoutapp.activity.signoutpage.SignoutActivity;
import com.aham.patientsignoutapp.util.Utility;
import com.aham.signout.shared.messaging.Lab;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public abstract class AbstractEditableListAdapter<T> extends ArrayAdapter<T> implements OnClickListener,
		OnItemClickListener, OnItemLongClickListener {

	List<T> mItems;
	EditableListView mEditableListView;
	Context mContext;
	int mSelectedItem;

	public AbstractEditableListAdapter(Context context, List<T> items, EditableListView editableListView) {
		super(context, R.layout.simple_list_item);

		setNotifyOnChange(true);

		mItems = items;
		mEditableListView = editableListView;
		mContext = context;

		getAddButtonView().setOnClickListener(this);

		mEditableListView.getListView().setOnItemClickListener(this);
		mEditableListView.getListView().setOnItemLongClickListener(this);

		editableListView.setAdapter(this);
	}

	public List<T> getListItems() {
		return mItems;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.simple_list_item, parent, false);
		((TextView) rowView.findViewById(R.id.tv_List_item)).setText(getItem(position).toString());
		return rowView;
	}

	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
		Utility.setListViewHeightBasedOnChildren(mEditableListView);
	}

	public Button getAddButtonView() {
		return mEditableListView.getAddButtonView();
	}

	public TextView getTitleView() {
		return mEditableListView.getTitleView();
	}

	public ListView getListView() {
		return mEditableListView.getListView();
	}

	public int getSelectedItem() {
		return mSelectedItem;
	}

	public abstract void addItem();

	public abstract void editItem();

	public abstract void removeItem();

	@Override
	public void onClick(View v) {
		if (v.getId() == getAddButtonView().getId()) {
			addItem();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
		mSelectedItem = pos;
		T item = getItem(mSelectedItem);
		if (item != null && item instanceof Lab && mContext instanceof SignoutActivity) {
			LabHistoryDialog historyDialog = LabHistoryDialog.newInstance(((Lab)item).getGroup());
			historyDialog.show( ((SignoutActivity) mContext).getSupportFragmentManager() , "labHistory");
		}
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
		mSelectedItem = pos;
		// Show context menu
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
				android.R.layout.select_dialog_singlechoice);
		adapter.add("Edit");
		adapter.add("Remove");
		AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
		builder.setTitle("Please select an action");
		builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				if (item == 0)
					editItem();
				else
					removeItem();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
		return false;
	}

	/************************* Overrides for the adapter functions *****************/

	@Override
	public int getCount() {
		int count = 0;
		if (isDeletableItems()) {
			for (T item : mItems) {
				if (!((DeletableItem) item).isDeleted()) {
					count++;
				}
			}
		} else
			count = mItems.size();

		return count;
	}

	@Override
	public T getItem(int position) {
		if (isDeletableItems()) {
			int tempPos = 0;
			for (T item : mItems) {
				if (tempPos == position) {
					if (!((DeletableItem) item).isDeleted())
						return item;
					else continue;
				}
				if (!((DeletableItem) item).isDeleted()) {
					tempPos++;
				}
			}
		} else
			return mItems.get(position);
		return null;
	}
	
	@Override
	public void add(T object) {
		mItems.add(object);
	}
	
	public void remove(int posision) {
		mItems.remove(posision);
	}
	
	public void replace(int pos, T item) {
		if(item instanceof Replaceable) {
			((Replaceable) getItem(pos)).replace(item);
		} else {
			mItems.set(pos, item);
		}
	}
	
	public boolean isDeletableItems() {
		return mItems != null && mItems.size() > 0 && mItems.get(0) instanceof DeletableItem;
	}
}
