package com.aham.patientsignoutapp.component;


import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.util.Utility;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class EditableListView extends LinearLayout {

	private Button mAddItem;
	private TextView mHeaderTitle;
	private ListView mListItems;

	public EditableListView(Context context, AttributeSet attrs) {
		super(context, attrs);

		this.setOrientation(HORIZONTAL);

		LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(R.layout.editable_list_view, this);

		this.mAddItem = (Button) findViewById(R.id.but_add_item);
		this.mListItems = (ListView) findViewById(R.id.lv_editable_list_view);
		this.mHeaderTitle = (TextView) findViewById(R.id.tv_list_title);
		
		// Get attribute
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.EditableListView);
		CharSequence headerString = a.getString(R.styleable.EditableListView_header_string);
		if (headerString != null) {
			mHeaderTitle.setText(headerString);
		}
		a.recycle();
	}
	
	public Button getAddButtonView() {
		return mAddItem;
	}
	
	public TextView getTitleView() {
		return mHeaderTitle;
	}
	
	public ListView getListView() {
		return mListItems;
	}
	
	public void setAdapter(AbstractEditableListAdapter<? extends Object> listAdapter) {
		getListView().setAdapter(listAdapter);
		Utility.setListViewHeightBasedOnChildren(this);
	}
}
