package com.aham.patientsignoutapp.component;

import com.aham.patientsignoutapp.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public abstract class EditItemDialog<T> extends DialogFragment {

	T mItem;
	boolean mAddNew;
	ResultReciever<T> mResultReciever;

	public static String ADD_EDIT = "add";

	public void setResultReciever(ResultReciever<T> resultReciever) {
		mResultReciever = resultReciever;
	}

	public ResultReciever<T> getResultReciever() {
		return mResultReciever;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		View dialoglayout = getContentView(mItem);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(dialoglayout);

		builder.setPositiveButton(mAddNew ? R.string.add : R.string.edit, null);
		builder.setNegativeButton(R.string.cancel, null);
		final AlertDialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);
		// Fix for not to dismiss dialog if there are invalid input
		dialog.setOnShowListener(new OnShowListener() {
			@Override
			public void onShow(DialogInterface d) {
				Button b = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
				b.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// Edit or Add been clicked
						T item = generateItem();
						if (item != null) { // Valid item
							mItem = item;
							deliverResult();
							dismiss();
						}

					}
				});
			}
		});
		return dialog;
	}

	public boolean isAdd() {
		return mAddNew;
	}

	public void setAddNew(Boolean isNew) {
		mAddNew = isNew;
	}

	public void setItem(T item) {
		mItem = item;
	}

	public T getItem() {
		return mItem;
	}

	public void deliverResult() {
		mResultReciever.onRecieverResult(mItem);
	}

	public abstract View getContentView(T item);

	public abstract T generateItem();

}
