package com.aham.patientsignoutapp.component;

public interface Replaceable {
	public void replace(Object object);
}
