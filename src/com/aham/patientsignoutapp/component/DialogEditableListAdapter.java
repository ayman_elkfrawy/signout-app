package com.aham.patientsignoutapp.component;

import java.util.Date;
import java.util.List;

import android.content.Context;
import android.support.v4.app.FragmentActivity;

public class DialogEditableListAdapter<T> extends AbstractEditableListAdapter<T> implements ResultReciever<T> {

	EditItemDialog<T> mDialog;
	boolean mAddingItem;

	public DialogEditableListAdapter(Context context, List<T> items, EditableListView editableListView,
			EditItemDialog<T> dialog) {
		super(context, items, editableListView);
		mDialog = dialog;
		mDialog.setResultReciever(this);
	}

	@Override
	public void addItem() {
		// Show the dialog
		mAddingItem = true;
		mDialog.setAddNew(mAddingItem);
		mDialog.setItem(null);
		mDialog.show(((FragmentActivity) getContext()).getSupportFragmentManager(), "dialog");
	}

	@Override
	public void editItem() {
		// Show the dialog
		mAddingItem = false;
		mDialog.setAddNew(mAddingItem);
		mDialog.setItem(getItem(getSelectedItem()));
		mDialog.show(((FragmentActivity) getContext()).getSupportFragmentManager(), "dialog");
	}

	@Override
	public void removeItem() {
		T item = getItem(getSelectedItem());
		if (item instanceof DeletableItem) {
			((DeletableItem) item).setDeleted(true);
			((DeletableItem) item).setDeleteDate(new Date());
		} else
			remove(getSelectedItem());
		notifyDataSetChanged();
	}

	@Override
	public void onRecieverResult(T resultItem) {
		if (mAddingItem) {
			// Add the item
			add(resultItem);
		} else {
			// Edit a current item
			replace(getSelectedItem(), resultItem);
		}
		notifyDataSetChanged();
	}

}
