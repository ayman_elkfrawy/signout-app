package com.aham.patientsignoutapp.labgroup;

import com.aham.patientsignoutapp.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class BMPLabLayout extends RelativeLayout {

    private LinearLayout mFirstLinear;
    private LinearLayout mSecondLinear;
    private LinearLayout mLastLinear;

    private EditText mUpperEditText;
    private EditText mLowerEditText;
    private Paint mPaint;

    public BMPLabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mPaint = new Paint();
        mPaint.setColor(Color.RED);
        mPaint.setStrokeWidth(2.0f);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mFirstLinear = (LinearLayout) findViewById(R.id.et21wrapper);
        mSecondLinear = (LinearLayout) findViewById(R.id.et43wrapper);
        mLastLinear = (LinearLayout) findViewById(R.id.et65wrapper);
        mUpperEditText = (EditText) findViewById(R.id.et_bmp_ca);
        mLowerEditText = (EditText) findViewById(R.id.et_bmp_mg);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        final int leftFirst = mFirstLinear.getLeft();
        final int topFirst = mFirstLinear.getTop();
        final int middleFirst = (mFirstLinear.getBottom() - mFirstLinear
                .getTop()) / 2;
        final int lastRight = mLastLinear.getRight();
        // Long horizontal line
        canvas.drawLine(leftFirst, topFirst + middleFirst, lastRight, topFirst
                + middleFirst, mPaint);
        final int rightFirst = mFirstLinear.getRight();
        final int bottomFirst = mFirstLinear.getBottom();
        
        final int margin = (mSecondLinear.getLeft() - leftFirst - mFirstLinear.getWidth())/2;
        // First vertical line
        canvas.drawLine(rightFirst+margin, topFirst, rightFirst+margin, bottomFirst, mPaint);

        // Second vertical line
        final int rightSecond = mSecondLinear.getRight();
        canvas.drawLine(rightSecond+margin, topFirst, rightSecond+margin, bottomFirst, mPaint);

        // The upper diagonal line
        final int leftUpperEdit = mUpperEditText.getLeft();
        final int topUpperEdit = mUpperEditText.getTop();
        final int middleUpperEdit = (mUpperEditText.getBottom() - mUpperEditText
                .getTop()) / 2;
        canvas.drawLine(lastRight, topFirst + middleFirst, leftUpperEdit,
                topUpperEdit + middleUpperEdit, mPaint);

        // The lower diagonal line
        final int leftLowerEdit = mLowerEditText.getLeft();
        final int topLowerEdit = mLowerEditText.getTop();
        final int middleLowerEdit = (mLowerEditText.getBottom() - mLowerEditText
                .getTop()) / 2;
        canvas.drawLine(lastRight, topFirst + middleFirst, leftLowerEdit, topLowerEdit
                + middleLowerEdit, mPaint);
    }

}