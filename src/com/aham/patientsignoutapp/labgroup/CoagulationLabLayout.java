package com.aham.patientsignoutapp.labgroup;

import com.aham.patientsignoutapp.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.RelativeLayout;

// com.example.signoutprototype.CoagulationLabLayout
public class CoagulationLabLayout extends RelativeLayout {

	private EditText mLeftEditText;
	private EditText mRightEditText;
	private EditText mBottomEditText;
	private Paint mPaint;

	public CoagulationLabLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		mPaint = new Paint();
		mPaint.setColor(Color.RED);
		mPaint.setStrokeWidth(2.0f);
	}

	@Override
	protected void onFinishInflate() {
		mLeftEditText = (EditText) findViewById(R.id.et_coag_pt);
		mRightEditText = (EditText) findViewById(R.id.et_coag_ptt);
		mBottomEditText = (EditText) findViewById(R.id.et_coag_inr);
	}

	@Override
	protected void dispatchDraw(Canvas canvas) {
		super.dispatchDraw(canvas);
		int margin = (mRightEditText.getLeft() - mLeftEditText.getRight()) / 2;
		int halfWidth = (mRightEditText.getLeft() - mRightEditText.getRight()) / 2;
		
		
		canvas.drawLine(mLeftEditText.getRight() + margin,
				mLeftEditText.getTop() - margin, mLeftEditText.getRight() + margin,
				mLeftEditText.getBottom(), mPaint);

		canvas.drawLine(mLeftEditText.getRight() + margin,
				mLeftEditText.getBottom(), mLeftEditText.getLeft() - halfWidth,
				mBottomEditText.getBottom(), mPaint);

		canvas.drawLine(mLeftEditText.getRight() + margin,
				mLeftEditText.getBottom(), mRightEditText.getRight() + halfWidth,
				mBottomEditText.getBottom(), mPaint);
	}

}