package com.aham.patientsignoutapp.labgroup;

import com.aham.patientsignoutapp.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class CBCLabLayout extends RelativeLayout {

    private View mLinear;

    private View mUpperEditText;
    private View mLowerEditText;
    private Paint mPaint;

    public CBCLabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mPaint = new Paint();
        mPaint.setColor(Color.RED);
        mPaint.setStrokeWidth(2.0f);
    }
    
    @Override
    protected void onFinishInflate() {
    	mLinear = findViewById(R.id.etWrapper);
        mUpperEditText =  findViewById(R.id.et_cbc_mcv);
        mLowerEditText =  findViewById(R.id.et_cbc_rdw);
    }


    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        final int leftFirst = mLinear.getLeft();
        final int topFirst = mLinear.getTop();
        final int middleFirst = (mLinear.getBottom() - mLinear
                .getTop()) / 2;
        final int rightFirst = mLinear.getRight();
        // Long horizontal line
        canvas.drawLine(leftFirst, topFirst + middleFirst, rightFirst, topFirst
                + middleFirst, mPaint);
        
        final int leftUpperEdit = mUpperEditText.getLeft();
        final int topUpperEdit = mUpperEditText.getTop();
        final int middleUpperEdit = (mUpperEditText.getBottom() - mUpperEditText
                .getTop()) / 2;
        
        canvas.drawLine(rightFirst, topFirst + middleFirst, leftUpperEdit,
                topUpperEdit + middleUpperEdit, mPaint);
        
        // Left diagonal line
        canvas.drawLine(leftFirst, topFirst + middleFirst, leftFirst - leftUpperEdit+rightFirst,
                topUpperEdit + middleUpperEdit, mPaint);

        final int leftLowerEdit = mLowerEditText.getLeft();
        final int topLowerEdit = mLowerEditText.getTop();
        final int middleLowerEdit = (mLowerEditText.getBottom() - mLowerEditText
                .getTop()) / 2;
        canvas.drawLine(rightFirst, topFirst + middleFirst, leftLowerEdit, topLowerEdit
                + middleLowerEdit, mPaint);
        canvas.drawLine(leftFirst, topFirst + middleFirst, leftFirst - leftUpperEdit+rightFirst
        		, topLowerEdit + middleLowerEdit , mPaint);
    }

}