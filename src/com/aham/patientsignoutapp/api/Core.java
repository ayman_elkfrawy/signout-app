package com.aham.patientsignoutapp.api;

import java.io.IOException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import com.aham.patientsignoutapp.util.Utility;
import com.aham.signout.shared.messaging.ApplicationVariables;
import com.aham.signout.shared.messaging.CoverPage;
import com.aham.signout.shared.messaging.Lab;
import com.aham.signout.shared.messaging.LabJSONAdapter;
import com.aham.signout.shared.messaging.Patient;
import com.aham.signout.shared.messaging.SignoutPage;
import com.aham.signout.shared.messaging.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

public class Core {

	 final static String BASE_URL = "https://emedsol.net:8443/signout-test/rest/";
	// final static String BASE_URL =
	// "http://emedsol.net:8080/SignOutServer/rest/";
//	final static String BASE_URL = "http://10.0.2.2:8080/com.aham.signout.server/rest/";

	static String mUsername;
	static String mPassword;
	
	static DefaultHttpClient mClient;

	static Gson gson = null;
	static {
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(Lab.class, new LabJSONAdapter());
		gson = builder.create();
	}

	public static int addEditUser(User user) {
		if (user.getPassword() != null && user.getPassword().length() > 0) {
			user.setPassword(Utility.md5(user.getPassword()));
		}
		String result = Utility.getStringFromResponse(postJSON("user", user));
		return Integer.parseInt(result.replaceAll("\n", ""));
	}

	public static List<User> getListOfUsers() {
		String result = Utility.getStringFromResponse(getWithParam("user/all", null));
		return gson.fromJson(result, new TypeToken<Collection<User>>() {
		}.getType());
	}

	public static NetworkResult<User> loginUser(String username, String password) {
		mUsername = username;
		mPassword = Utility.md5(password);
		mClient = null;
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("password", Utility.md5(password)));
		HttpResponse response = getWithParam("user/" + username, params);
		User result = null;
		try {
			result = gson.fromJson(Utility.getStringFromResponse(response), User.class);
		} catch (JsonSyntaxException e) {
		}
		return new NetworkResult<User>(result, response);
	}

	public static boolean removeUser(String username) {
		// List<NameValuePair> params = new ArrayList<NameValuePair>();
		// params.add(new BasicNameValuePair("username", username));
		HttpResponse response = deleteWithParam("user/" + username, null);
		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
			return true;
		else
			return false;
	}

	public static Patient addEditPatient(Patient patient) {
		String result;
		if (patient.getId() > 0) // Edit
			result = Utility.getStringFromResponse(putJSON("patient", patient));
		else
			result = Utility.getStringFromResponse(postJSON("patient", patient)); // add
		return gson.fromJson(result, Patient.class);
	}

	public static boolean deletePatient(long patientID) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("id", patientID + ""));
		String result = Utility.getStringFromResponse(deleteWithParam("patient", params));
		return result.equalsIgnoreCase("true") ? true : false;
	}

	public static List<Patient> getPatientList(String userName, int index, int count, String filter) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		// params.add(new BasicNameValuePair("username", userName));
		params.add(new BasicNameValuePair("start", index + ""));
		params.add(new BasicNameValuePair("count", count + ""));
		params.add(new BasicNameValuePair("filter", filter));
		String result = Utility.getStringFromResponse(getWithParam("patient", params));
		if (isResponseEmptyList(result))
			return new ArrayList<Patient>(); // Empty list
		else
			return gson.fromJson(result, new TypeToken<Collection<Patient>>() {
			}.getType());
	}

	private static boolean isResponseEmptyList(String result) {
		return result.equals("[]");
	}

	public static CoverPage getCoverPage(long patientID) {
		// List<NameValuePair> params = new ArrayList<NameValuePair>();
		// params.add(new BasicNameValuePair("id", patientID + ""));
		String result = Utility.getStringFromResponse(getWithParam("coverpage/" + patientID, null));
		return gson.fromJson(result, CoverPage.class);
	}

	public static List<Date> getSignoutPagesDays(long patientID) {
		// List<NameValuePair> params = new ArrayList<NameValuePair>();
		// params.add(new BasicNameValuePair("id", patientID + ""));
		String result = Utility.getStringFromResponse(getWithParam("signoutpage/" + patientID, null));
		return gson.fromJson(result, new TypeToken<Collection<Date>>() {
		}.getType());
	}

	public static SignoutPage getSignoutPage(long patientID, Date signoutPageDate) {
		// List<NameValuePair> params = new ArrayList<NameValuePair>();
		// params.add(new BasicNameValuePair("id", patientID + ""));
		// params.add(new BasicNameValuePair("date",
		// signoutPageDate.toString()));
		String result = Utility.getStringFromResponse(getWithParam("signoutpage/" + patientID + "/"
				+ new Gson().toJson(signoutPageDate).replaceAll("\"", "").replaceAll(" ", "%20"), null));
		return gson.fromJson(result, SignoutPage.class);
	}

	public static SignoutPage saveSignoutPage(long patientID, SignoutPage signoutPage) {
		String result = Utility.getStringFromResponse(postJSON("signoutpage/" + patientID, signoutPage));
		return gson.fromJson(result, SignoutPage.class);
	}

	public static ApplicationVariables getApplicationVariables() {
		String result = Utility.getStringFromResponse(getWithParam("appvariables", null));
		return gson.fromJson(result, ApplicationVariables.class);
	}

	public static List<Lab> getLabHistory(long patientID, String requiredLab) {
		String result = Utility.getStringFromResponse(getWithParam("patient/" + patientID + "/lab/" + requiredLab, null));
		return gson.fromJson(result, Lab.getLabTypeCollection(requiredLab));
	}

	private static HttpResponse postJSON(String serviceDir, Object object) {
		HttpPost request = new HttpPost(BASE_URL + serviceDir);
		request.setHeader("Content-type", "application/json");
		HttpResponse response = null;
		try {
			request.setEntity(new StringEntity(gson.toJson(object)));
			response = getClient().execute(request);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}

	private static HttpResponse putJSON(String serviceDir, Object object) {
		HttpPut request = new HttpPut(BASE_URL + serviceDir);
		request.setHeader("Content-type", "application/json");
		HttpResponse response = null;
		try {
			request.setEntity(new StringEntity(gson.toJson(object)));
			response = getClient().execute(request);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}

	private static HttpResponse getWithParam(String serviceDir, List<NameValuePair> params) {
		String paramString = "";
		if (params != null && params.size() > 0) {
			paramString = "?" + URLEncodedUtils.format(params, "utf-8");
		}
		HttpGet request = new HttpGet(BASE_URL + serviceDir + "/" + paramString);
		HttpResponse response = null;
		try {
			response = getClient().execute(request);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}

	private static HttpResponse deleteWithParam(String serviceDir, List<NameValuePair> params) {
		HttpClient client = new DefaultHttpClient();
		String paramString = "";
		if (params != null && params.size() > 0) {
			paramString = "?" + URLEncodedUtils.format(params, "utf-8");
		}
		HttpDelete request = new HttpDelete(BASE_URL + serviceDir + "/" + paramString);
		HttpResponse response = null;
		try {
			response = client.execute(request);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}

	public static HttpClient getClient() {
		if (mClient != null) return mClient;
		try {
			// Ignore certification
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);

			SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			registry.register(new Scheme("https", sf, 443));

			ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

			mClient = new DefaultHttpClient(ccm, params);
		} catch (Exception e) {
			mClient = new DefaultHttpClient();
		}

		if (mClient != null) {
			mClient.getCredentialsProvider().setCredentials(new AuthScope(null, -1),
					new UsernamePasswordCredentials(mUsername, mPassword));
		}
		return mClient;
	}
}
