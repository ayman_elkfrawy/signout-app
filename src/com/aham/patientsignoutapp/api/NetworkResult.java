package com.aham.patientsignoutapp.api;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;

public class NetworkResult <T> {
	T mResult;
	HttpResponse mResponse;
	
	public NetworkResult() {}

	public NetworkResult(T result, HttpResponse response) {
		mResult = result;
		mResponse = response;
	}
	
	public T getResult() {
		return mResult;
	}
	
	public void setResult(T result) {
		mResult = result;
	}
	
	public HttpResponse getResponse() {
		return mResponse;
	}

	public void setResponse(HttpResponse response) {
		mResponse = response;
	}
	
	public int getStatusCode() {
		return mResponse != null? mResponse.getStatusLine().getStatusCode() : -1 ;
	}
	
	public boolean isOKResponse() {
		return mResponse != null && mResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK;
	}
}
