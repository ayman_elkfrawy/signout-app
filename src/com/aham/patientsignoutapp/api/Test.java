package com.aham.patientsignoutapp.api;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.aham.signout.shared.messaging.CoverPage;
import com.aham.signout.shared.messaging.Patient;
import com.aham.signout.shared.messaging.PatientBasic;
import com.aham.signout.shared.messaging.Radiology;
import com.aham.signout.shared.messaging.SignoutPage;
import com.aham.signout.shared.messaging.SimpleListItem;


public class Test {
	
	public static void main(String[] args) {
		
	}
	
	public static Patient getNewPatient() {
		Patient patient = new Patient();
		List<SignoutPage> signoutPages = new ArrayList<SignoutPage>();
		
		patient.setPatientBasicInfo(getPatientBasic());
		patient.setCoverPage(null);
		patient.setSignoutPages(signoutPages);
		
		return patient;
	}
	
	public static PatientBasic getPatientBasic() {
		PatientBasic patientBasicInfo = new PatientBasic();
		patientBasicInfo.setAdmittedFor(getListFromString("Ayman#Ahmed#Mohamed", SimpleListItem.class));
		patientBasicInfo.setAdmittingDiagnosis(getListFromString("Diag1#Diag2#Diag3#Diag4", String.class));
		patientBasicInfo.setAllergies(getListFromString("Cats#Milk", String.class));
		patientBasicInfo.setBirthDate(new Date());
		patientBasicInfo.setBp1(55.54f);
		patientBasicInfo.setBp2(43.44f);
		patientBasicInfo.setEdCourse("ED course, bla bla");
		patientBasicInfo.setFirstName("Donald");
		patientBasicInfo.setGender("Male");
		patientBasicInfo.setHeadCircumference(12.4f);
		patientBasicInfo.setHeight(165f);
		patientBasicInfo.setHomeMedications(getListFromString("Home Med1#Home Med2#Home Med3", String.class));
		// patientBasicInfo.setHostingDate(new Date()); // Should it be automatic or manually?
		patientBasicInfo.setHr(2.53f);
		patientBasicInfo.setLastName("Adams");
		// patientBasicInfo.setMiddleName(""); // Test it as null
		patientBasicInfo.setO2sat(64.54f);
		patientBasicInfo.setPmh(getListFromString("PMH1#PMH2#PMH3#PMH4", String.class));
		patientBasicInfo.setPw(getListFromString("PMH1#PMH2#PMH3#PMH4", String.class));
		patientBasicInfo.setTemp(32.4f);
		patientBasicInfo.setWeight(70.3f);
		patientBasicInfo.setAttendingOnService("AttendingOnService 1");
		patientBasicInfo.setPmd("PMD 1");
		
		return patientBasicInfo;
	}
	
	public static CoverPage getCoverPage() {
		CoverPage coverPage = new CoverPage();
		coverPage.setHosbitalCourse("Hosbital Course Bla bla bla");
		coverPage.setContingencyPlan(getListFromString("ContingencyPlan 1#ContingencyPlan 2",SimpleListItem.class));
		coverPage.setOnEvents(getListFromString("Event 1#Event 2#Event 3",SimpleListItem.class));
		coverPage.setProblems(getListFromString("Problem 1#Problem 2#Problem 3",SimpleListItem.class));
		coverPage.setToDoList(getListFromString("TODO 1#TODO 2#TODO 3#TODO 4",SimpleListItem.class));
		
		return coverPage;
	}
	
	public static SignoutPage getSignoutPage() {
		SignoutPage signout = new SignoutPage();
		// TODO
		
		return signout;
	}
	
	//********************  Helper Method ***********************
	public static <T> List<T> getListFromString(String string, Class<T> ListType) {
		List<T> list = new ArrayList<T>();
		
		if (ListType.equals(String.class)) {
			for (String token : string.split("#")) 
				list.add((T) token);
		} else if (ListType.equals(SimpleListItem.class)) {
			for (String token : string.split("#")) {
				SimpleListItem item = new SimpleListItem();
				// item.setCreateDate(new Date()); // Check set it auto
				item.setText(token);
				list.add((T) item);
			}
		} else if (ListType.equals(Radiology.class)) {
			for (String token : string.split("#")) {
				Radiology item = new Radiology();
				// Should create create date auto
				item.setDate(new Date()); 
				item.setName(token);
				item.setResult("Result of " + token);
				list.add((T) item);
			}
		}
		return list;
	}
	
}
