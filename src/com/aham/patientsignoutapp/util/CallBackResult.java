package com.aham.patientsignoutapp.util;

public interface CallBackResult<T> {
	public void callBack(T result);
}
