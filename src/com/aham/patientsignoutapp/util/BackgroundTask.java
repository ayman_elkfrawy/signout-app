package com.aham.patientsignoutapp.util;

public interface BackgroundTask<T> {
	public T doInBackground();
}
