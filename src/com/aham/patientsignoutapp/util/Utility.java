package com.aham.patientsignoutapp.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.http.HttpResponse;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.component.EditableListView;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Utility {
	static SimpleDateFormat dateFormater = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

	public static String getStringFromResponse(HttpResponse response) {
		String result = "";
		try {
			InputStream is = response.getEntity().getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}
		return result;
	}

	public static void setListViewHeightBasedOnChildren(EditableListView editableListView) {
		ListView listView = editableListView.getListView();
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			// pre-condition
			return;
		}

		int totalHeight = 0;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(0, 0);
			totalHeight += listItem.getMeasuredHeight();
		}
		if (listAdapter.getCount() == 1)
			totalHeight *= 2;

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		// Check if the height enough to show the add button
		/*
		 * if (totalHeight < editableListView.getAddButtonView().getHeight()) {
		 * params.height = editableListView.getAddButtonView().getHeight(); }
		 * else
		 */
		params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
	}

	public static boolean checkForEmptyInput(View view) {
		boolean emptyInput = false;
		if (view instanceof ViewGroup) {
			ViewGroup vg = (ViewGroup) view;
			for (int i = vg.getChildCount() - 1; i >= 0; i--) {
				if (checkForEmptyInput(vg.getChildAt(i)))
					emptyInput = true;
			}
		} else if (view instanceof EditText) {
			((EditText) view).setError(null);
			if (TextUtils.isEmpty(((EditText) view).getText())) {
				emptyInput = true;
				((EditText) view).setError(view.getContext().getText(R.string.error_field_required));
				view.requestFocus();
			}
		}
		return emptyInput;
	}

	public static int getArrayItemPos(String[] array, String item) {
		int index = -1;
		for (int i = 0; i < array.length; i++) {
			if (array[i].equalsIgnoreCase(item)) {
				index = i;
				break;
			}
		}
		return index;
	}

	public static String formatDate(Date date) {
		return dateFormater.format(date);
	}

	public static Date parseDate(String date) throws ParseException {
		return dateFormater.parse(date);
	}

	public static boolean isToday(Date date) {
		return Utility.formatDate(date).equals(Utility.formatDate(new Date()));
	}

	public static void clearView(View view) {
		if (view instanceof ViewGroup) {
			ViewGroup vg = (ViewGroup) view;
			for (int i = 0; i < vg.getChildCount(); i++) {
				clearView(vg.getChildAt(i));
			}
		} else if (view instanceof EditText) {
			((EditText) view).setText("");
		}
	}

	public static float parseFloat(String string) {
		if (TextUtils.isEmpty(string))
			return 0;
		else
			try {
				return Float.parseFloat(string);
			} catch (Exception e) {
				return 0;
			}
	}

	public static String numberToString(float number) {
		if (number == 0)
			return "";
		else
			return number + "";
	}

	public static int parseInt(String string) {
		if (TextUtils.isEmpty(string))
			return 0;
		else
			try {
				return Integer.parseInt(string);
			} catch (Exception e) {
				return 0;
			}
	}

	public static int calculateAge(Date birthDate, Date date) {
		Calendar dob = Calendar.getInstance();
		dob.setTime(birthDate);
		Calendar today = Calendar.getInstance();
		int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
		if (today.get(Calendar.DAY_OF_YEAR) <= dob.get(Calendar.DAY_OF_YEAR))
			age--;
		return age++;
	}

	/**
	 * function md5 encryption for passwords
	 * 
	 * @param password
	 * @return passwordEncrypted
	 */
	public static final String md5(final String password) {
		try {

			MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
			digest.update(password.getBytes());
			byte messageDigest[] = digest.digest();

			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++) {
				String h = Integer.toHexString(0xFF & messageDigest[i]);
				while (h.length() < 2)
					h = "0" + h;
				hexString.append(h);
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static void assignEditTextDate(final EditText editText, Date date) {
		editText.setText(formatDate(date));
		final Calendar calender = Calendar.getInstance();
		calender.setTime(date);
		editText.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new DatePickerDialog(editText.getContext(), new OnDateSetListener() {
					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
						editText.setText(dayOfMonth + "/" + ++monthOfYear + "/" + year);
						editText.setError(null);
					}
				}, calender.get(Calendar.YEAR), calender.get(Calendar.MONTH), calender.get(Calendar.DAY_OF_MONTH)).show();
			}
		});
	}
	
	public static Dialog getProgressdialog(Activity activity) {
		// Precondition if API >= 11 then return null no need for dialog
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) return null;
			
		ProgressDialog dialog = new ProgressDialog(activity, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		dialog.setMessage("Please wait");
		dialog.show();
		
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		
		lp.copyFrom(dialog.getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		dialog.getWindow().setAttributes(lp);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		return dialog; 
	}

	public static int dpToPx(Context context, int dp) {
		Resources r = context.getResources();
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
	}

}
