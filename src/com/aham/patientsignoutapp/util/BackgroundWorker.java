package com.aham.patientsignoutapp.util;

import android.os.AsyncTask;

public class BackgroundWorker<T> extends AsyncTask<Void, Void, T> {
	
	BackgroundTask<T> mTask;
	CallBackResult<T> mCallBack;
	
	public BackgroundWorker(BackgroundTask<T> task, CallBackResult<T> callBack) {
		mTask = task;
		mCallBack = callBack;
		execute();
	}

	@Override
	protected T doInBackground(Void... params) {
		if(mTask != null) return mTask.doInBackground();
		else return null;
	} 
	
	@Override
	protected void onPostExecute(T result) {
		if(mCallBack != null) mCallBack.callBack(result);
	}	
}
