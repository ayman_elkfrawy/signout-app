package com.aham.patientsignoutapp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aham.patientsignoutapp.activity.signoutpage.SignoutActivity;
import com.aham.patientsignoutapp.api.Core;
import com.aham.patientsignoutapp.util.Utility;
import com.aham.signout.shared.messaging.CoverPage;
import com.aham.signout.shared.messaging.Lab;
import com.aham.signout.shared.messaging.Medication;
import com.aham.signout.shared.messaging.Patient;
import com.aham.signout.shared.messaging.PatientBasic;
import com.aham.signout.shared.messaging.Radiology;
import com.aham.signout.shared.messaging.SignoutPage;
import com.aham.signout.shared.messaging.SimpleListItem;
import com.aham.signout.shared.messaging.User;

public class PatientDelegate {

	Patient mNewPatient;

	SignoutPage mNewSignoutPage;
	SignoutPage mCurrentSignoutPage;

	List<Date> mSignoutPages;

	List<Patient> mPatientsList;

	User loggedUser;

	int mSelectedPatientIndex;

	public List<Patient> getPatientsList() {
		return mPatientsList;
	}

	public void setmPatientsList(List<Patient> mPatientsList) {
		this.mPatientsList = mPatientsList;
	}

	public void initNewPatient() {
		mNewPatient = new Patient();
		PatientBasic patientBasic = new PatientBasic();
		patientBasic.setAdmittingDiagnosis(new ArrayList<String>());
		patientBasic.setAllergies(new ArrayList<String>());
		patientBasic.setAdmittedFor(new ArrayList<SimpleListItem>());
		patientBasic.setPw(new ArrayList<String>());
		patientBasic.setPmh(new ArrayList<String>());
		patientBasic.setHomeMedications(new ArrayList<String>());
		mNewPatient.setPatientBasicInfo(patientBasic);

		// Setting Signout page
		List<SignoutPage> signoutPages = new ArrayList<SignoutPage>();
		SignoutPage firstPage = new SignoutPage();
		signoutPages.add(firstPage);
		firstPage.setRadiologies(new ArrayList<Radiology>());
		firstPage.setAllLabs(new ArrayList<Lab>());
		firstPage.setAllMeds(new ArrayList<Medication>());
		firstPage.setDoh(1);
		firstPage.setPageDate(new Date());
		mNewPatient.setSignoutPages(signoutPages);

		// Setting CoverPage
		CoverPage coverpage = new CoverPage();
		coverpage.setContingencyPlan(new ArrayList<SimpleListItem>());
		coverpage.setOnEvents(new ArrayList<SimpleListItem>());
		coverpage.setProblems(new ArrayList<SimpleListItem>());
		coverpage.setToDoList(new ArrayList<SimpleListItem>());
		mNewPatient.setCoverPage(coverpage);
	}

	public Patient getNewPatient() {
		return mNewPatient;
	}

	public Patient postNewPatient() {
		// Set all meds section to be as meds group
		for (Medication med : mNewPatient.getSignoutPages().get(0).getAllMeds()) {
			med.setSection(med.getGroup());
		}

		// Set laps sections
		boolean bmpFound, lftFound, abgFound, cbcFound, coagFound;
		bmpFound = lftFound = abgFound = cbcFound = coagFound = false;
		for (Lab lab : mNewPatient.getSignoutPages().get(0).getAllLabs())
			if (lab.getGroup().equalsIgnoreCase(Lab.BMP) && !bmpFound) {
				lab.setSection(SignoutActivity.TAB_FENGI);
				bmpFound = true;
			} else if (lab.getGroup().equalsIgnoreCase(Lab.LFT) && !lftFound) {
				lab.setSection(SignoutActivity.TAB_FENGI);
				lftFound = true;
			} else if (lab.getGroup().equalsIgnoreCase(Lab.ABG) && !abgFound) {
				lab.setSection(SignoutActivity.TAB_RESP);
				abgFound = true;
			} else if (lab.getGroup().equalsIgnoreCase(Lab.CBC) && !cbcFound) {
				lab.setSection(SignoutActivity.TAB_HEME);
				cbcFound = true;
			} else if (lab.getGroup().equalsIgnoreCase(Lab.COAGULTION) && !coagFound) {
				lab.setSection(SignoutActivity.TAB_HEME);
				coagFound = true;
			} else {
				lab.setSection(SignoutActivity.TAB_OTHER);
			}
		return Core.addEditPatient(mNewPatient);
	}

	public SignoutPage initNewSignoutPage() {
		// Check if there is no any signout pages
		List<Date> pageDates = getSignoutPagesList();
		SignoutPage lastSignoutPage;
		if (pageDates != null && pageDates.size() > 0)
			lastSignoutPage = Core.getSignoutPage(getSelectedPatient().getId(), getSignoutPagesList().get(0));
		else lastSignoutPage = null;
		mNewSignoutPage = new SignoutPage();
		mNewSignoutPage.setOtherHemeOnc(new ArrayList<SimpleListItem>());
		mNewSignoutPage.setAllLabs(new ArrayList<Lab>());
		mNewSignoutPage.setAllMeds(lastSignoutPage != null? lastSignoutPage.getAllMeds() : new ArrayList<Medication>());
		mNewSignoutPage.setRadiologies(new ArrayList<Radiology>());
		mNewSignoutPage.setPageDate(new Date());
		return mNewSignoutPage;
	}

	/**
	 * Get list of signout pages dates, then it get the first page from server,
	 * and if checks if it is not today, it create new page for today
	 * 
	 * @return List of signout page dates, with first item guaranteed to be
	 *         exist
	 */
	public List<Date> getSignoutPagesList() {
		Patient currentPatient = mPatientsList.get(mSelectedPatientIndex);
		mSignoutPages = Core.getSignoutPagesDays(currentPatient.getId());
		mNewSignoutPage = null;
		return mSignoutPages;
	}

	public Patient getSelectedPatient() {
		return mPatientsList.get(mSelectedPatientIndex);
	}

	public SignoutPage getSignoutPage(Date date) {
		Patient currentPatient = getPatientsList().get(getSelectedPatientIndex());
		SignoutPage page = null;
		if (date == null) {
			// We are still adding the new page locally
			page = getNewSignoutPage();
		} else { // get it from server
			page = Core.getSignoutPage(currentPatient.getId(), date);
		}
		List<SignoutPage> pages = new ArrayList<SignoutPage>();
		pages.add(page);
		currentPatient.setSignoutPages(pages);
		return page;
	}

	public SignoutPage getSelectedSignoutPage() {
		if (getPatientsList().get(getSelectedPatientIndex()).getSignoutPages().size() > 0)
			return getPatientsList().get(getSelectedPatientIndex()).getSignoutPages().get(0);
		else
			return null;
	}

	public boolean isNewSignoutPage() {
		return mNewSignoutPage != null;
	}

	public void setLoggedUser(User user) {
		loggedUser = user;
	}

	public User getLoggedUser() {
		return loggedUser;
	}

	private static PatientDelegate instance;

	public static PatientDelegate getInstance() {
		if (instance == null)
			instance = new PatientDelegate();
		return instance;
	}

	private PatientDelegate() {
	}

	public SignoutPage getNewSignoutPage() {
		return mNewSignoutPage;
	}
	
	public void removeNewSignoutPage() {
		mNewSignoutPage = null;
	}


	public void setSelectedPatientIndex(int position) {
		mSelectedPatientIndex = position;
	}

	public int getSelectedPatientIndex() {
		return mSelectedPatientIndex;
	}

	public void setNewPatient(Patient result) {
		this.mNewPatient = result;
	}

	public SignoutPage saveSignoutPage() {
		Patient patient = getPatientsList().get(getSelectedPatientIndex());
		boolean addingNew = false;
		if (getSelectedSignoutPage().getId() == 0) { // New signoutpage
			addingNew = true;
			handleNewPageBeforeSending();
		}
		SignoutPage page = Core.saveSignoutPage(patient.getId(), getSelectedSignoutPage());
		if (page != null) {
			patient.getSignoutPages().set(0, page);
			if (addingNew)
				mNewSignoutPage = null; // No new page anymore
		}
		return page;
	}

	private void handleNewPageBeforeSending() {
		SignoutPage page = getSelectedSignoutPage();
		// Set all ids to 0 to let server add them instead of modify them
		// TODO REMOVE DELETED ITEMS
		for (Lab lab : page.getAllLabs())
			lab.setId(0);
		for (Medication med : page.getAllMeds())
			med.setId(0);
		for (Radiology rad : page.getRadiologies())
			rad.setId(0);
		for (SimpleListItem item : page.getOtherHemeOnc())
			item.setId(0);
	}

	public boolean checkDateExist(Date pageDate) {
		for (Date date: mSignoutPages) {
			if (Utility.formatDate(date).equalsIgnoreCase(Utility.formatDate(pageDate)))
				return true;
		}
		return false;
	}
}
