package com.aham.patientsignoutapp.activity;

import org.apache.http.HttpStatus;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.activity.patientscreen.PatientActivity;
import com.aham.patientsignoutapp.api.Core;
import com.aham.patientsignoutapp.api.NetworkResult;
import com.aham.patientsignoutapp.controller.PatientDelegate;
import com.aham.signout.shared.messaging.User;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class LoginActivity extends FragmentActivity {

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;

	private String mUsername;
	private String mPassword;

	// UI references.
	private EditText mUsernameView;
	private EditText mPasswordView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;

	public final static String SAVED_USERNAME_URI = "com.aham.patientsignoutapp.SAVED_USERNAME";
	public final static String SAVED_PASSWORD_URI = "com.aham.patientsignoutapp.SAVED_PASSWROD";
	public final static String SAVED_PREF_URI = "com.aham.patientsignoutapp.PREF";
	public final static String LOGGED_OUT = "logout";

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().hide();
			Resources res = getResources();
			Drawable drawable = res.getDrawable(R.drawable.so_ab);
			getActionBar().setBackgroundDrawable(drawable);
		}
		setContentView(R.layout.activity_login);

		mUsernameView = (EditText) findViewById(R.id.email);

		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
				attemptLogin();
				return true;
			}
		});

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				attemptLogin();
			}
		});
		checkSavedLoginInfo();

	}

	private void checkSavedLoginInfo() {
		SharedPreferences sharedPref = getSharedPreferences(SAVED_PREF_URI, Context.MODE_PRIVATE);

		mUsername = sharedPref.getString(SAVED_USERNAME_URI, null);
		mPassword = sharedPref.getString(SAVED_PASSWORD_URI, null);

		mUsernameView.setText(mUsername);
		mPasswordView.setText(mPassword);

		if (mUsername != null && mPassword != null) {
			attemptLogin();
		}
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mUsernameView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mUsername = mUsernameView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 6) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid username
		if (TextUtils.isEmpty(mUsername)) {
			mUsernameView.setError(getString(R.string.error_field_required));
			focusView = mUsernameView;
			cancel = true;
		} /*
		 * else if (!mUsername.contains("@")) { // Check for username validate!
		 * mUsernameView.setError(getString(R.string.error_invalid_email));
		 * focusView = mUsernameView; cancel = true; }
		 */

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.execute((Void) null);

			// Hide soft keyboard
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(mPasswordView.getWindowToken(), 0);
			imm.hideSoftInputFromWindow(mUsernameView.getWindowToken(), 0);
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
				}
			});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
				}
			});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, NetworkResult<User>> {
		@Override
		protected NetworkResult<User> doInBackground(Void... params) {
			return Core.loginUser(mUsername, mPassword);
		}

		@Override
		protected void onPostExecute(final NetworkResult<User> result) {
			mAuthTask = null;
			showProgress(false);

			if (result.isOKResponse() && result.getResult() != null) {
				// Save current values
				SharedPreferences sharedPref = getSharedPreferences(SAVED_PREF_URI, Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = sharedPref.edit();
				editor.putString(SAVED_USERNAME_URI, mUsername);
				editor.putString(SAVED_PASSWORD_URI, mPassword);
				editor.commit();

				PatientDelegate.getInstance().setLoggedUser(result.getResult());

				// Open the patient list activity
				Intent intent = new Intent(LoginActivity.this, PatientActivity.class);
				startActivity(intent);

				finish();
			} else {
				// Problem getting the result
				if (result.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
					mPasswordView.setError(getString(R.string.error_incorrect_password));
					mPasswordView.requestFocus();
				} else if (result.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
					mUsernameView.setError(getString(R.string.error_incorrect_username));
					mUsernameView.requestFocus();
				} else {
					Toast.makeText(getApplicationContext(), R.string.error_connecting_server, Toast.LENGTH_LONG).show();
				}
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
}
