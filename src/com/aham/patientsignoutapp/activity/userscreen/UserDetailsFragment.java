package com.aham.patientsignoutapp.activity.userscreen;

import com.aham.patientsignoutapp.R;
import com.aham.signout.shared.messaging.User;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class UserDetailsFragment extends Fragment implements OnClickListener {
	
	User mUser;
	
	// UI
	EditText mUsernameView;
	EditText mFirstnameView;
	EditText mLastnameView;
	EditText mPasswordView;
	EditText mConfirmPasswordView;
	Button mChangePassButton;
	Spinner mUserGroupSpinner;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.user_details, container);
		
		mUsernameView = (EditText) view.findViewById(R.id.et_username);
		mFirstnameView = (EditText) view.findViewById(R.id.et_firstname);
		mLastnameView = (EditText) view.findViewById(R.id.et_lastname);
		mPasswordView = (EditText) view.findViewById(R.id.et_password);
		mConfirmPasswordView = (EditText) view.findViewById(R.id.et_confirm_password);
		mChangePassButton = (Button) view.findViewById(R.id.but_change_password);
		mUserGroupSpinner = (Spinner) view.findViewById(R.id.sp_usergroup);
		
		view.findViewById(R.id.tl_user_details).setVisibility(View.GONE);
		
		// Set adapter for the user group dropdown menu
		String[] userGroups = {User.DOCTOR, User.ADMIN};
		ArrayAdapter<CharSequence> userGroupsAdapter =  new ArrayAdapter<CharSequence>(getActivity(), android.R.layout.simple_spinner_item, userGroups);
		userGroupsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			Button saveButton = (Button) view.findViewById(R.id.but_save_user);
			saveButton.setVisibility(View.VISIBLE);
			saveButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (getActivity() instanceof UserActivity) {
						((UserActivity) getActivity()).saveUser();
					}
				}
			});
		}

		mUserGroupSpinner.setAdapter(userGroupsAdapter);
		
		return view;
	}

	private void updateGUI() {
		resetErrors();
		getView().findViewById(R.id.tl_user_details).setVisibility(View.VISIBLE);
		mUsernameView.setText(mUser.getUsername());
		mFirstnameView.setText(mUser.getFirstName());
		mLastnameView.setText(mUser.getLastName());
		mUserGroupSpinner.setSelection(((ArrayAdapter) mUserGroupSpinner.getAdapter()).getPosition(mUser.getType()));
		if (mUser.getId() > 0 ) {
			mChangePassButton.setVisibility(View.VISIBLE);
			mPasswordView.setVisibility(View.GONE);
			getView().findViewById(R.id.tr_confirm_password_row).setVisibility(View.GONE);
			mChangePassButton.setOnClickListener(this);
		} else { // Show password to enter
			mChangePassButton.setVisibility(View.GONE);
			mPasswordView.setVisibility(View.VISIBLE);
			getView().findViewById(R.id.tr_confirm_password_row).setVisibility(View.VISIBLE);
		}
	}
	
	public boolean validateInput() {
		boolean result = true;
		View focousView = null;
		if (TextUtils.isEmpty(mUsernameView.getText().toString())) {
			result = false;
			mUsernameView.setError(getString(R.string.error_field_required));
			focousView = mUsernameView;
		}
		
		if (TextUtils.isEmpty(mFirstnameView.getText().toString())) {
			result = false;
			mFirstnameView.setError(getString(R.string.error_field_required));
			focousView = mFirstnameView;
		}
		
		if (TextUtils.isEmpty(mLastnameView.getText().toString())) {
			result = false;
			mLastnameView.setError(getString(R.string.error_field_required));
			focousView = mLastnameView;
		}

		if (mPasswordView.getVisibility() == View.VISIBLE) {
			String password = mPasswordView.getText().toString();
			String confirmPassword = mConfirmPasswordView.getText().toString();
			
			if (TextUtils.isEmpty(password)) {
				result = false;
				mPasswordView.setError(getString(R.string.error_field_required));
				focousView = mPasswordView;
			}
			
			if (TextUtils.isEmpty(confirmPassword)) {
				result = false;
				mConfirmPasswordView.setError(getString(R.string.error_field_required));
				focousView = mConfirmPasswordView;
			}
			
			if (password.length() < 6) {
				result = false;
				mPasswordView.setError(getString(R.string.error_invalid_password));
				focousView = mPasswordView;
			}
			
			if (!password.equals(confirmPassword)) {
				result = false;
				mConfirmPasswordView.setError(getString(R.string.confirm_doesnt_match));
				focousView = mConfirmPasswordView;
			}
		}
		
		if(!result) focousView.requestFocus(); 
		
		return result;		
	}
	
	void resetErrors() {
		mFirstnameView.setError(null);
		mLastnameView.setError(null);
		mUsernameView.setError(null);
		mPasswordView.setError(null);
		mConfirmPasswordView.setError(null);
	}
	
	public void setUser(User user) {
		mUser = user;
		updateGUI();
	}

	@Override
	public void onClick(View arg0) {
		if (arg0.getId() == R.id.but_change_password) {
			
			mChangePassButton.setVisibility(View.GONE);
			mPasswordView.setVisibility(View.VISIBLE);
			getView().findViewById(R.id.tr_confirm_password_row).setVisibility(View.VISIBLE);
			
			mPasswordView.setText("");
			mConfirmPasswordView.setText("");
		}
	}
	
	public User getUserFromInputs() {
		return new User(0, mFirstnameView.getText().toString(), mLastnameView.getText().toString(),
				mUsernameView.getText().toString(), mPasswordView.getVisibility()== View.VISIBLE ? mPasswordView.getText().toString():null
				, mUserGroupSpinner.getSelectedItem().toString());
	}
}
