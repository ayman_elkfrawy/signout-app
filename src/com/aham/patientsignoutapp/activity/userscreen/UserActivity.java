package com.aham.patientsignoutapp.activity.userscreen;

import java.util.List;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.api.Core;
import com.aham.patientsignoutapp.util.BackgroundTask;
import com.aham.patientsignoutapp.util.BackgroundWorker;
import com.aham.patientsignoutapp.util.CallBackResult;
import com.aham.patientsignoutapp.util.Utility;
import com.aham.signout.shared.messaging.User;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.Window;
import android.widget.Toast;

public class UserActivity extends FragmentActivity implements UserListFragment.OnUserSelectedListener, LoaderCallbacks<List<User>>,
		OnMenuItemClickListener {

	UserListAdapter mUserListAdapter;
	int selectedIndex;
	static final int USER_LIST_LOADER_ID = 0;

	ListFragment mListFragment;
	UserDetailsFragment mDetailsFragment;

	Dialog mDialog;

	MenuItem mSave;
	MenuItem mDelete;
	MenuItem mAdd;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			Resources res = getResources();
			Drawable drawable = res.getDrawable(R.drawable.so_ab);
			getActionBar().setBackgroundDrawable(drawable);
		}
		setProgressBarIndeterminateVisibility(false);
		setContentView(R.layout.user_screen);

		getSupportLoaderManager().initLoader(USER_LIST_LOADER_ID, arg0, this);

		mListFragment = (ListFragment) getSupportFragmentManager().findFragmentById(R.id.user_list_fragment);
		mDetailsFragment = (UserDetailsFragment) getSupportFragmentManager().findFragmentById(R.id.user_details_fragment);

	}

	void setListAdapter() {
		mListFragment.setListAdapter(mUserListAdapter);
		selectedIndex = 0;
		onUserSelected(selectedIndex);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.user_screen_menu, menu);
		mSave = menu.findItem(R.id.mi_save);
		mDelete = menu.findItem(R.id.mi_delete);
		mAdd = menu.findItem(R.id.mi_add);
		if (mUserListAdapter == null) {
			mSave.setOnMenuItemClickListener(this);
			mDelete.setOnMenuItemClickListener(this);
			mAdd.setOnMenuItemClickListener(this);
		}
		return true;
	}

	@Override
	public void onUserSelected(int position) {
		selectedIndex = position;
		mUserListAdapter.setSelectedIndex(position);
		mDetailsFragment.setUser(mUserListAdapter.getItem(selectedIndex));
	}

	@Override
	public Loader<List<User>> onCreateLoader(int arg0, Bundle bundle) {
		return new UserListLoader(this);
	}

	@Override
	public void onLoadFinished(Loader<List<User>> loader, List<User> users) {
		setProgressBarIndeterminateVisibility(false);
		if (mUserListAdapter == null && users != null) {
			mUserListAdapter = new UserListAdapter(this, users);
			setListAdapter();
			if (mSave != null)
				mSave.setEnabled(true);
			if (mDelete != null)
				mDelete.setEnabled(true);
			if (mAdd != null)
				mAdd.setEnabled(true);
		}
	}

	@Override
	public void onLoaderReset(Loader<List<User>> loader) {
	}

	@Override
	public boolean onMenuItemClick(MenuItem arg0) {
		int menuID = arg0.getItemId();
		// On actionbar delete click
		if (menuID == R.id.mi_delete) {
			Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(R.string.confirm_delete).setPositiveButton(R.string.yes, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					deleteCurrentUser();
				}
			}).setNegativeButton(R.string.cancel, null).create().show();
		} else if (menuID == R.id.mi_save) {
			saveUser();
		} else if (menuID == R.id.mi_add) {
			// Check if a temporary user NOT already exists
			if (mUserListAdapter.getCount() > 0 && mUserListAdapter.getItem(0).getId() > 0) {
				// Add temporary user
				mUserListAdapter.insert(new User(), 0);
				onUserSelected(0);
			} else {
				// Check if the temporary user is not selected
				if (selectedIndex != 0) {
					onUserSelected(0); // Select the temporary user
				}
			}
		}
		return false;
	}

	public void saveUser() {
		if (mDetailsFragment.validateInput()) {
			saveCurrentUser();
		}
	}

	private void saveCurrentUser() {
		final User userToChange = mUserListAdapter.getItem(selectedIndex);
		final User tempUser = getUserFromInputs();
		setProgressBarIndeterminateVisibility(true);
		mDialog = Utility.getProgressdialog(this);
		new BackgroundWorker<Integer>(new BackgroundTask<Integer>() {
			@Override
			public Integer doInBackground() {
				if (userToChange.getId() > 0) {
					tempUser.setId(userToChange.getId());
				}
				return Core.addEditUser(tempUser);
			}
		}, new CallBackResult<Integer>() {
			@Override
			public void callBack(Integer result) {
				setProgressBarIndeterminateVisibility(false);
				if (mDialog != null)
					mDialog.dismiss();
				if (result > 0) {
					tempUser.setId(result);
					userToChange.copyFromUser(tempUser);
					mUserListAdapter.notifyDataSetChanged();
				} else {
					Toast.makeText(UserActivity.this, R.string.save_user_problem, Toast.LENGTH_LONG).show();
				}

			}
		});

	}

	private User getUserFromInputs() {
		return mDetailsFragment.getUserFromInputs();
	}

	private void deleteCurrentUser() {
		final User userToDelete = mUserListAdapter.getItem(selectedIndex);
		// Pre condition, if this is a temporary user, just delete it from list
		if (userToDelete.getId() <= 0) {
			mUserListAdapter.remove(userToDelete);
			// Set the details of the first user
			if (mUserListAdapter.getCount() > 0) {
				onUserSelected(0);
			}
			return;
		}
		setProgressBarIndeterminateVisibility(true);
		mDialog = Utility.getProgressdialog(this);
		new BackgroundWorker<Boolean>(new BackgroundTask<Boolean>() {
			@Override
			public Boolean doInBackground() {
				return Core.removeUser(userToDelete.getUsername());
			}
		}, new CallBackResult<Boolean>() {
			@Override
			public void callBack(Boolean result) {
				setProgressBarIndeterminateVisibility(false);
				if (mDialog != null)
					mDialog.dismiss();
				if (result) {
					mUserListAdapter.remove(userToDelete);
					// Set the details of the first user
					if (mUserListAdapter.getCount() > 0) {
						onUserSelected(0);
					}
				} else {
					Toast.makeText(UserActivity.this, R.string.delete_user_problem, Toast.LENGTH_LONG).show();
				}
			}
		});

	}

}
