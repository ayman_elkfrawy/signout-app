package com.aham.patientsignoutapp.activity.userscreen;

import java.util.List;

import com.aham.patientsignoutapp.R;
import com.aham.signout.shared.messaging.User;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class UserListAdapter extends ArrayAdapter<User> {

	List<User> mUsers;
	int selectedIndex;

	public UserListAdapter(Context context, List<User> users) {
		super(context, android.R.layout.two_line_list_item, users);
		setNotifyOnChange(true);
		mUsers = users;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null)
			convertView = ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
					android.R.layout.two_line_list_item, null);
		if (position == selectedIndex) {
			convertView.setBackgroundResource(R.drawable.gradient_bg_hover);
		} else convertView.setBackgroundResource(R.drawable.gradient_bg);
		
		User currentUser = mUsers.get(position);
		if (currentUser.getFirstName() == null) { // Looks like a new user
			((TextView) convertView.findViewById(android.R.id.text1)).setText("New User");
		} else
			((TextView) convertView.findViewById(android.R.id.text1)).setText(currentUser.getFirstName() + " "
					+ currentUser.getLastName());
		((TextView) convertView.findViewById(android.R.id.text2)).setText(currentUser.getType());
		return convertView;
	}
	
	public void setSelectedIndex(int index) {
		selectedIndex = index;
		notifyDataSetChanged();
	}

}
