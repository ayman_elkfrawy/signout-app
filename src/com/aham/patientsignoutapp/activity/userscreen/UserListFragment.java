package com.aham.patientsignoutapp.activity.userscreen;


import com.aham.patientsignoutapp.R;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

public class UserListFragment extends ListFragment {

	OnUserSelectedListener mCallback;
	View lastSelectedView=null;// to store last selected item
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		getListView().setBackgroundResource(R.drawable.so_list);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (OnUserSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnUserSelectedListener");
		}
	}

	public interface OnUserSelectedListener {
		public void onUserSelected(int position);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		mCallback.onUserSelected(position);
	}
	
}
