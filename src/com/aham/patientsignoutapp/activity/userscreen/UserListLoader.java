package com.aham.patientsignoutapp.activity.userscreen;

import java.util.List;

import com.aham.patientsignoutapp.api.Core;
import com.aham.signout.shared.messaging.User;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

public class UserListLoader extends AsyncTaskLoader<List<User>> {

	public UserListLoader(Context context) {
		super(context);
		forceLoad();
	}

	@Override
	public List<User> loadInBackground() {
		return Core.getListOfUsers();
	}

}
