package com.aham.patientsignoutapp.activity.signoutpage;

import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.controller.PatientDelegate;
import com.aham.patientsignoutapp.util.Utility;

public class SignoutListFragment extends ListFragment {

	OnPageSelectedListener mCallback;

	List<Date> mDateList;
	
	int mSelectedDateIndex;
	
	DatesAdapter mDateAdapter;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (OnPageSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnUserSelectedListener");
		}
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		getListView().setBackgroundResource(R.drawable.so_list);
	}

	public interface OnPageSelectedListener {
		public void onPageSelected(int position);
	}

	public void setDateList(List<Date> dates) {
		mDateList = dates;
		mDateAdapter = new DatesAdapter(getActivity(), dates);
		setListAdapter(mDateAdapter);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		setSelection(position);
		mSelectedDateIndex = position;
		mCallback.onPageSelected(position);
	}

	class DatesAdapter extends ArrayAdapter<Date> {

		public DatesAdapter(Context context, List<Date> objects) {
			super(context, android.R.layout.simple_list_item_1, objects);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null)
				convertView = ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
						.inflate(android.R.layout.simple_list_item_1, null);
			// To highlight selected row
			if (position == mSelectedDateIndex)
				convertView.setBackgroundResource(R.drawable.gradient_bg_hover);
			else
				convertView.setBackgroundResource(R.drawable.gradient_bg);
			String dateText = "";
			/*if (Utility.isToday(getItem(position)))
				dateText = "Today's page";
			else*/
				dateText = Utility.formatDate(getItem(position));
			((TextView) convertView.findViewById(android.R.id.text1)).setText(dateText);
			return convertView;
		}
	}

	public Date getSelectedDate() {
		return mDateList.get(mSelectedDateIndex);				
	}
	
	public int getSelectedDateIndex() {
		return mSelectedDateIndex;				
	}
	
	public void addDate(Date date) {
		mDateList.add(0,date);
		mDateAdapter.notifyDataSetChanged();
	}
	
	public void setDate(int position, Date date) {
		mDateList.set(position, date);
		mDateAdapter.notifyDataSetChanged();
	}

	public void updateSelected(int position) {
		mSelectedDateIndex = position;
		mDateAdapter.notifyDataSetChanged();
	}
	
	public List<Date> getDateList() {
		return mDateList;
	}

	public void removeDate(int i) {
		if (mDateList != null && mDateList.size() > 0) {
			mDateList.remove(i);
			mDateAdapter.notifyDataSetChanged();
		}
		
	}
	
}

