package com.aham.patientsignoutapp.activity.signoutpage;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.activity.editdialog.MedEditDialog;
import com.aham.patientsignoutapp.component.DialogEditableListAdapter;
import com.aham.patientsignoutapp.component.EditableListView;
import com.aham.patientsignoutapp.controller.PatientDelegate;
import com.aham.signout.shared.messaging.Medication;
import com.aham.signout.shared.messaging.SignoutPage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class TabCVFragment extends Fragment implements BasicSignoutTab {
	
	EditText mAccess;
	EditText mEKG;
	EditText mEcho;
	
	EditableListView mMeds;
	DialogEditableListAdapter<Medication> mMedsAdapter;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		View view = inflater.inflate(R.layout.signout_cv, container, false); 
		
		mAccess = (EditText) view.findViewById(R.id.et_Access);
		mEKG = (EditText) view.findViewById(R.id.et_EKG);
		mEcho = (EditText) view.findViewById(R.id.et_Echo);
		mMeds = (EditableListView) view.findViewById(R.id.elv_Meds_cv);
		
		SignoutPage page = PatientDelegate.getInstance().getSelectedSignoutPage();
		if(page != null) updateGUI(page);
		
		return view;
	}

	@Override
	public void updateGUI(SignoutPage signoutPage) {
		mAccess.setText(signoutPage.getAccess());
		mEKG.setText(signoutPage.getEkg());
		mEcho.setText(signoutPage.getEcho());
		
		mMedsAdapter = new DialogEditableListAdapter<Medication>(getActivity(), signoutPage.getMedsBySection(Medication.CV), mMeds, 
									new MedEditDialog(Medication.CV));
	}

	@Override
	public void populatePage(SignoutPage signoutPage) {
		signoutPage.setAccess(mAccess.getText().toString());
		signoutPage.setEkg(mEKG.getText().toString());
		signoutPage.setEcho(mEcho.getText().toString());
		
		signoutPage.addMedList(mMedsAdapter.getListItems(), Medication.CV);
	}
}
