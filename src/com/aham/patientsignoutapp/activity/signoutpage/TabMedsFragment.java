package com.aham.patientsignoutapp.activity.signoutpage;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.activity.editdialog.MedEditDialog;
import com.aham.patientsignoutapp.component.DialogEditableListAdapter;
import com.aham.patientsignoutapp.component.EditableListView;
import com.aham.patientsignoutapp.controller.PatientDelegate;
import com.aham.signout.shared.messaging.Medication;
import com.aham.signout.shared.messaging.SignoutPage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TabMedsFragment extends Fragment implements BasicSignoutTab {
	
	EditableListView mFENGIMeds;
	DialogEditableListAdapter<Medication> mFENGIAdapter;
	EditableListView mRespMeds;
	DialogEditableListAdapter<Medication> mRespAdapter;
	EditableListView mCVMeds;
	DialogEditableListAdapter<Medication> mCVAdapter;
	EditableListView mHEMEMeds;
	DialogEditableListAdapter<Medication> mHEMEAdapter;
	EditableListView mIDMeds;
	DialogEditableListAdapter<Medication> mIDAdapter;
	EditableListView mNeurologyMeds;
	DialogEditableListAdapter<Medication> mNeurologyAdapter;
	EditableListView mEndocrineMeds;
	DialogEditableListAdapter<Medication> mEndocrinedapter;
	EditableListView mNephroMeds;
	DialogEditableListAdapter<Medication> mNephroAdapter;
	EditableListView mOtherMeds;
	DialogEditableListAdapter<Medication> mOtherAdapter;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		View view = inflater.inflate(R.layout.signout_meds, container, false);
		
		mFENGIMeds = (EditableListView) view.findViewById(R.id.elv_FENGIMeds);
		mRespMeds = (EditableListView) view.findViewById(R.id.elv_RespMeds);
		mCVMeds = (EditableListView) view.findViewById(R.id.elv_CVMeds);
		mHEMEMeds = (EditableListView) view.findViewById(R.id.elv_HemeMeds);
		mIDMeds = (EditableListView) view.findViewById(R.id.elv_IDMeds);
		mNeurologyMeds = (EditableListView) view.findViewById(R.id.elv_NeurologyMeds);
		mEndocrineMeds = (EditableListView) view.findViewById(R.id.elv_EndocrineMeds);
		mNephroMeds = (EditableListView) view.findViewById(R.id.elv_NephroMeds);
		mOtherMeds = (EditableListView) view.findViewById(R.id.elv_otherMeds);
		
		SignoutPage page = PatientDelegate.getInstance().getSelectedSignoutPage();
		if(page != null) updateGUI(page);
		
		return view;
	}

	@Override
	public void updateGUI(SignoutPage signoutPage) {
		mFENGIAdapter = new DialogEditableListAdapter<Medication>(getActivity(), signoutPage.getMedsBySection(SignoutActivity.TAB_FENGI)
				, mFENGIMeds, new MedEditDialog(SignoutActivity.TAB_FENGI));
		mRespAdapter = new DialogEditableListAdapter<Medication>(getActivity(), signoutPage.getMedsBySection(SignoutActivity.TAB_RESP)
				, mRespMeds, new MedEditDialog(SignoutActivity.TAB_RESP));
		mCVAdapter = new DialogEditableListAdapter<Medication>(getActivity(), signoutPage.getMedsBySection(SignoutActivity.TAB_CV)
				, mCVMeds, new MedEditDialog(SignoutActivity.TAB_CV));
		mHEMEAdapter = new DialogEditableListAdapter<Medication>(getActivity(), signoutPage.getMedsBySection(SignoutActivity.TAB_HEME)
				, mHEMEMeds, new MedEditDialog(SignoutActivity.TAB_HEME));
		mIDAdapter = new DialogEditableListAdapter<Medication>(getActivity(), signoutPage.getMedsBySection(SignoutActivity.TAB_ID)
				, mIDMeds, new MedEditDialog(SignoutActivity.TAB_ID));
		mNeurologyAdapter = new DialogEditableListAdapter<Medication>(getActivity(), signoutPage.getMedsBySection(SignoutActivity.TAB_NEUROLOGY)
				, mNeurologyMeds, new MedEditDialog(SignoutActivity.TAB_NEUROLOGY));
		mEndocrinedapter = new DialogEditableListAdapter<Medication>(getActivity(), signoutPage.getMedsBySection(SignoutActivity.TAB_ENDOCRINE)
				, mEndocrineMeds, new MedEditDialog(null));
		mNephroAdapter = new DialogEditableListAdapter<Medication>(getActivity(), signoutPage.getMedsBySection(SignoutActivity.TAB_NEPHRO)
				, mNephroMeds, new MedEditDialog(null));
		mOtherAdapter = new DialogEditableListAdapter<Medication>(getActivity(), signoutPage.getMedsBySection(SignoutActivity.TAB_OTHER)
				, mOtherMeds, new MedEditDialog(null));
	}

	@Override
	public void populatePage(SignoutPage signoutPage) {
		signoutPage.addMedList(mFENGIAdapter.getListItems(), SignoutActivity.TAB_FENGI);
		signoutPage.addMedList(mRespAdapter.getListItems(), SignoutActivity.TAB_RESP);
		signoutPage.addMedList(mCVAdapter.getListItems(), SignoutActivity.TAB_CV);
		signoutPage.addMedList(mHEMEAdapter.getListItems(), SignoutActivity.TAB_HEME);
		signoutPage.addMedList(mIDAdapter.getListItems(), SignoutActivity.TAB_ID);
		signoutPage.addMedList(mNeurologyAdapter.getListItems(), SignoutActivity.TAB_NEUROLOGY);
		signoutPage.addMedList(mEndocrinedapter.getListItems(), SignoutActivity.TAB_ENDOCRINE);
		signoutPage.addMedList(mNephroAdapter.getListItems(), SignoutActivity.TAB_NEPHRO);
		signoutPage.addMedList(mOtherAdapter.getListItems(), SignoutActivity.TAB_OTHER);
	}
}
