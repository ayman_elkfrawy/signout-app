package com.aham.patientsignoutapp.activity.signoutpage;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.activity.editdialog.LabEditDialog;
import com.aham.patientsignoutapp.activity.editdialog.MedEditDialog;
import com.aham.patientsignoutapp.component.DialogEditableListAdapter;
import com.aham.patientsignoutapp.component.EditableListView;
import com.aham.patientsignoutapp.controller.PatientDelegate;
import com.aham.signout.shared.messaging.Lab;
import com.aham.signout.shared.messaging.Medication;
import com.aham.signout.shared.messaging.SignoutPage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TabNephroFragment extends Fragment implements BasicSignoutTab {
	
	EditableListView mMeds;
	DialogEditableListAdapter<Medication> mMedsAdapter;
	
	EditableListView mLabs;
	DialogEditableListAdapter<Lab> mLabsAdapter;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		View view = inflater.inflate(R.layout.signout_nephro, container, false);
		mMeds = (EditableListView) view.findViewById(R.id.elv_Meds_nephro);
		mLabs = (EditableListView) view.findViewById(R.id.elv_Labs_nephro);

		SignoutPage page = PatientDelegate.getInstance().getSelectedSignoutPage();
		if(page != null) updateGUI(page);

		return view;
	}

	@Override
	public void updateGUI(SignoutPage signoutPage) {
		mMedsAdapter = new DialogEditableListAdapter<Medication>(getActivity(), signoutPage.getMedsBySection(SignoutActivity.TAB_NEPHRO)
				, mMeds, new MedEditDialog(null));
		mLabsAdapter = new DialogEditableListAdapter<Lab>(getActivity(), signoutPage.getLabsBySection(SignoutActivity.TAB_NEPHRO)
				, mLabs, new LabEditDialog());
	}

	@Override
	public void populatePage(SignoutPage signoutPage) {
		signoutPage.addMedList(mMedsAdapter.getListItems(), SignoutActivity.TAB_NEPHRO);
		signoutPage.addLabList(mLabsAdapter.getListItems(), SignoutActivity.TAB_NEPHRO);
	}
}
