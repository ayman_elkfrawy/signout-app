package com.aham.patientsignoutapp.activity.signoutpage;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.activity.editdialog.MedEditDialog;
import com.aham.patientsignoutapp.component.DialogEditableListAdapter;
import com.aham.patientsignoutapp.component.EditableListView;
import com.aham.patientsignoutapp.controller.PatientDelegate;
import com.aham.signout.shared.messaging.Medication;
import com.aham.signout.shared.messaging.SignoutPage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class TabNeurologyFragment extends Fragment implements BasicSignoutTab {
	
	EditText mVEEG;
	EditableListView mMeds;
	DialogEditableListAdapter<Medication> mMedsAdapter;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		View view = inflater.inflate(R.layout.signout_neurology, container, false);
		mVEEG = (EditText) view.findViewById(R.id.et_VEEG);
		mMeds = (EditableListView) view.findViewById(R.id.elv_Meds_neurology);

		SignoutPage page = PatientDelegate.getInstance().getSelectedSignoutPage();
		if(page != null) updateGUI(page);
		
		return view;
	}

	@Override
	public void updateGUI(SignoutPage signoutPage) {
		mVEEG.setText(signoutPage.getVeeg());
		mMedsAdapter = new DialogEditableListAdapter<Medication>(getActivity(), signoutPage.getMedsBySection(Medication.NEUROLOGY),
				mMeds, new MedEditDialog(Medication.NEUROLOGY));
	}

	@Override
	public void populatePage(SignoutPage signoutPage) {
		signoutPage.setVeeg(mVEEG.getText().toString());
		signoutPage.addMedList(mMedsAdapter.getListItems(), Medication.NEUROLOGY);
	}
}
