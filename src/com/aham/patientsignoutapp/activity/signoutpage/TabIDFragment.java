package com.aham.patientsignoutapp.activity.signoutpage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.activity.editdialog.MedEditDialog;
import com.aham.patientsignoutapp.component.DialogEditableListAdapter;
import com.aham.patientsignoutapp.component.EditableListView;
import com.aham.patientsignoutapp.controller.PatientDelegate;
import com.aham.patientsignoutapp.util.Utility;
import com.aham.signout.shared.messaging.Medication;
import com.aham.signout.shared.messaging.SignoutPage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class TabIDFragment extends Fragment implements BasicSignoutTab {

	EditText mBcx;
	EditText mBcx2;
	EditText mBcxDate;
	EditText mUcx;
	EditText mUcx2;
	EditText mUcxDate;
	EditText mWcx;
	EditText mWcx2;
	EditText mWcxDate;
	EditText mCSFcx;
	EditText mCSFcx2;
	EditText mCSFcxDate;
	EditText mOther;

	EditableListView mMeds;
	DialogEditableListAdapter<Medication> mMedsAdapter;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		View view = inflater.inflate(R.layout.signout_id, container, false);
		mBcx = (EditText) view.findViewById(R.id.et_BloodCx);
		mBcx2 = (EditText) view.findViewById(R.id.et_BloodCx2);
		mBcxDate = (EditText) view.findViewById(R.id.et_BloodCxDate);
		mUcx = (EditText) view.findViewById(R.id.et_UrineCx);
		mUcx2 = (EditText) view.findViewById(R.id.et_UrineCx2);
		mUcxDate = (EditText) view.findViewById(R.id.et_UrineCxDate);
		mWcx = (EditText) view.findViewById(R.id.et_WoundCx);
		mWcx2 = (EditText) view.findViewById(R.id.et_WoundCx2);
		mWcxDate = (EditText) view.findViewById(R.id.et_WoundCxDate);
		mCSFcx = (EditText) view.findViewById(R.id.et_CSFCx);
		mCSFcx2 = (EditText) view.findViewById(R.id.et_CSFCx2);
		mCSFcxDate = (EditText) view.findViewById(R.id.et_CSFCxDate);
		mOther = (EditText) view.findViewById(R.id.et_Other);
		mMeds = (EditableListView) view.findViewById(R.id.elv_Meds_id);

		SignoutPage page = PatientDelegate.getInstance().getSelectedSignoutPage();
		if(page != null) updateGUI(page);

		return view;
	}

	@Override
	public void updateGUI(SignoutPage signoutPage) {
		Date today = new Date();
		mBcx.setText(signoutPage.getBcx1());
		mBcx2.setText(signoutPage.getBcx2());
		mBcxDate.setText(Utility.formatDate(signoutPage.getBcx()!=null?signoutPage.getBcx():today));
		mUcx.setText(signoutPage.getUcx1());
		mUcx2.setText(signoutPage.getUcx2());
		mUcxDate.setText(Utility.formatDate(signoutPage.getUcx()!=null?signoutPage.getUcx():today));
		mWcx.setText(signoutPage.getWcx1());
		mWcx2.setText(signoutPage.getWcx2());
		mWcxDate.setText(Utility.formatDate(signoutPage.getWcx()!=null?signoutPage.getWcx():today));
		mCSFcx.setText(signoutPage.getCsfcx1());
		mCSFcx2.setText(signoutPage.getCsfcx2());
		mCSFcxDate.setText(Utility.formatDate(signoutPage.getCsfcx()!=null?signoutPage.getCsfcx():today));
		mOther.setText(signoutPage.getOther());

		mMedsAdapter = new DialogEditableListAdapter<Medication>(getActivity(),
				signoutPage.getMedsBySection(Medication.ID), mMeds, new MedEditDialog(Medication.ID));

	}

	@Override
	public void populatePage(SignoutPage signoutPage) {
		try {
			signoutPage.setBcx(Utility.parseDate(mBcx.getText().toString()));
			signoutPage.setBcx(Utility.parseDate(mBcx.getText().toString()));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		signoutPage.setBcx1(mBcx.getText().toString());
		signoutPage.setBcx2(mBcx2.getText().toString());
		signoutPage.setUcx1(mUcx.getText().toString());
		signoutPage.setUcx2(mUcx2.getText().toString());
		signoutPage.setWcx1(mWcx.getText().toString());
		signoutPage.setWcx2(mWcx2.getText().toString());
		signoutPage.setCsfcx1(mCSFcx.getText().toString());
		signoutPage.setCsfcx2(mCSFcx2.getText().toString());
		signoutPage.setOther(mOther.getText().toString());

		signoutPage.addMedList(mMedsAdapter.getListItems(), Medication.ID);
	}
}
