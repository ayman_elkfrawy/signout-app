package com.aham.patientsignoutapp.activity.signoutpage;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.activity.editdialog.MedEditDialog;
import com.aham.patientsignoutapp.activity.editdialog.SimpleEditDialog;
import com.aham.patientsignoutapp.component.DialogEditableListAdapter;
import com.aham.patientsignoutapp.component.EditableListView;
import com.aham.patientsignoutapp.controller.PatientDelegate;
import com.aham.patientsignoutapp.util.Utility;
import com.aham.signout.shared.messaging.BMPlab;
import com.aham.signout.shared.messaging.CBClab;
import com.aham.signout.shared.messaging.CoagultionLab;
import com.aham.signout.shared.messaging.LFTlab;
import com.aham.signout.shared.messaging.Lab;
import com.aham.signout.shared.messaging.Medication;
import com.aham.signout.shared.messaging.SignoutPage;
import com.aham.signout.shared.messaging.SimpleListItem;

public class TabHemeFragment extends Fragment implements BasicSignoutTab {
	
	ViewGroup mCBCLab;
	ViewGroup mCoagulationLab;
	EditableListView mOthers;
	DialogEditableListAdapter<SimpleListItem> mOthersAdapter;
	EditableListView mMeds;
	DialogEditableListAdapter<Medication> mMedsAdapter;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		View view = inflater.inflate(R.layout.signout_heme, container, false);
		mCBCLab = (ViewGroup) view.findViewById(R.id.vg_lab_cbc);		
		mCoagulationLab = (ViewGroup) view.findViewById(R.id.vg_lab_coagulation);		
		mOthers = (EditableListView) view.findViewById(R.id.elv_heme_others);
		mMeds = (EditableListView) view.findViewById(R.id.elv_meds_heme);

		SignoutPage page = PatientDelegate.getInstance().getSelectedSignoutPage();
		if(page != null) updateGUI(page);
		
		return view;
	}

	@Override
	public void updateGUI(SignoutPage signoutPage) {
		List<Lab> labs = signoutPage.getLabsBySection(SignoutActivity.TAB_HEME);
		boolean cbcFound = false;
		boolean coagFound = false;
		for (Lab lab : labs) {
			if(lab instanceof CBClab) {
				lab.updateView(mCBCLab);
				cbcFound = true;
			} else if(lab instanceof CoagultionLab) {
				lab.updateView(mCoagulationLab);
				coagFound = true;
			}
		}
		if(!cbcFound) Utility.clearView(mCBCLab);
		if(!coagFound) Utility.clearView(mCoagulationLab);
		
		mOthersAdapter = new DialogEditableListAdapter<SimpleListItem>(getActivity(), signoutPage.getOtherHemeOnc(), mOthers, new SimpleEditDialog());
		mMedsAdapter = new DialogEditableListAdapter<Medication>(getActivity(), signoutPage.getMedsBySection(Medication.HEME_ONC),
								mMeds, new MedEditDialog(Medication.HEME_ONC));

	}

	@Override
	public void populatePage(SignoutPage signoutPage) {
		List<Lab> labs = signoutPage.getLabsBySection(SignoutActivity.TAB_HEME);
		boolean cbcFound = false;
		boolean coagFound = false;
		for (Lab lab : labs) {
			if(lab instanceof CBClab) {
				lab.updateView(mCBCLab);
				cbcFound = true;
			} else if(lab instanceof CoagultionLab) {
				lab.updateView(mCoagulationLab);
				coagFound = true;
			}
		}
		
		// Check if these labs wasn't exist
		List<Lab> newLabs = new ArrayList<Lab>();
		if(!cbcFound) { // no cbclab found then Create new lab
			Lab cbcLab = new CBClab();
			cbcLab.populateLab(mCBCLab);
			newLabs.add(cbcLab);
		}
		if(!coagFound) { // no coag lab found then Create new lab
			Lab coagLab = new CoagultionLab();
			coagLab.populateLab(mCoagulationLab);
			newLabs.add(coagLab);
		}
		if(newLabs.size() > 0) signoutPage.addLabList(newLabs, SignoutActivity.TAB_HEME);
		
		
		signoutPage.setOtherHemeOnc(mOthersAdapter.getListItems());
		signoutPage.addMedList(mMedsAdapter.getListItems(), Medication.HEME_ONC);
	}
}
