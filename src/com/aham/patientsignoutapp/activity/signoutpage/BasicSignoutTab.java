package com.aham.patientsignoutapp.activity.signoutpage;

import com.aham.signout.shared.messaging.SignoutPage;

public interface BasicSignoutTab {
	public void updateGUI(SignoutPage signoutPage);
	public void populatePage(SignoutPage signoutPage);
}
