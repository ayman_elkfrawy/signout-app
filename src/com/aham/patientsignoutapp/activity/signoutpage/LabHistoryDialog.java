package com.aham.patientsignoutapp.activity.signoutpage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.api.Core;
import com.aham.patientsignoutapp.controller.PatientDelegate;
import com.aham.patientsignoutapp.util.BackgroundTask;
import com.aham.patientsignoutapp.util.BackgroundWorker;
import com.aham.patientsignoutapp.util.CallBackResult;
import com.aham.signout.shared.messaging.Lab;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView.FindListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class LabHistoryDialog extends DialogFragment {

	List<Lab> mLabs;

	String mLabGroup;
	LinearLayout mHistoryContainer;
	ProgressBar mLoadingProgress;
	LinearLayout mFieldsHeader;
	TextView mNoLabsView;

	public static LabHistoryDialog newInstance(String labGroup) {
		LabHistoryDialog f = new LabHistoryDialog();

		// Supply num input as an argument.
		Bundle args = new Bundle();
		args.putString("lab", labGroup);
		f.setArguments(args);

		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mLabGroup = getArguments().getString("lab");

		new BackgroundWorker<List<Lab>>(new BackgroundTask<List<Lab>>() {
			@Override
			public List<Lab> doInBackground() {
				return Core.getLabHistory(PatientDelegate.getInstance().getSelectedPatient().getId(), mLabGroup);
			}
		}, new CallBackResult<List<Lab>>() {
			@Override
			public void callBack(List<Lab> result) {
				if (result != null) {
					mLabs = result;
					// Add local labs
					if (getActivity() instanceof SignoutActivity) {
						((SignoutActivity) getActivity()).saveCurrentTab();
						for (Lab localLab: PatientDelegate.getInstance().getSelectedSignoutPage().getAllLabs()) {
							if (!mLabGroup.equalsIgnoreCase(localLab.getGroup())) continue;
							if (localLab.getId() == 0) // this lab is just added locally
								result.add(localLab);
							else if (!localLab.isDeleted()){ // check if lab not deleted and replace it with the server lab
								for (Lab serveLab: result) {
									if (serveLab.getId() == localLab.getId()) {
										serveLab.replace(localLab);
										break;
									}
								}
							}
						}
					}
					mLabs = new ArrayList<Lab>();
					for(Lab lab: result) {
						if (lab.getDate() != null)
							mLabs.add(lab);
					}
					updateView();
				} else {
					Toast.makeText(getActivity(), R.string.error_connection, Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setNegativeButton("Close", null);
		builder.setView(getContentView());
		builder.setTitle(mLabGroup + " Lab history");
		
		return builder.create();
	}

	private View getContentView() {
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.lab_history, null);
		//view.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		mHistoryContainer = (LinearLayout) view.findViewById(R.id.vg_lab_history_container);
		mLoadingProgress = (ProgressBar) view.findViewById(R.id.pb_loading);
		mFieldsHeader = (LinearLayout) view.findViewById(R.id.vg_fields_title_column);
		mNoLabsView = (TextView) view.findViewById(R.id.tv_noLlabs_text);

		return view;
	}

	/*@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.lab_history, container, true);
		//view.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		mHistoryContainer = (LinearLayout) view.findViewById(R.id.vg_lab_history_container);
		mLoadingProgress = (ProgressBar) view.findViewById(R.id.pb_loading);

		return view;
	}*/

	private void updateView() {
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		// Check if labs size = 0
		if(mLabs.size() == 0) {
			mLoadingProgress.setVisibility(View.GONE);
			mHistoryContainer.setVisibility(View.GONE);
			mNoLabsView.setVisibility(View.VISIBLE);
		}
		
		// Add fields names
		updateColumn(mFieldsHeader, mLabs.get(0).getFieldsNames(), null, null);
		// mHistoryContainer.addView(getVerticalLine());

		// Add values for each day
		for (int i=0 ; i < mLabs.size() ; i++) {
			Lab lab = mLabs.get(i);
			if (lab.getDate() == null)
				continue; // Skip empty labs
			LinearLayout fieldsValuesColumn = (LinearLayout) inflater.inflate(R.layout.lab_history_column, null);
			updateColumn(fieldsValuesColumn, lab.getFieldsValues(), i==0?null:mLabs.get(i-1).getFieldsValues(), lab.getDate());
			mHistoryContainer.addView(fieldsValuesColumn);
			// mHistoryContainer.addView(getVerticalLine());
		}

		// Hide progress bar and show the content
		mLoadingProgress.setVisibility(View.GONE);
		mHistoryContainer.setVisibility(View.VISIBLE);
		mNoLabsView.setVisibility(View.GONE);
		
	}

	private void updateColumn(LinearLayout fieldsColumn, String[] values, String[] preValues, Date date) {
		String headString = "";
		if (date != null)
			headString = new SimpleDateFormat("dd/MMM").format(date);
		fieldsColumn.addView(getTextView(headString, true, false, 0f));
		boolean alternate = false;
		for (int i=0; i<values.length ; i++) {
			String value = values[i];
			fieldsColumn.addView(getTextView(value, date == null, (alternate = !alternate),
					preValues==null?0:floatDiffer(value, preValues[i])));
			// fieldsColumn.addView(getHorizontalLine());
		}
	}

	private Float floatDiffer(String value1 , String value2) {
		try {
			float value1Float = Float.parseFloat(value1);
			float value2Float = Float.parseFloat(value2);
			return (value1Float - value2Float);
		} catch (NumberFormatException e) {
			return null;
		}
	}


	public View getTextView(String value, boolean header, boolean altColor, Float differnce) {
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewGroup itemView = (ViewGroup) inflater.inflate(R.layout.lab_history_item, null);
		// Setup the text view
		TextView textView = (TextView) itemView.findViewById(R.id.tv_item_value);
		textView.setText(value);
		textView.setTextAppearance(getActivity(), header ? R.style.HistoryLabelHeader : R.style.HistoryLabel);

		if (altColor)
			itemView.setBackgroundColor(Color.rgb(225, 225, 225));
		else
			itemView.setBackgroundColor(Color.rgb(250, 250, 250));

		// Setup the state image
		ImageView imageView = (ImageView) itemView.findViewById(R.id.iv_item_state);
		if(header) imageView.setVisibility(View.GONE);
		else {
			if (differnce == null) imageView.setVisibility(View.INVISIBLE);
			else if (differnce > 0) imageView.setImageResource(R.drawable.arrow_up);
			else if (differnce < 0) imageView.setImageResource(R.drawable.arrow_down);
			else {
				imageView.setBackgroundResource(R.drawable.arrow_middle);
				imageView.setLayoutParams(new LinearLayout.LayoutParams(dpToPx(32), LinearLayout.LayoutParams.WRAP_CONTENT));
			}
		}
		return itemView;
	}

	public View getHorizontalLine() {
		View v = new View(getActivity());
		v.setLayoutParams(new LayoutParams(120, dpToPx(3)));
		v.setBackgroundColor(Color.rgb(51, 51, 51));
		return v;
	}

	public View getVerticalLine() {
		View v = new View(getActivity());
		v.setLayoutParams(new LayoutParams(dpToPx(3), LayoutParams.MATCH_PARENT));
		v.setBackgroundColor(Color.rgb(51, 51, 51));
		return v;
	}

	public List<Lab> getmLabs() {
		return mLabs;
	}

	public void setmLabs(List<Lab> mLabs) {
		this.mLabs = mLabs;
	}

	public int dpToPx(int dp) {
		Resources r = getResources();
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
	}

}
