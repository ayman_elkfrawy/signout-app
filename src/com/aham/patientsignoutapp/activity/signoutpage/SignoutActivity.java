package com.aham.patientsignoutapp.activity.signoutpage;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.activity.signoutpage.SignoutListFragment.OnPageSelectedListener;
import com.aham.patientsignoutapp.controller.PatientDelegate;
import com.aham.patientsignoutapp.util.BackgroundTask;
import com.aham.patientsignoutapp.util.BackgroundWorker;
import com.aham.patientsignoutapp.util.CallBackResult;
import com.aham.patientsignoutapp.util.Utility;
import com.aham.signout.shared.messaging.Medication;
import com.aham.signout.shared.messaging.Patient;
import com.aham.signout.shared.messaging.SignoutPage;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabContentFactory;
import android.widget.Toast;

public class SignoutActivity extends FragmentActivity implements OnPageSelectedListener, OnTabChangeListener, OnMenuItemClickListener {

	SignoutListFragment mSignoutListFragment;
	private TabHost mTabHost;
	private HashMap<String, TabInfo> mapTabInfo = new HashMap<String, TabInfo>();
	private TabInfo mLastTab = null;

	private boolean addingNewPage = false;

	Dialog mProgressDialog;

	MenuItem mSaveButton;
	MenuItem mAddButton;
	MenuItem mDeleteButton;

	public static final String TAB_CV = Medication.CV;
	public static final String TAB_ENDOCRINE = "Endocrine";
	public static final String TAB_FENGI = Medication.FENGI;
	public static final String TAB_HEME = Medication.HEME_ONC;
	public static final String TAB_ID = Medication.ID;
	public static final String TAB_ALL_MEDS = "All Meds";
	public static final String TAB_NEPHRO = "NEPHRO";
	public static final String TAB_NEUROLOGY = Medication.NEUROLOGY;
	public static final String TAB_OTHER = "Other";
	public static final String TAB_RESP = Medication.RESP;
	public static final String TAB_VITALS = "Vitals";

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			Resources res = getResources();
			Drawable drawable = res.getDrawable(R.drawable.so_ab);
			getActionBar().setBackgroundDrawable(drawable);
		}
		setContentView(R.layout.signout_screen);
		mSignoutListFragment = (SignoutListFragment) getSupportFragmentManager().findFragmentById(R.id.signout_list_fragment);
		initialiseTabHost(arg0);

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			Button saveButton = (Button) findViewById(R.id.but_save_signoutpage);
			saveButton.setVisibility(View.VISIBLE);
			saveButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					savePage();
				}
			});
		}

		setProgressBarIndeterminateVisibility(false);

		getDateList();
	}

	private void initialiseTabHost(Bundle args) {
		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup();
		TabInfo tabInfo = null;

		addTab(this, this.mTabHost, this.mTabHost.newTabSpec(TAB_VITALS).setIndicator(TAB_VITALS), (tabInfo = new TabInfo(TAB_VITALS,
				TabVitalsFragment.class, args)));
		this.mapTabInfo.put(tabInfo.tag, tabInfo);

		addTab(this, this.mTabHost, this.mTabHost.newTabSpec(TAB_FENGI).setIndicator(TAB_FENGI), (tabInfo = new TabInfo(TAB_FENGI,
				TabFENGIFragment.class, args)));
		this.mapTabInfo.put(tabInfo.tag, tabInfo);

		addTab(this, this.mTabHost, this.mTabHost.newTabSpec(TAB_RESP).setIndicator(TAB_RESP), (tabInfo = new TabInfo(TAB_RESP,
				TabRESPFragment.class, args)));
		this.mapTabInfo.put(tabInfo.tag, tabInfo);

		addTab(this, this.mTabHost, this.mTabHost.newTabSpec(TAB_CV).setIndicator(TAB_CV), (tabInfo = new TabInfo(TAB_CV,
				TabCVFragment.class, args)));
		this.mapTabInfo.put(tabInfo.tag, tabInfo);

		addTab(this, this.mTabHost, this.mTabHost.newTabSpec(TAB_ID).setIndicator(TAB_ID), (tabInfo = new TabInfo(TAB_ID,
				TabIDFragment.class, args)));
		this.mapTabInfo.put(tabInfo.tag, tabInfo);

		addTab(this, this.mTabHost, this.mTabHost.newTabSpec(TAB_HEME).setIndicator(TAB_HEME), (tabInfo = new TabInfo(TAB_HEME,
				TabHemeFragment.class, args)));
		this.mapTabInfo.put(tabInfo.tag, tabInfo);

		addTab(this, this.mTabHost, this.mTabHost.newTabSpec(TAB_NEUROLOGY).setIndicator(TAB_NEUROLOGY), (tabInfo = new TabInfo(
				TAB_NEUROLOGY, TabNeurologyFragment.class, args)));
		this.mapTabInfo.put(tabInfo.tag, tabInfo);

		addTab(this, this.mTabHost, this.mTabHost.newTabSpec(TAB_NEPHRO).setIndicator(TAB_NEPHRO), (tabInfo = new TabInfo(TAB_NEPHRO,
				TabNephroFragment.class, args)));
		this.mapTabInfo.put(tabInfo.tag, tabInfo);

		addTab(this, this.mTabHost, this.mTabHost.newTabSpec(TAB_ENDOCRINE).setIndicator(TAB_ENDOCRINE), (tabInfo = new TabInfo(
				TAB_ENDOCRINE, TabEndocrineFragment.class, args)));
		this.mapTabInfo.put(tabInfo.tag, tabInfo);

		addTab(this, this.mTabHost, this.mTabHost.newTabSpec(TAB_OTHER).setIndicator(TAB_OTHER), (tabInfo = new TabInfo(TAB_OTHER,
				TabOtherFragment.class, args)));
		this.mapTabInfo.put(tabInfo.tag, tabInfo);

		addTab(this, this.mTabHost, this.mTabHost.newTabSpec(TAB_ALL_MEDS).setIndicator(TAB_ALL_MEDS), (tabInfo = new TabInfo(TAB_ALL_MEDS,
				TabMedsFragment.class, args)));
		this.mapTabInfo.put(tabInfo.tag, tabInfo);

		this.onTabChanged(TAB_VITALS);

		// Adjust tab width for older versions
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			int px = Utility.dpToPx(this, 70);
			TabWidget tw = mTabHost.getTabWidget();
			// View currentView;
			for (int i = 0; i < tw.getChildCount(); i++) {
				tw.getChildAt(i).getLayoutParams().width = px;
				// currentView = tw.getChildAt(i);
				// LinearLayout.LayoutParams currentLayout =
				// (LinearLayout.LayoutParams) currentView.getLayoutParams();
				// currentLayout.setMargins(px, 0, px, 0);
			}
			tw.requestLayout();
		}
		mTabHost.setOnTabChangedListener(this);
	}

	private static void addTab(SignoutActivity activity, TabHost tabHost, TabHost.TabSpec tabSpec, TabInfo tabInfo) {
		// Attach a Tab view factory to the spec
		tabSpec.setContent(activity.new TabFactory(activity));
		String tag = tabSpec.getTag();

		if (tabInfo.fragment == null) { // initiate the fragment
			FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
			tabInfo.fragment = Fragment.instantiate(activity, tabInfo.clss.getName(), tabInfo.args);
			ft.add(R.id.realtabcontent, tabInfo.fragment, tabInfo.tag);
			ft.attach(tabInfo.fragment);
			ft.commit();
			activity.getSupportFragmentManager().executePendingTransactions();
		}

		// Check to see if we already have a fragment for this tab, probably
		// from a previously saved state. If so, deactivate it, because our
		// initial state is that a tab isn't shown.
		tabInfo.fragment = activity.getSupportFragmentManager().findFragmentByTag(tag);
		if (tabInfo.fragment != null && !tabInfo.fragment.isDetached()) {
			FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
			ft.detach(tabInfo.fragment);
			ft.commit();
			activity.getSupportFragmentManager().executePendingTransactions();
		}

		tabHost.addTab(tabSpec);
	}

	@Override
	public void onTabChanged(String tag) {
		TabInfo newTab = this.mapTabInfo.get(tag);
		if (mLastTab != newTab) {
			SignoutPage page = PatientDelegate.getInstance().getSelectedSignoutPage();
			FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
			if (mLastTab != null) {
				if (mLastTab.fragment != null) {
					// Save current data
					((BasicSignoutTab) mLastTab.fragment).populatePage(page);
					ft.detach(mLastTab.fragment);
				}
			}
			if (newTab != null) {
				if (newTab.fragment == null) {
					newTab.fragment = Fragment.instantiate(this, newTab.clss.getName(), newTab.args);
					ft.add(R.id.realtabcontent, newTab.fragment, newTab.tag);
				} else {
					ft.attach(newTab.fragment);
				}
			}

			mLastTab = newTab;

			ft.commit();
			this.getSupportFragmentManager().executePendingTransactions();
		}
	}

	public void saveCurrentTab() {
		((BasicSignoutTab) mLastTab.fragment).populatePage(PatientDelegate.getInstance().getSelectedSignoutPage());
	}

	@Override
	public void onPageSelected(final int position) {
		mSignoutListFragment.updateSelected(position);
		setProgressBarIndeterminateVisibility(true);
		mProgressDialog = Utility.getProgressdialog(this);
		// Get the selected signoutpage
		new BackgroundWorker<SignoutPage>(new BackgroundTask<SignoutPage>() {
			@Override
			public SignoutPage doInBackground() {
				if (position == 0 && isAddingNewPage())
					return PatientDelegate.getInstance().getSignoutPage(null);
				return PatientDelegate.getInstance().getSignoutPage(mSignoutListFragment.getSelectedDate());
			}
		}, new CallBackResult<SignoutPage>() {
			@Override
			public void callBack(SignoutPage result) {
				setProgressBarIndeterminateVisibility(false);
				if (mProgressDialog != null)
					mProgressDialog.dismiss();
				if (mSaveButton != null)
					mSaveButton.setEnabled(true);
				if (result != null) {
					findViewById(R.id.vg_signout_container).setVisibility(View.VISIBLE);
					// Update current tab
					if (mLastTab.getFragment() != null) {
						((BasicSignoutTab) mLastTab.getFragment()).updateGUI(result);
					}
					// Move to the vitals tap
					mTabHost.setCurrentTabByTag(TAB_VITALS);
					ScrollView scroll = (ScrollView) mLastTab.getFragment().getView().findViewById(R.id.sv_vitals_scroll);
					scroll.scrollTo(0, 0);
				} else {
					Toast.makeText(SignoutActivity.this, R.string.error_connection, Toast.LENGTH_LONG).show();
				}
			}
		});

	}

	void getDateList() {
		setProgressBarIndeterminateVisibility(true);
		mProgressDialog = Utility.getProgressdialog(this);
		if (mAddButton != null)
			mAddButton.setEnabled(false);
		if (mSaveButton != null)
			mSaveButton.setEnabled(false);
		new BackgroundWorker<List<Date>>(new BackgroundTask<List<Date>>() {
			@Override
			public List<Date> doInBackground() {
				return PatientDelegate.getInstance().getSignoutPagesList();
			}
		}, new CallBackResult<List<Date>>() {

			@Override
			public void callBack(List<Date> result) {
				setProgressBarIndeterminateVisibility(false);
				if (mProgressDialog != null)
					mProgressDialog.dismiss();
				if (result != null) {
					if (mAddButton != null)
						mAddButton.setEnabled(true);
					mSignoutListFragment.setDateList(result);
					if (result.size() > 0)
						onPageSelected(0);
				} else {
					Toast.makeText(SignoutActivity.this, R.string.error_connection, Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	@Override
	public boolean onMenuItemClick(MenuItem item) {
		if (item.getItemId() == R.id.mi_save) {
			savePage();
		} else if (item.getItemId() == R.id.mi_add) {
			addNewSignoutPage();
		} else if (item.getItemId() == R.id.mi_delete) {
			// Pre condition
			if (mSignoutListFragment.getDateList() == null ||
					mSignoutListFragment.getDateList().size() == 0)
				return false;
						
			// Show confirm message
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(R.string.delete_signoutpage_title);
			builder.setMessage(Html.fromHtml(getString(R.string.delete_signoutpage_confirm)));
			builder.setNegativeButton(R.string.cancel, new Dialog.OnClickListener() {
				@Override
				public void onClick(DialogInterface arg0, int arg1) {}});
			builder.setPositiveButton(R.string.delete, new Dialog.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					deletePage();
				}

			});
			builder.create().show();
			return true;
		}

		return false;
	}

	private void addNewSignoutPage() {
		if (isAddingNewPage()) { // Page already added, ensure being selected
			if (mSignoutListFragment.getSelectedDateIndex() != 0) {
				onPageSelected(0);
			}
		} else {
			setProgressBarIndeterminateVisibility(true);
			mProgressDialog = Utility.getProgressdialog(this);
			new BackgroundWorker<SignoutPage>(new BackgroundTask<SignoutPage>() {
				@Override
				public SignoutPage doInBackground() {
					return PatientDelegate.getInstance().initNewSignoutPage();
				}
			}, new CallBackResult<SignoutPage>() {
				@Override
				public void callBack(SignoutPage result) {
					setProgressBarIndeterminateVisibility(false);
					if (mProgressDialog != null)
						mProgressDialog.dismiss();
					if (result != null) {
						mSignoutListFragment.addDate(result.getPageDate());
						setAddingNewPage(true);
						onPageSelected(0);
					} else {
						Toast.makeText(SignoutActivity.this, R.string.error_connection, Toast.LENGTH_LONG).show();
					}
				}
			});
		}
	}

	private void savePage() {
		// Check dates problems
		final boolean savingNewPage = mSignoutListFragment.getSelectedDateIndex() == 0 && isAddingNewPage();
		final int dateIndexToSave = mSignoutListFragment.getSelectedDateIndex();
		if (!checkDatesBeforeSaving(savingNewPage))
			return;

		setProgressBarIndeterminateVisibility(true);
		SignoutPage pageToSave = PatientDelegate.getInstance().getSelectedSignoutPage();
		((BasicSignoutTab) mLastTab.getFragment()).populatePage(pageToSave);

		// Save current changes
		new BackgroundWorker<SignoutPage>(new BackgroundTask<SignoutPage>() {
			@Override
			public SignoutPage doInBackground() {
				return PatientDelegate.getInstance().saveSignoutPage();
			}
		}, new CallBackResult<SignoutPage>() {
			@Override
			public void callBack(SignoutPage result) {
				setProgressBarIndeterminateVisibility(false);
				if (mProgressDialog != null)
					mProgressDialog.dismiss();
				if (result != null) {
					if (savingNewPage)
						setAddingNewPage(false); // The new page has been added
					mSignoutListFragment.setDate(dateIndexToSave, result.getPageDate());
					onPageSelected(mSignoutListFragment.getSelectedDateIndex());
				} else {
					Toast.makeText(SignoutActivity.this, R.string.error_connection, Toast.LENGTH_LONG).show();
				}
			}
		});
	}
	
	private void deletePage() {
		// ckeck if it was just new page
		final boolean newPage = mSignoutListFragment.getSelectedDateIndex() == 0 && isAddingNewPage();
		if (newPage) {
			setAddingNewPage(false);
			PatientDelegate.getInstance().removeNewSignoutPage();
			mSignoutListFragment.removeDate(0);
			if (mSignoutListFragment.getDateList().size() > 0)
				onPageSelected(0);
			else {
				// Hide the tabs
				findViewById(R.id.vg_signout_container).setVisibility(View.INVISIBLE);
			}
			return;
		}
		
		final int dateIndexToDelete = mSignoutListFragment.getSelectedDateIndex();

		setProgressBarIndeterminateVisibility(true);
		SignoutPage pageToSave = PatientDelegate.getInstance().getSelectedSignoutPage();
		pageToSave.setTimeDeleted(new Date()); // Delete date
		
		// Save current changes
		new BackgroundWorker<SignoutPage>(new BackgroundTask<SignoutPage>() {
			@Override
			public SignoutPage doInBackground() {
				return PatientDelegate.getInstance().saveSignoutPage();
			}
		}, new CallBackResult<SignoutPage>() {
			@Override
			public void callBack(SignoutPage result) {
				setProgressBarIndeterminateVisibility(false);
				if (mProgressDialog != null)
					mProgressDialog.dismiss();
				if (result != null) {
					mSignoutListFragment.removeDate(dateIndexToDelete);
					if (mSignoutListFragment.getDateList().size() > 0)
						onPageSelected(0);
					else {
						// Hide the tabs
						findViewById(R.id.vg_signout_container).setVisibility(View.INVISIBLE);
					}
				} else {
					Toast.makeText(SignoutActivity.this, R.string.error_connection, Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	private boolean checkDatesBeforeSaving(boolean addNewPage) {
		Date pageDate = null;
		if (mLastTab.tag.equalsIgnoreCase(TAB_VITALS)) {
			EditText dateTextView = (EditText) mLastTab.getFragment().getView().findViewById(R.id.et_page_date);
			try {
				pageDate = Utility.parseDate(dateTextView.getText().toString());
			} catch (ParseException e) {
				dateTextView.setError("Invalid date format");
				dateTextView.requestFocus();
				return false;
			}
		} else
			pageDate = PatientDelegate.getInstance().getSelectedSignoutPage().getPageDate();
		if (PatientDelegate.getInstance().checkDateExist(pageDate) && addNewPage) {
			if (!mLastTab.tag.equalsIgnoreCase(TAB_VITALS)) {
				// Set current tab to vital
				mTabHost.setCurrentTabByTag(TAB_VITALS);
			}
			EditText dateTextView = (EditText) mLastTab.getFragment().getView().findViewById(R.id.et_page_date);
			dateTextView.setError("This date already exist");
			dateTextView.requestFocus();
			return false;
		} else
			return true;
	}

	public boolean isAddingNewPage() {
		return addingNewPage;
	}

	public void setAddingNewPage(boolean addingNewPage) {
		this.addingNewPage = addingNewPage;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.signout_screen_menu, menu);
		mSaveButton = menu.findItem(R.id.mi_save);
		mAddButton = menu.findItem(R.id.mi_add);
		mDeleteButton = menu.findItem(R.id.mi_delete);
		mSaveButton.setOnMenuItemClickListener(this);
		mAddButton.setOnMenuItemClickListener(this);
		mDeleteButton.setOnMenuItemClickListener(this);

		return true;
	}

	private class TabInfo {
		private String tag;
		private Class clss;
		private Bundle args;
		private Fragment fragment;

		TabInfo(String tag, Class clazz, Bundle args) {
			this.tag = tag;
			this.clss = clazz;
			this.args = args;
		}

		public Fragment getFragment() {
			return fragment;
		}

	}

	class TabFactory implements TabContentFactory {

		private final Context mContext;

		/**
		 * @param context
		 */
		public TabFactory(Context context) {
			mContext = context;
		}

		/**
		 * (non-Javadoc)
		 * 
		 * @see android.widget.TabHost.TabContentFactory#createTabContent(java.lang.String)
		 */
		public View createTabContent(String tag) {
			View v = new View(mContext);
			v.setMinimumWidth(0);
			v.setMinimumHeight(0);
			return v;
		}

	}

}
