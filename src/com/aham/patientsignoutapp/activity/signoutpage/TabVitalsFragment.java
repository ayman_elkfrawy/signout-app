package com.aham.patientsignoutapp.activity.signoutpage;

import java.text.ParseException;
import java.util.Date;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.controller.PatientDelegate;
import com.aham.patientsignoutapp.util.Utility;
import com.aham.signout.shared.messaging.SignoutPage;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class TabVitalsFragment extends Fragment implements BasicSignoutTab {
	EditText mRRMin, mRRMax;
	EditText mTempMin, mTempMax;
	EditText mHRMin, mHRMax;
	EditText mBPMin, mBPMax;
	EditText mO2SatMin, mO2SatMax;
	EditText mPageDate;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}

		View view = inflater.inflate(R.layout.signout_vitals, container, false);
		mRRMin = (EditText) view.findViewById(R.id.et_rr_min);
		mRRMax = (EditText) view.findViewById(R.id.et_rr_max);
		mTempMin = (EditText) view.findViewById(R.id.et_temp_min);
		mTempMax = (EditText) view.findViewById(R.id.et_temp_max);
		mHRMin = (EditText) view.findViewById(R.id.et_hr_min);
		mHRMax = (EditText) view.findViewById(R.id.et_hr_max);
		mBPMin = (EditText) view.findViewById(R.id.et_bp_min);
		mBPMax = (EditText) view.findViewById(R.id.et_bp_max);
		mO2SatMin = (EditText) view.findViewById(R.id.et_o2sat_min);
		mO2SatMax = (EditText) view.findViewById(R.id.et_o2sat_max);
		mPageDate = (EditText) view.findViewById(R.id.et_page_date);
		Utility.assignEditTextDate(mPageDate, new Date());

		mTempMin.addTextChangedListener(new TempratureTextChange(mTempMin));
		mTempMax.addTextChangedListener(new TempratureTextChange(mTempMax));

		SignoutPage page = PatientDelegate.getInstance().getSelectedSignoutPage();
		if (page != null)
			updateGUI(page);

		return view;
	}

	@Override
	public void updateGUI(SignoutPage signoutPage) {
		mRRMin.setText(Utility.numberToString(signoutPage.getRrMin()));
		mRRMax.setText(Utility.numberToString(signoutPage.getRrMax()));
		mTempMin.setText(Utility.numberToString(signoutPage.getTempMin()));
		mTempMax.setText(Utility.numberToString(signoutPage.getTempMax()));
		mHRMin.setText(Utility.numberToString(signoutPage.getHrMin()));
		mHRMax.setText(Utility.numberToString(signoutPage.getHrMax()));
		mBPMin.setText(Utility.numberToString(signoutPage.getBpMin()));
		mBPMax.setText(Utility.numberToString(signoutPage.getBpMax()));
		mO2SatMin.setText(Utility.numberToString(signoutPage.getO2satMin()));
		mO2SatMax.setText(Utility.numberToString(signoutPage.getO2satMax()));
		mPageDate.setText(Utility.formatDate(signoutPage.getPageDate()));
		mPageDate.setError(null);
	}

	@Override
	public void populatePage(SignoutPage signoutPage) {
		signoutPage.setRrMax(Utility.parseFloat(mRRMax.getText().toString()));
		signoutPage.setRrMin(Utility.parseFloat(mRRMin.getText().toString()));
		signoutPage.setTempMax(Utility.parseFloat(mTempMax.getText().toString()));
		signoutPage.setTempMin(Utility.parseFloat(mTempMin.getText().toString()));
		signoutPage.setHrMax(Utility.parseFloat(mHRMax.getText().toString()));
		signoutPage.setHrMin(Utility.parseFloat(mHRMin.getText().toString()));
		signoutPage.setBpMax(Utility.parseFloat(mBPMax.getText().toString()));
		signoutPage.setBpMin(Utility.parseFloat(mBPMin.getText().toString()));
		signoutPage.setO2satMax(Utility.parseFloat(mO2SatMax.getText().toString()));
		signoutPage.setO2satMin(Utility.parseFloat(mO2SatMin.getText().toString()));
		try {
			signoutPage.setPageDate(Utility.parseDate(mPageDate.getText().toString()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	private class TempratureTextChange implements TextWatcher {
		EditText mEditText;

		public TempratureTextChange(EditText editText) {
			this.mEditText = editText;
		}

		@Override
		public void afterTextChanged(Editable s) {
			try {
				if (Float.parseFloat(s.toString()) > 100.3)
					mEditText.setTextColor(Color.RED);
				else
					mEditText.setTextColor(Color.BLACK);
			} catch (NumberFormatException e) {
				mEditText.setTextColor(Color.BLACK);
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}

	}
}
