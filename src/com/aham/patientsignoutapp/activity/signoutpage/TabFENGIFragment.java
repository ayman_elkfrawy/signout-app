package com.aham.patientsignoutapp.activity.signoutpage;

import java.util.ArrayList;
import java.util.List;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.activity.editdialog.MedEditDialog;
import com.aham.patientsignoutapp.component.DialogEditableListAdapter;
import com.aham.patientsignoutapp.component.EditableListView;
import com.aham.patientsignoutapp.controller.PatientDelegate;
import com.aham.patientsignoutapp.util.Utility;
import com.aham.signout.shared.messaging.BMPlab;
import com.aham.signout.shared.messaging.LFTlab;
import com.aham.signout.shared.messaging.Lab;
import com.aham.signout.shared.messaging.Medication;
import com.aham.signout.shared.messaging.SignoutPage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class TabFENGIFragment extends Fragment implements BasicSignoutTab {
	
	Spinner mDiet;
	EditText mInsOut1;
	EditText mInsOut2;
	EditText mHours;
	EditText mUONum;
	EditText mUOHour;
	EditText mUOWeight;
	EditText mNotes;
	Spinner mIVF1;
	Spinner mIVF2;
	EditText mIVFnum;
	ViewGroup mBMP;
	ViewGroup mLFT;
	EditableListView mMeds;
	DialogEditableListAdapter<Medication> mMedsAdapter;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		
		View view = inflater.inflate(R.layout.signout_fengi, container, false);
		mDiet = (Spinner) view.findViewById(R.id.sp_Diet);
		mIVF1= (Spinner) view.findViewById(R.id.sp_IVF);
		mIVF2 = (Spinner) view.findViewById(R.id.sp_IVF2);
		mInsOut1 = (EditText) view.findViewById(R.id.et_InsOut1);
		mInsOut2 = (EditText) view.findViewById(R.id.et_InsOut2);
		mHours = (EditText) view.findViewById(R.id.et_Hours);
		mUONum = (EditText) view.findViewById(R.id.et_UONum);
		mUOHour = (EditText) view.findViewById(R.id.et_UOHour);
		mUOWeight = (EditText) view.findViewById(R.id.et_UOWeight);
		mNotes = (EditText) view.findViewById(R.id.et_Notes);
		mIVFnum = (EditText) view.findViewById(R.id.et_IVFnum);
		mBMP = (ViewGroup) view.findViewById(R.id.vg_lab_bmp);
		mLFT = (ViewGroup) view.findViewById(R.id.vg_lab_lft);
		mMeds = (EditableListView) view.findViewById(R.id.elv_Meds_fengi);
		
		mDiet.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, SignoutPage.diets));
		mIVF1.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, SignoutPage.ivf1List));
		mIVF2.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, SignoutPage.ivf2List));

		SignoutPage page = PatientDelegate.getInstance().getSelectedSignoutPage();
		if(page != null) updateGUI(page);
		
		return view;
	}

	@Override
	public void updateGUI(SignoutPage signoutPage) {
		Log.w("TEST", "Updating GUI " + this.getClass());
		mInsOut1.setText(Utility.numberToString(signoutPage.getInsOut1()));
		mInsOut2.setText(Utility.numberToString(signoutPage.getInsOut2()));
		mHours.setText(Utility.numberToString(signoutPage.getHours()));
		mUONum.setText(Utility.numberToString(signoutPage.getUoNum()));
		mUOHour.setText(Utility.numberToString(signoutPage.getUoHour()));
		mUOWeight.setText(Utility.numberToString(signoutPage.getUoweight()));
		mNotes.setText(signoutPage.getNotes());
		mIVFnum.setText(Utility.numberToString(signoutPage.getIvfNum()));
		
		mDiet.setSelection(Utility.getArrayItemPos(SignoutPage.diets, signoutPage.getDiet()));
		mIVF1.setSelection(Utility.getArrayItemPos(SignoutPage.ivf1List, signoutPage.getIvf1()));
		mIVF2.setSelection(Utility.getArrayItemPos(SignoutPage.ivf2List, signoutPage.getIvf1()));
		
		// Adding lab view
		List<Lab> labs = signoutPage.getLabsBySection(SignoutActivity.TAB_FENGI);
		boolean bmpFound = false;
		boolean lftFound = false;
		for (Lab lab : labs) {
			if(lab instanceof BMPlab) {
				lab.updateView(mBMP);
				bmpFound = true;
			} else if(lab instanceof LFTlab) {
				lab.updateView(mLFT);
				lftFound = true;
			}
		}
		if(!bmpFound) Utility.clearView(mBMP);
		if(!lftFound) Utility.clearView(mLFT);
		
		mMedsAdapter = new DialogEditableListAdapter<Medication>(getActivity(), signoutPage.getMedsBySection(Medication.FENGI), mMeds , 
									new MedEditDialog(Medication.FENGI));
	}

	@Override
	public void populatePage(SignoutPage signoutPage) {
		signoutPage.setInsOut1(Utility.parseFloat(mInsOut1.getText().toString()));
		signoutPage.setInsOut2(Utility.parseFloat(mInsOut2.getText().toString()));
		signoutPage.setHours(Utility.parseInt(mHours.getText().toString()));
		signoutPage.setUoNum(Utility.parseFloat(mUONum.getText().toString()));
		signoutPage.setUoHour(Utility.parseInt(mUOHour.getText().toString()));
		signoutPage.setUoweight(Utility.parseFloat(mUOWeight.getText().toString()));
		signoutPage.setIvfNum(Utility.parseFloat(mIVFnum.getText().toString()));
		signoutPage.setNotes(mNotes.getText().toString());
		signoutPage.setDiet(mDiet.getSelectedItem().toString());
		signoutPage.setDiet(mIVF1.getSelectedItem().toString());
		signoutPage.setDiet(mIVF2.getSelectedItem().toString());
		
		List<Lab> labs = signoutPage.getLabsBySection(SignoutActivity.TAB_FENGI);
		boolean bmpFound = false;
		boolean lftFound = false;
		for (Lab lab : labs) {
			if(lab instanceof BMPlab) {
				lab.populateLab(mBMP);
				bmpFound = true;
			} else if(lab instanceof LFTlab) {
				lab.populateLab(mLFT);
				lftFound = true;
			}
		}
		// Check if these labs wasn't exist
		List<Lab> newLabs = new ArrayList<Lab>();
		if(!bmpFound) { // no bmpLab found then Create new lab
			Lab bmpLab = new BMPlab();
			bmpLab.populateLab(mBMP);
			newLabs.add(bmpLab);
		}
		if(!lftFound) { // no bmpLab found then Create new lab
			Lab lftLab = new LFTlab();
			lftLab.populateLab(mLFT);
			newLabs.add(lftLab);
		}
		if(newLabs.size() > 0) signoutPage.addLabList(newLabs, SignoutActivity.TAB_FENGI);
		
		signoutPage.addMedList(mMedsAdapter.getListItems(), Medication.FENGI);
	}
	
}
