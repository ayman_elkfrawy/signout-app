package com.aham.patientsignoutapp.activity.signoutpage;

import java.util.ArrayList;
import java.util.List;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.activity.editdialog.MedEditDialog;
import com.aham.patientsignoutapp.component.DialogEditableListAdapter;
import com.aham.patientsignoutapp.component.EditableListView;
import com.aham.patientsignoutapp.controller.PatientDelegate;
import com.aham.patientsignoutapp.util.Utility;
import com.aham.signout.shared.messaging.ABGlab;
import com.aham.signout.shared.messaging.Lab;
import com.aham.signout.shared.messaging.Medication;
import com.aham.signout.shared.messaging.SignoutPage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class TabRESPFragment extends Fragment implements BasicSignoutTab {
	
	EditText mNC;
	ViewGroup mABGLab;
	EditableListView mMeds;
	DialogEditableListAdapter<Medication> mMedsAdapter;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		View view = inflater.inflate(R.layout.signout_resp, container, false);
		mNC = (EditText) view.findViewById(R.id.et_NC);
		mABGLab = (ViewGroup) view.findViewById(R.id.vg_lab_abg);
		mMeds = (EditableListView) view.findViewById(R.id.elv_Meds_resp);

		SignoutPage page = PatientDelegate.getInstance().getSelectedSignoutPage();
		if(page != null) updateGUI(page);
		
		return view;
	}

	@Override
	public void updateGUI(SignoutPage signoutPage) {
		mNC.setText(signoutPage.getNc() + "");
		List<Lab> currentLabs = signoutPage.getLabsBySection(SignoutActivity.TAB_RESP);
		if(currentLabs != null && !currentLabs.isEmpty()) currentLabs.get(0).updateView(mABGLab);
		else Utility.clearView(mABGLab);
		mMedsAdapter = new DialogEditableListAdapter<Medication>(getActivity(), signoutPage.getMedsBySection(Medication.RESP), mMeds,
									new MedEditDialog(Medication.RESP));
	}

	@Override
	public void populatePage(SignoutPage signoutPage) {
		signoutPage.setNc(Float.parseFloat(mNC.getText().toString()));
		List<Lab> currentLabs = signoutPage.getLabsBySection(SignoutActivity.TAB_RESP);
		if(currentLabs != null && !currentLabs.isEmpty()) currentLabs.get(0).populateLab(mABGLab);
		else { // Add new lab
			List<Lab> labs = new ArrayList<Lab>();
			Lab abgLab = new ABGlab();
			abgLab.populateLab(mABGLab);
			labs.add(abgLab);
			signoutPage.addLabList(labs, SignoutActivity.TAB_RESP);
		}
		signoutPage.addMedList(mMedsAdapter.getListItems(), Medication.RESP);
	}
}
