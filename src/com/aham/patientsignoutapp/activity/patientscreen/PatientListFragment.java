/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.aham.patientsignoutapp.activity.patientscreen;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.controller.PatientDelegate;
import com.aham.signout.shared.messaging.Patient;

public class PatientListFragment extends ListFragment {
	OnPatientSelectedListener mCallback;
	PatientListAdapter mListAdapter;
	View lastSelectedView=null;//to store last selected cell to be de-highlighted
	
	public interface OnPatientSelectedListener {
		public void onPatientSelected(int position);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		getListView().setBackgroundResource(R.drawable.so_list);
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onStart() {
		super.onStart();
		getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		LayoutInflater inflater=getActivity().getLayoutInflater();
		getListView().setEmptyView(inflater.inflate(R.layout.list_noresult, null));

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception.
		try {
			mCallback = (OnPatientSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnPatientSelectedListener");
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// Notify the parent activity of selected item
		mCallback.onPatientSelected(position);


		//Highlighting the selected item and retain the last selected  item  normal state
	    clearSelection();
	    lastSelectedView=v;
        v.setBackgroundResource(R.drawable.gradient_bg_hover);
	}
	
	public void clearSelection()
    {
        if(lastSelectedView != null) lastSelectedView.setBackgroundResource(R.drawable.gradient_bg);
    }
	
	public void setPatientList(List<Patient> patients) {
		mListAdapter = new PatientListAdapter(getActivity(), patients);
		setListAdapter(mListAdapter);
	}
	
	public Patient getSelectedPatient() {
		return PatientDelegate.getInstance().getSelectedPatient();
		//return mListAdapter.getItem(getSelectedItemPosition());
	}

	public void addPatient(Patient newPatient) {
		mListAdapter.insert(newPatient,0);
		mListAdapter.notifyDataSetChanged();
	}


	public void delete(Patient patientToDelete) {
		mListAdapter.remove(patientToDelete);
		PatientDelegate.getInstance().setmPatientsList(mListAdapter.getPatientList());
		mListAdapter.notifyDataSetChanged();
	}
	
	public void setPatient(int location, Patient patient) {
		mListAdapter.getPatientList().set(location, patient);
		PatientDelegate.getInstance().setmPatientsList(mListAdapter.getPatientList());
		mListAdapter.notifyDataSetChanged();
	}

}