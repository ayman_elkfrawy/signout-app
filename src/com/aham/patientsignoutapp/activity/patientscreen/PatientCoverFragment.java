/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.aham.patientsignoutapp.activity.patientscreen;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.activity.editdialog.SimpleEditDialog;
import com.aham.patientsignoutapp.component.DeletableItem;
import com.aham.patientsignoutapp.component.DialogEditableListAdapter;
import com.aham.patientsignoutapp.component.EditableListView;
import com.aham.patientsignoutapp.util.Utility;
import com.aham.signout.shared.messaging.CoverPage;
import com.aham.signout.shared.messaging.Patient;
import com.aham.signout.shared.messaging.PatientBasic;
import com.aham.signout.shared.messaging.SimpleListItem;

public class PatientCoverFragment extends Fragment {

	TextView mWeightView;
	TextView mAttendingOSView;
	TextView mPMDView;
	TextView mEDCourseView;
	TextView mPatientDescView;
	EditText mHospitalCourseView;
	EditableListView mProblemList;
	EditableListView mTodoList;
	EditableListView mONEventList;
	EditableListView mContingencyPlanList;
	DialogEditableListAdapter<SimpleListItem> mProblemListAdapter;
	DialogEditableListAdapter<SimpleListItem> mTodoListAdapter;
	DialogEditableListAdapter<SimpleListItem> mONEventListAdapter;
	DialogEditableListAdapter<SimpleListItem> mContingencyPlanListAdapter;
	
	Patient mCurrentPatient;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View v = inflater.inflate(R.layout.patientcover_view, container, false);

		mWeightView = (TextView) v.findViewById(R.id.tv_weight);
		mAttendingOSView = (TextView) v.findViewById(R.id.tv_attending);
		mPMDView = (TextView) v.findViewById(R.id.tv_pmd);
		mEDCourseView = (TextView) v.findViewById(R.id.tv_ed_course);
		mHospitalCourseView = (EditText) v.findViewById(R.id.et_hospital_course);
		mProblemList = (EditableListView) v.findViewById(R.id.elv_problem_list);
		mTodoList = (EditableListView) v.findViewById(R.id.elv_todo_list);
		mONEventList = (EditableListView) v.findViewById(R.id.elv_on_events);
		mContingencyPlanList = (EditableListView) v.findViewById(R.id.elv_contingency_plan);

		// adding onClick Listener To Description Text View
		mPatientDescView = (TextView) v.findViewById(R.id.tv_patient_desc);
		mPatientDescView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showEditDialog();
			}
		});
		
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			Button saveButton = (Button) v.findViewById(R.id.but_save_coverpage);
			saveButton.setVisibility(View.VISIBLE);
			saveButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if(getActivity() instanceof PatientActivity) {
						((PatientActivity) getActivity()).savePatient();
					}
				}
			});
		}

		return v;
	}

	public void updateCoverPageView(Patient patient) {
		mCurrentPatient = patient;
		getView().findViewById(R.id.ll_cover_page_container).setVisibility(View.VISIBLE);
		CoverPage coverpage = patient.getCoverPage();
		PatientBasic patientBasic = patient.getPatientBasicInfo();
		mWeightView.setText(patientBasic.getWeight() + " " + patientBasic.getWeightScale());
		mAttendingOSView.setText(patientBasic.getAttendingOnService());
		mPMDView.setText(patientBasic.getPmd());
		mEDCourseView.setText(patientBasic.getEdCourse());
		mHospitalCourseView.setText(coverpage.getHosbitalCourse());
		updatePatientDesc(patientBasic);

		mProblemListAdapter = new DialogEditableListAdapter<SimpleListItem>(getActivity(), coverpage.getProblems(), mProblemList,
				new SimpleEditDialog());
		mTodoListAdapter = new DialogEditableListAdapter<SimpleListItem>(getActivity(), coverpage.getToDoList(), mTodoList,
				new SimpleEditDialog());
		mONEventListAdapter = new DialogEditableListAdapter<SimpleListItem>(getActivity(), coverpage.getOnEvents(), mONEventList,
				new SimpleEditDialog());
		mContingencyPlanListAdapter = new DialogEditableListAdapter<SimpleListItem>(getActivity(), coverpage.getContingencyPlan(),
				mContingencyPlanList, new SimpleEditDialog());
	}

	private void updatePatientDesc(PatientBasic patientBasic) {
		mPatientDescView.setText(Html.fromHtml("<b>" + patientBasic.getFirstName() + "  " + patientBasic.getLastName() + "</b> is a <b>"
				+ Utility.calculateAge(patientBasic.getBirthDate(), new Date()) + "</b> years <b>" + patientBasic.getGender() + "</b> with PMH of <b>"
				+  arrayToString(patientBasic.getPmh()) + "</b>, who presents with <b>" + 
				arrayToString(patientBasic.getPw()) + "</b>, admitted for <b>" + arrayToString(patientBasic.getAdmittedFor()) + "</b>"));
	}
	
	private String arrayToString(List<?> list) {
		List<Object> newList = new ArrayList<Object>();
		for(Object item: list) {
			if(item instanceof DeletableItem && ((DeletableItem) item).isDeleted())
				continue;
			else newList.add(item);
		}
		return newList.toString().replaceAll("[\\[\\]]", "");
	}

	public String getHospitalCourse() {
		return mHospitalCourseView.getText().toString();
	}

	private void showEditDialog() {
		final Patient patientToChange = mCurrentPatient;
		//  show editable list view for admitted for
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Editing Admitted for");
		EditableListView editListView = (EditableListView) ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
														.inflate(R.layout.admitted_for_edit_dialog, null);
		// Get copy of current list of admitted for
		final List<SimpleListItem> admittedForCopy = copyOfList(patientToChange.getPatientBasicInfo().getAdmittedFor());
		new DialogEditableListAdapter<SimpleListItem>(getActivity(), admittedForCopy, editListView, new SimpleEditDialog());

		builder.setView(editListView);
		builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				patientToChange.getPatientBasicInfo().setAdmittedFor(admittedForCopy);
				updatePatientDesc(patientToChange.getPatientBasicInfo());
			}
		});
		
		builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {}
		});
		builder.create().show();
	}

	private List<SimpleListItem> copyOfList(List<SimpleListItem> admittedFor) {
		List<SimpleListItem> newList = new ArrayList<SimpleListItem>();
		for(SimpleListItem item: admittedFor) {
			newList.add((SimpleListItem) item.clone());
		}
		return newList;
	}

	public void populateCoverPage(CoverPage coverPage) {
		coverPage.setHosbitalCourse(getHospitalCourse());
		coverPage.setContingencyPlan(mContingencyPlanListAdapter.getListItems());
		coverPage.setOnEvents(mONEventListAdapter.getListItems());
		coverPage.setToDoList(mTodoListAdapter.getListItems());
		coverPage.setProblems(mProblemListAdapter.getListItems());
	}

}