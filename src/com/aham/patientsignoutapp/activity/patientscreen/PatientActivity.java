package com.aham.patientsignoutapp.activity.patientscreen;

import java.util.Date;
import java.util.List;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.activity.LoginActivity;
import com.aham.patientsignoutapp.activity.signoutpage.SignoutActivity;
import com.aham.patientsignoutapp.activity.userscreen.UserActivity;
import com.aham.patientsignoutapp.api.Core;
import com.aham.patientsignoutapp.controller.PatientDelegate;
import com.aham.patientsignoutapp.util.BackgroundTask;
import com.aham.patientsignoutapp.util.BackgroundWorker;
import com.aham.patientsignoutapp.util.CallBackResult;
import com.aham.patientsignoutapp.util.Utility;
import com.aham.signout.shared.messaging.CoverPage;
import com.aham.signout.shared.messaging.Patient;
import com.aham.signout.shared.messaging.User;

public class PatientActivity extends FragmentActivity implements PatientListFragment.OnPatientSelectedListener, OnMenuItemClickListener {

	PatientListFragment mPatientListFragment;
	PatientCoverFragment mCoverPageFragment;

	final static int REQUEST_CODE = 1;

	MenuItem mSignoutPagesItem;
	MenuItem mSaveItem;
	MenuItem mEditItem;
	MenuItem mDeleteItem;

	Dialog mDialog;

	/** Called when the activity is first created. */
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setProgressBarIndeterminateVisibility(false);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			Resources res = getResources();
			Drawable drawable = res.getDrawable(R.drawable.so_ab);
			getActionBar().setBackgroundDrawable(drawable);
		}
		setContentView(R.layout.patient_layout);

		loadPatientList();

		mPatientListFragment = (PatientListFragment) getSupportFragmentManager().findFragmentById(R.id.patient_list_fragment);
		mPatientListFragment.getListView().setEmptyView(findViewById(android.R.id.empty));
		mCoverPageFragment = (PatientCoverFragment) getSupportFragmentManager().findFragmentById(R.id.cover_fragment);
	}

	private void loadPatientList() {
		setProgressBarIndeterminateVisibility(true);
		mDialog = Utility.getProgressdialog(this);
		if(mSignoutPagesItem != null) mSignoutPagesItem.setEnabled(false);
		if(mSaveItem != null) mSaveItem.setEnabled(false);
		new BackgroundWorker<List<Patient>>(new BackgroundTask<List<Patient>>() {
			@Override
			public List<Patient> doInBackground() {
				return Core.getPatientList("", 0, Integer.MAX_VALUE, "");
			}
		}, new CallBackResult<List<Patient>>() {
			@Override
			public void callBack(List<Patient> result) {
				setProgressBarIndeterminateVisibility(false);
				if (mDialog != null)
					mDialog.dismiss();
				if (result != null) {
					if (mSignoutPagesItem != null) mSignoutPagesItem.setEnabled(true);
					mPatientListFragment.setPatientList(result);
					PatientDelegate.getInstance().setmPatientsList(result);
					if (result.size() > 0)
						onPatientSelected(0);
				} else {
					Toast.makeText(PatientActivity.this, R.string.error_connection, Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	public void onPatientSelected(int position) {
		PatientDelegate.getInstance().setSelectedPatientIndex(position);
		((ArrayAdapter) mPatientListFragment.getListAdapter()).notifyDataSetChanged();
		final Patient patientToChange = PatientDelegate.getInstance().getSelectedPatient();
		setProgressBarIndeterminateVisibility(true);
		mDialog = Utility.getProgressdialog(this);
		new BackgroundWorker<CoverPage>(new BackgroundTask<CoverPage>() {
			@Override
			public CoverPage doInBackground() {
				return Core.getCoverPage(mPatientListFragment.getSelectedPatient().getId());
			}
		}, new CallBackResult<CoverPage>() {
			@Override
			public void callBack(CoverPage result) {
				setProgressBarIndeterminateVisibility(false);
				if (mDialog != null)
					mDialog.dismiss();
				if (mSaveItem != null) mSaveItem.setEnabled(true);
				if (result != null) {
					patientToChange.setCoverPage(result);
					mCoverPageFragment.getView().setVisibility(View.VISIBLE);
					mCoverPageFragment.updateCoverPageView(patientToChange);
				} else {
					Toast.makeText(PatientActivity.this, R.string.error_connection, Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.patientcover_menu, menu);
		mSignoutPagesItem = menu.findItem(R.id.menu_signoutpages);
		mSignoutPagesItem.setOnMenuItemClickListener(this);
		mSaveItem = menu.findItem(R.id.menu_save);
		mSaveItem.setOnMenuItemClickListener(this);
		mEditItem = menu.findItem(R.id.menu_edit);
		mEditItem.setOnMenuItemClickListener(this);
		mDeleteItem = menu.findItem(R.id.menu_delete);
		mDeleteItem.setOnMenuItemClickListener(this);
		
		menu.findItem(R.id.menu_add).setOnMenuItemClickListener(this);
		menu.findItem(R.id.menu_user_screen).setOnMenuItemClickListener(this);
		menu.findItem(R.id.menu_logout).setOnMenuItemClickListener(this);

		// Check if user not admin to hide the user screen
		String userType = PatientDelegate.getInstance().getLoggedUser().getType();
		if (!(userType != null && userType.equalsIgnoreCase(User.ADMIN)))
			menu.findItem(R.id.menu_user_screen).setVisible(false);

		return true;
	}

	@Override
	public boolean onMenuItemClick(MenuItem item) {
		int viewID = item.getItemId();
		if (viewID == R.id.menu_user_screen) {
			Intent intent = new Intent(this, UserActivity.class);
			startActivity(intent);
			return true;
		} else if (viewID == R.id.menu_add) {
			PatientDelegate.getInstance().initNewPatient();
			Intent intent = new Intent(this, NewPatientActivity.class);
			intent.putExtra(NewPatientActivity.ADD_PATIENT, true);
			startActivityForResult(intent, REQUEST_CODE);
			return true;
		} else if (viewID == R.id.menu_save) {
			return savePatient();
		} else if (viewID == R.id.menu_signoutpages) {
			startActivity(new Intent(this, SignoutActivity.class));
			return true;
		} else if (viewID == R.id.menu_logout) {
			// Remove user password
			SharedPreferences sharedPref = getSharedPreferences(LoginActivity.SAVED_PREF_URI, Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = sharedPref.edit();
			editor.putString(LoginActivity.SAVED_PASSWORD_URI, "");
			editor.commit();
			// Start the login activity
			Intent intent = new Intent(this, LoginActivity.class);
			startActivity(intent);
			finish();
			return true;
		} else if(viewID == R.id.menu_delete) {
			// Pre condition
			if (PatientDelegate.getInstance().getPatientsList() == null ||
					PatientDelegate.getInstance().getPatientsList().size() == 0)
				return false;
			// Get current patient
			final Patient patientToDelete = PatientDelegate.getInstance().getSelectedPatient();
						
			// Show confirm message
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(R.string.delete_patient_title);
			builder.setMessage(Html.fromHtml(getString(R.string.delete_patient_message) + ": <b>" + 
					patientToDelete.getPatientBasicInfo().getFirstName() + " " + patientToDelete.getPatientBasicInfo()
																						.getLastName()+"</b>"));
			builder.setNegativeButton(R.string.cancel, new OnClickListener() {
				@Override
				public void onClick(DialogInterface arg0, int arg1) {}});
			builder.setPositiveButton(R.string.delete, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					patientToDelete.setTimeDeleted(new Date());
					deletePatient(patientToDelete);
				}

			});
			builder.create().show();
			return true;
		}  else if(viewID == R.id.menu_edit) {
			Intent intent = new Intent(this, NewPatientActivity.class);
			intent.putExtra(NewPatientActivity.ADD_PATIENT, false);
			startActivityForResult(intent, REQUEST_CODE);
			return true;
		} else
			return false;
	}
	
	private void deletePatient(final Patient patientToDelete) {
		setProgressBarIndeterminateVisibility(true);
		mDialog = Utility.getProgressdialog(this);
		new BackgroundWorker<Patient>(new BackgroundTask<Patient>() {
			@Override
			public Patient doInBackground() {
				return Core.addEditPatient(patientToDelete);
			}
		}, new CallBackResult<Patient>() {
			@Override
			public void callBack(Patient result) {
				setProgressBarIndeterminateVisibility(false);
				if (mDialog != null)
					mDialog.dismiss();
				if (result != null) {
					mPatientListFragment.delete(patientToDelete);
					if (PatientDelegate.getInstance().getPatientsList().size() > 0)
						onPatientSelected(0);
					else // Hide the Cover page
						mCoverPageFragment.getView().setVisibility(View.INVISIBLE);
				} else {
					Toast.makeText(PatientActivity.this, R.string.error_connection, Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	public boolean savePatient() {
		final Patient patientToSave = PatientDelegate.getInstance().getSelectedPatient();
		patientToSave.setSignoutPages(null);
		mCoverPageFragment.populateCoverPage(patientToSave.getCoverPage());
		savePatient(patientToSave);
		return true;
	}

	private void savePatient(final Patient patientToSave) {
		setProgressBarIndeterminateVisibility(true);
		mDialog = Utility.getProgressdialog(this);
		new BackgroundWorker<Patient>(new BackgroundTask<Patient>() {
			@Override
			public Patient doInBackground() {
				return Core.addEditPatient(patientToSave);
			}
		}, new CallBackResult<Patient>() {
			@Override
			public void callBack(Patient result) {
				setProgressBarIndeterminateVisibility(false);
				if (mDialog != null)
					mDialog.dismiss();
				if (result != null) {
					// Replace current patient with the new patient
					patientToSave.replace(result);
					// Update cover page if current patient is currently visible
					if (PatientDelegate.getInstance().getSelectedPatient() == patientToSave)
						onPatientSelected(PatientDelegate.getInstance().getSelectedPatientIndex());
				} else {
					Toast.makeText(PatientActivity.this, R.string.error_connection, Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent arg2) {
		if (requestCode == REQUEST_CODE) {
			if (resultCode == NewPatientActivity.RESULT_OK_ADD) {
				mPatientListFragment.addPatient(PatientDelegate.getInstance().getNewPatient());
				onPatientSelected(0);
			} else if(resultCode == NewPatientActivity.RESULT_OK_EDIT) {
				mPatientListFragment.setPatient(PatientDelegate.getInstance().getSelectedPatientIndex(), 
														PatientDelegate.getInstance().getNewPatient());
				onPatientSelected(PatientDelegate.getInstance().getSelectedPatientIndex());
			}
		}
	}
}