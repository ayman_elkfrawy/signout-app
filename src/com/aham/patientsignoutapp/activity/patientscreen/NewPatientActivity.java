package com.aham.patientsignoutapp.activity.patientscreen;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View.OnClickListener;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.activity.editdialog.LabEditDialog;
import com.aham.patientsignoutapp.activity.editdialog.MedEditDialog;
import com.aham.patientsignoutapp.activity.editdialog.RadEditDialog;
import com.aham.patientsignoutapp.activity.editdialog.SimpleEditDialog;
import com.aham.patientsignoutapp.activity.editdialog.StringEditDialog;
import com.aham.patientsignoutapp.api.Core;
import com.aham.patientsignoutapp.component.DialogEditableListAdapter;
import com.aham.patientsignoutapp.component.EditableListView;
import com.aham.patientsignoutapp.controller.PatientDelegate;
import com.aham.patientsignoutapp.util.BackgroundTask;
import com.aham.patientsignoutapp.util.BackgroundWorker;
import com.aham.patientsignoutapp.util.CallBackResult;
import com.aham.patientsignoutapp.util.Utility;
import com.aham.signout.shared.messaging.ApplicationVariables;
import com.aham.signout.shared.messaging.Lab;
import com.aham.signout.shared.messaging.Medication;
import com.aham.signout.shared.messaging.Patient;
import com.aham.signout.shared.messaging.PatientBasic;
import com.aham.signout.shared.messaging.Radiology;
import com.aham.signout.shared.messaging.SimpleListItem;

;

public class NewPatientActivity extends FragmentActivity implements DatePickerDialog.OnDateSetListener, OnMenuItemClickListener {

	TextView mDOB;

	EditableListView mPMHView;
	EditableListView mPWView;
	EditableListView mAdmittedForView;
	EditableListView mAllergiesView;
	EditableListView mHomeMedsView;

	EditableListView mLabsView;
	EditableListView mRadiologyView;
	EditableListView mAdmittingDiagView;
	EditableListView mMedsView;

	MenuItem mSaveButton;

	Patient mPatient;
	
	Dialog mDialog;
	
	public static final String ADD_PATIENT = "add_new_patient";
	public static final int RESULT_OK_ADD = 1;
	public static final int RESULT_OK_EDIT = 2;
	boolean mAddingNew;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			Resources res = getResources();
			Drawable drawable = res.getDrawable(R.drawable.so_ab);
			getActionBar().setBackgroundDrawable(drawable);
		}
		
		setContentView(R.layout.new_patient);
		setProgressBarIndeterminateVisibility(false);

		mDOB = (TextView) findViewById(R.id.et_dob);
		mPMHView = (EditableListView) findViewById(R.id.elv_pmh);
		mPWView = (EditableListView) findViewById(R.id.elv_pw);
		mAdmittedForView = (EditableListView) findViewById(R.id.elv_admitteedfor);
		mAllergiesView = (EditableListView) findViewById(R.id.elv_allergies);
		mHomeMedsView = (EditableListView) findViewById(R.id.elv_home_meds);

		mLabsView = (EditableListView) findViewById(R.id.elv_labs);
		mRadiologyView = (EditableListView) findViewById(R.id.elv_radiology);
		mAdmittingDiagView = (EditableListView) findViewById(R.id.elv_admitting_diag);
		mMedsView = (EditableListView) findViewById(R.id.elv_meds);
		
		// Check if edit or add new patient
		mAddingNew = getIntent().getExtras().getBoolean(ADD_PATIENT);
		if (mAddingNew) {
			mPatient = PatientDelegate.getInstance().getNewPatient();
		} else {
			mPatient = PatientDelegate.getInstance().getSelectedPatient();
			updateGUI();
		}

		new DialogEditableListAdapter<String>(this, mPatient.getPatientBasicInfo().getPmh(), mPMHView, new StringEditDialog());
		new DialogEditableListAdapter<String>(this, mPatient.getPatientBasicInfo().getPw(), mPWView, new StringEditDialog());
		new DialogEditableListAdapter<SimpleListItem>(this, mPatient.getPatientBasicInfo().getAdmittedFor(), mAdmittedForView,
				new SimpleEditDialog());
		new DialogEditableListAdapter<String>(this, mPatient.getPatientBasicInfo().getAllergies(), mAllergiesView,
				new StringEditDialog());
		new DialogEditableListAdapter<String>(this, mPatient.getPatientBasicInfo().getHomeMedications(), mHomeMedsView,
				new StringEditDialog());
		new DialogEditableListAdapter<String>(this, mPatient.getPatientBasicInfo().getAdmittingDiagnosis(), mAdmittingDiagView,
				new StringEditDialog());
		if (mAddingNew) {
			new DialogEditableListAdapter<Lab>(this, mPatient.getSignoutPages().get(0).getAllLabs(), mLabsView, new LabEditDialog());
			new DialogEditableListAdapter<Radiology>(this, mPatient.getSignoutPages().get(0).getRadiologies(), mRadiologyView,
					new RadEditDialog());
			new DialogEditableListAdapter<Medication>(this, mPatient.getSignoutPages().get(0).getAllMeds(), mMedsView, new MedEditDialog(
					null));
		} else {
			mLabsView.setVisibility(View.GONE);
			mRadiologyView.setVisibility(View.GONE);
			mMedsView.setVisibility(View.GONE);
		}
		updateAutoCompleteEditTexts();

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			Button saveButton = (Button) findViewById(R.id.but_add_new_patient);
			saveButton.setVisibility(View.VISIBLE);
			saveButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// Save changes
					if (validateInput()) {
						saveCurrentPatient();
					}
				}
			});
		}
	}

	private void updateGUI() {
		// //******** Basic info ************////
		PatientBasic basicInfo = mPatient.getPatientBasicInfo();
		// Date fields
		((TextView) findViewById(R.id.et_dob)).setText(Utility.formatDate(basicInfo.getBirthDate()));
		// ((TextView) findViewById(R.id.et_h)).setText(Utility.formatDate(basicInfo.getHostingDate()));
		
		// Float fields
		((TextView) findViewById(R.id.et_bp1)).setText(basicInfo.getBp1() + "");
		((TextView) findViewById(R.id.et_bp2)).setText(basicInfo.getBp2() + "");
		((TextView) findViewById(R.id.et_hr)).setText(basicInfo.getHr() + "");
		((TextView) findViewById(R.id.et_height)).setText(basicInfo.getHeight() + "");
		((TextView) findViewById(R.id.et_o2sat)).setText(basicInfo.getO2sat() + "");
		((TextView) findViewById(R.id.et_temp)).setText(basicInfo.getTemp() + "");
		((TextView) findViewById(R.id.et_weight)).setText(basicInfo.getWeight() + "");
		((TextView) findViewById(R.id.et_head_circum)).setText(basicInfo.getHeadCircumference() + "");
		
		// String fields
		((TextView) findViewById(R.id.et_ed_course)).setText(basicInfo.getEdCourse());
		((TextView) findViewById(R.id.et_firstname)).setText(basicInfo.getFirstName());
		((TextView) findViewById(R.id.et_middlename)).setText(basicInfo.getMiddleName());
		((TextView) findViewById(R.id.et_lastname)).setText(basicInfo.getLastName());
		((TextView) findViewById(R.id.et_attending_os_text)).setText(basicInfo.getAttendingOnService());
		((TextView) findViewById(R.id.et_pmd_text)).setText(basicInfo.getPmd());

		// Spinner fields
		Spinner spGender = (Spinner) findViewById(R.id.sp_gender);
		Spinner spWeightScale = (Spinner) findViewById(R.id.sp_weight_scale);
		Spinner spHeightScale = (Spinner) findViewById(R.id.sp_height_scale);
		Spinner spCircScale = (Spinner) findViewById(R.id.sp_head_circ_scale);
		
		spGender.setSelection(((ArrayAdapter) spGender.getAdapter()).getPosition(basicInfo.getGender()));
		spWeightScale.setSelection(((ArrayAdapter) spWeightScale.getAdapter()).getPosition(basicInfo.getWeightScale()));
		spHeightScale.setSelection(((ArrayAdapter) spHeightScale.getAdapter()).getPosition(basicInfo.getHeightScale()));
		spCircScale.setSelection(((ArrayAdapter) spCircScale.getAdapter()).getPosition(basicInfo.getCircScale()));
	}

	private void updateAutoCompleteEditTexts() {
		new BackgroundWorker<ApplicationVariables>(new BackgroundTask<ApplicationVariables>() {
			@Override
			public ApplicationVariables doInBackground() {
				return Core.getApplicationVariables();
			}
		}, new CallBackResult<ApplicationVariables>() {
			@Override
			public void callBack(ApplicationVariables result) {
				if (result != null) {
					((AutoCompleteTextView) findViewById(R.id.et_pmd_text)).setAdapter(new ArrayAdapter<String>(NewPatientActivity.this,
							android.R.layout.simple_spinner_dropdown_item, result.getPmd()));
					((AutoCompleteTextView) findViewById(R.id.et_attending_os_text)).setAdapter(new ArrayAdapter<String>(
							NewPatientActivity.this, android.R.layout.simple_spinner_dropdown_item, result.getAttendingOnService()));
				} else {
					Toast.makeText(NewPatientActivity.this, R.string.save_patient_problem, Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
		mDOB.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
	}

	public void showDatePicker(View view) {
		Calendar calendar = Calendar.getInstance();
		int yy = calendar.get(Calendar.YEAR);
		int mm = calendar.get(Calendar.MONTH);
		int dd = calendar.get(Calendar.DAY_OF_MONTH);
		new DatePickerDialog(this, this, yy, mm, dd).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.new_patient_menu, menu);
		mSaveButton = menu.findItem(R.id.mi_save);
		mSaveButton.setOnMenuItemClickListener(this);
		
		if(!mAddingNew)
			mSaveButton.setTitle(R.string.save_changes);
		
		return true;
	}

	@Override
	public boolean onMenuItemClick(MenuItem item) {
		if (item.getItemId() == R.id.mi_save) {
			// Save changes
			if (validateInput()) {
				saveCurrentPatient();
			}
		}
		return false;
	}

	private void saveCurrentPatient() {
		// Populate and check problems
		if (!populateCurrentPatient())
			return;
		// Start saving
		setProgressBarIndeterminateVisibility(true);
		mDialog = Utility.getProgressdialog(this);
		if(mSaveButton != null) mSaveButton.setEnabled(false);
		new BackgroundWorker<Patient>(new BackgroundTask<Patient>() {
			@Override
			public Patient doInBackground() {
				if(mAddingNew)
					return PatientDelegate.getInstance().postNewPatient();
				else {
					mPatient.setCoverPage(null);
					mPatient.setSignoutPages(null);
					return Core.addEditPatient(mPatient);
				}
			}
		}, new CallBackResult<Patient>() {
			@Override
			public void callBack(Patient result) {
				setProgressBarIndeterminateVisibility(false);
				if (mDialog != null) mDialog.dismiss();
				if (mSaveButton != null) mSaveButton.setEnabled(true);
				if (result != null) {
					PatientDelegate.getInstance().setNewPatient(result);
					if (mAddingNew) setResult(RESULT_OK_ADD); // New patient added successfully
					else setResult(RESULT_OK_EDIT); // patient saved successfully
					finish();
				} else {
					Toast.makeText(NewPatientActivity.this, R.string.save_patient_problem, Toast.LENGTH_LONG).show();
				}
			}
		});

	}

	private boolean populateCurrentPatient() {
		// //******** Basic info ************////
		PatientBasic basicInfo = mPatient.getPatientBasicInfo();
		// Date fields
		try {
			basicInfo.setBirthDate(Utility.parseDate(((TextView) findViewById(R.id.et_dob)).getText().toString()));
			basicInfo.setHostingDate(new Date()); // Set hosting date is now!
		} catch (ParseException e) {
			findViewById(R.id.et_dob).requestFocus();
			((EditText) findViewById(R.id.et_dob)).setError(getText(R.string.error_date));
			return false;
		}

		// Float fields
		basicInfo.setBp1(Float.parseFloat(((TextView) findViewById(R.id.et_bp1)).getText().toString()));
		basicInfo.setBp2(Float.parseFloat(((TextView) findViewById(R.id.et_bp2)).getText().toString()));
		basicInfo.setHr(Float.parseFloat(((TextView) findViewById(R.id.et_hr)).getText().toString()));
		basicInfo.setHeight(Float.parseFloat(((TextView) findViewById(R.id.et_height)).getText().toString()));
		basicInfo.setO2sat(Float.parseFloat(((TextView) findViewById(R.id.et_o2sat)).getText().toString()));
		basicInfo.setTemp(Float.parseFloat(((TextView) findViewById(R.id.et_temp)).getText().toString()));
		basicInfo.setWeight(Float.parseFloat(((TextView) findViewById(R.id.et_weight)).getText().toString()));
		basicInfo.setHeadCircumference(Float.parseFloat(((TextView) findViewById(R.id.et_head_circum)).getText().toString()));
		// String fields
		basicInfo.setEdCourse(((TextView) findViewById(R.id.et_ed_course)).getText().toString());
		basicInfo.setFirstName(((TextView) findViewById(R.id.et_firstname)).getText().toString());
		basicInfo.setMiddleName(((TextView) findViewById(R.id.et_middlename)).getText().toString());
		basicInfo.setLastName(((TextView) findViewById(R.id.et_lastname)).getText().toString());
		basicInfo.setAttendingOnService(((TextView) findViewById(R.id.et_attending_os_text)).getText().toString());
		basicInfo.setPmd(((TextView) findViewById(R.id.et_pmd_text)).getText().toString());
		// Spinner fields
		basicInfo.setGender(((Spinner) findViewById(R.id.sp_gender)).getSelectedItem().toString());
		basicInfo.setWeightScale(((Spinner) findViewById(R.id.sp_weight_scale)).getSelectedItem().toString());
		basicInfo.setHeightScale(((Spinner) findViewById(R.id.sp_height_scale)).getSelectedItem().toString());
		basicInfo.setCircScale(((Spinner) findViewById(R.id.sp_head_circ_scale)).getSelectedItem().toString());

		return true;
	}

	private boolean validateInput() {
		return !Utility.checkForEmptyInput(this.findViewById(android.R.id.content));
	}

}
