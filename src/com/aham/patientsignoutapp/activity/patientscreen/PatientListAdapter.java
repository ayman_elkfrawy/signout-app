package com.aham.patientsignoutapp.activity.patientscreen;

import java.util.Date;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.controller.PatientDelegate;
import com.aham.patientsignoutapp.util.Utility;
import com.aham.signout.shared.messaging.Patient;
import com.aham.signout.shared.messaging.PatientBasic;

public class PatientListAdapter extends ArrayAdapter<Patient> {

	List<Patient> mPatientList;

	public PatientListAdapter(Context context, List<Patient> objects) {
		super(context, R.layout.patient_list_row, objects);
		this.mPatientList = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null)
			convertView = ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
					R.layout.patient_list_row, null);
		// To highlight selected row
		if (position == PatientDelegate.getInstance().getSelectedPatientIndex())
			convertView.setBackgroundResource(R.drawable.gradient_bg_hover);
		else
			convertView.setBackgroundResource(R.drawable.gradient_bg);

		PatientBasic currentPatient = mPatientList.get(position).getPatientBasicInfo();
		((TextView) convertView.findViewById(R.id.tv_patient_name)).setText(currentPatient.getFirstName() + " "
				+ currentPatient.getMiddleName() + " " + currentPatient.getLastName());
		// Calculate age
		int age = Utility.calculateAge(currentPatient.getBirthDate(), new Date());
		((TextView) convertView.findViewById(R.id.tv_birth_date)).setText(Utility.formatDate(currentPatient.getBirthDate()) + " (" + age
				+ ")");

		// Get admitting diag
		String admitDiag = "";
		for (String string : currentPatient.getAdmittingDiagnosis())
			admitDiag += string + ", ";
		((TextView) convertView.findViewById(R.id.tv_admit_diag)).setText(admitDiag);

		// Calculate hosting days
		long today = new Date().getTime();
		long hostingDay = currentPatient.getHostingDate().getTime();
		int days = (int) ((today - hostingDay) / 86400000) + 1;
		((TextView) convertView.findViewById(R.id.tv_duration)).setText(days + " DOH");

		return convertView;
	}
	
	public List<Patient> getPatientList() {
		return mPatientList;
	}
}