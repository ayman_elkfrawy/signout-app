package com.aham.patientsignoutapp.activity.editdialog;


import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.component.EditItemDialog;
import com.aham.patientsignoutapp.util.Utility;
import com.aham.signout.shared.messaging.SimpleListItem;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;


public class SimpleEditDialog extends EditItemDialog<SimpleListItem> {
	EditText mEditString;

	@Override
	public View getContentView(SimpleListItem item) {
		View view =  ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.simple_edit_dialog, null);
		//View view = getLayoutInflater(null).inflate(R.layout.simple_edit_dialog, null);
		mEditString = (EditText) view.findViewById(R.id.etInputString);
		if (!isAdd())
			mEditString.setText(item.getText());
		setItem(new SimpleListItem());
		return view;
	}

	@Override
	public SimpleListItem generateItem() {
		if (Utility.checkForEmptyInput(mEditString)) {
			return null;
		}
		getItem().setText(mEditString.getText().toString());
		return getItem();
	}

}
