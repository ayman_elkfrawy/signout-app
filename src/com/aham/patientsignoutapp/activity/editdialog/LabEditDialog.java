package com.aham.patientsignoutapp.activity.editdialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.component.EditItemDialog;
import com.aham.patientsignoutapp.util.Utility;
import com.aham.signout.shared.messaging.ABGlab;
import com.aham.signout.shared.messaging.BCxLab;
import com.aham.signout.shared.messaging.BMPlab;
import com.aham.signout.shared.messaging.CBClab;
import com.aham.signout.shared.messaging.CSFCxLab;
import com.aham.signout.shared.messaging.CoagultionLab;
import com.aham.signout.shared.messaging.LFTlab;
import com.aham.signout.shared.messaging.Lab;
import com.aham.signout.shared.messaging.OtherLab;
import com.aham.signout.shared.messaging.UCxLab;

public class LabEditDialog extends EditItemDialog<Lab> implements OnItemSelectedListener, OnDateSetListener,
		OnClickListener {

	ViewGroup mLabWrapper;
	Spinner mLabSpinner;
	EditText mLabDate;

	String[] labGroups = { Lab.ABG, Lab.BMP, Lab.CBC, Lab.COAGULTION, Lab.BCX, Lab.UCX, Lab.CSFCX, Lab.LFT, Lab.OTHER };

	@Override
	public View getContentView(Lab item) {
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.labs_dialog, null, false);

		mLabWrapper = (ViewGroup) view.findViewById(R.id.vg_lab_content);
		mLabSpinner = (Spinner) view.findViewById(R.id.sp_lab_groups);
		mLabDate = (EditText) view.findViewById(R.id.et_date);

		mLabDate.setOnClickListener(this);
		// Check if user is editing an existing item or add new one
		if (isAdd()) { // Add new item
			mLabSpinner.setAdapter(new ArrayAdapter<String>(getActivity(),
					android.R.layout.simple_spinner_dropdown_item, labGroups));
			mLabSpinner.setOnItemSelectedListener(this);
			// Set current date
			Calendar calendar = Calendar.getInstance();
			onDateSet(null, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
					calendar.get(Calendar.DAY_OF_MONTH));

		} else { // Edit existing one
			mLabSpinner.setVisibility(View.GONE); // Hide spinner
			view.findViewById(R.id.tv_select_lab).setVisibility(View.GONE);
			addLabView(item, item.getGroup()); // Add that item
			mLabDate.setText(Utility.formatDate(item.getDate()));
		}

		return view;
	}

	@Override
	public Lab generateItem() {
		mLabDate.setError(null);
		try {
			getItem().setDate(Utility.parseDate(mLabDate.getText().toString()));
			// Check empty fields
			if (Utility.checkForEmptyInput(mLabWrapper)) {
				return null;
			}
			getItem().populateLab(mLabWrapper);
		} catch (ParseException e) {
			mLabDate.setError(getString(R.string.error_date));
			mLabDate.requestFocus();
			return null;
		}
		return getItem();
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		addLabView(null, labGroups[position]);
	}

	private void addLabView(Lab lab, String labType) {
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = null;
		// inflate the lab view group
		if (labType.equalsIgnoreCase(Lab.ABG)) {
			view = inflater.inflate(R.layout.lab_abg, mLabWrapper, false);
			setItem(new ABGlab());
		} else if (labType.equalsIgnoreCase(Lab.BCX)) {
			view = inflater.inflate(R.layout.lab_bcx, mLabWrapper, false);
			setItem(new BCxLab());
		} else if (labType.equalsIgnoreCase(Lab.BMP)) {
			view = inflater.inflate(R.layout.lab_bmp, mLabWrapper, false);
			setItem(new BMPlab());
		} else if (labType.equalsIgnoreCase(Lab.CBC)) {
			view = inflater.inflate(R.layout.lab_cbc, mLabWrapper, false);
			setItem(new CBClab());
		} else if (labType.equalsIgnoreCase(Lab.COAGULTION)) {
			view = inflater.inflate(R.layout.lab_coagulation, mLabWrapper, false);
			setItem(new CoagultionLab());
		} else if (labType.equalsIgnoreCase(Lab.CSFCX)) {
			view = inflater.inflate(R.layout.lab_ucx, mLabWrapper, false);
			setItem(new CSFCxLab());
		} else if (labType.equalsIgnoreCase(Lab.LFT)) {
			view = inflater.inflate(R.layout.lab_lft, mLabWrapper, false);
			setItem(new LFTlab());
		} else if (labType.equalsIgnoreCase(Lab.OTHER)) {
			view = inflater.inflate(R.layout.lab_other, mLabWrapper, false);
			setItem(new OtherLab());
		} else if (labType.equalsIgnoreCase(Lab.UCX)) {
			view = inflater.inflate(R.layout.lab_ucx, mLabWrapper, false);
			setItem(new UCxLab());
		}
		// Update the view with the lab if lab exist
		if (lab != null && view != null)
			lab.updateView(view);

		// Add the lab view group to the wrapper
		mLabWrapper.removeAllViews();
		mLabWrapper.addView(view);
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub

	}

	public void showDatePicker() {
		Calendar calendar = Calendar.getInstance();
		int yy = calendar.get(Calendar.YEAR);
		int mm = calendar.get(Calendar.MONTH);
		int dd = calendar.get(Calendar.DAY_OF_MONTH);
		new DatePickerDialog(getActivity(), this, yy, mm, dd).show();
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
		mLabDate.setText(dayOfMonth + "/" + (monthOfYear+1) + "/" + year);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == mLabDate.getId()) {
			showDatePicker();
		}
	}

}
