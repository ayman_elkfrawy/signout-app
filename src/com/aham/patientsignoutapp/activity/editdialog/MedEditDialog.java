package com.aham.patientsignoutapp.activity.editdialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.component.EditItemDialog;
import com.aham.patientsignoutapp.util.Utility;
import com.aham.signout.shared.messaging.Medication;

@SuppressLint("ValidFragment")
public class MedEditDialog extends EditItemDialog<Medication> {

	String[] medCategories = { Medication.FENGI, Medication.RESP, Medication.CV, Medication.HEME_ONC, Medication.ID,
			Medication.NEUROLOGY };

	Spinner mMedCatSpinner;
	EditText mMedName;
	TextView mMedGroupTV;
	String mMedGroup;

	public MedEditDialog(String medGroup) {
		mMedGroup = medGroup;
	}

	@Override
	public View getContentView(Medication item) {
		View view = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
				R.layout.meds_dialog, null);
		mMedCatSpinner = (Spinner) view.findViewById(R.id.sp_med_cat);
		mMedName = (EditText) view.findViewById(R.id.et_med_name);
		mMedGroupTV = (TextView) view.findViewById(R.id.tv_med_group);

		mMedCatSpinner.setAdapter(new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_spinner_dropdown_item, medCategories));
		if (!isAdd()) // Edit existing item
			mMedCatSpinner.setSelection(Utility.getArrayItemPos(medCategories, item.getGroup()));
		if (mMedGroup != null) {
			mMedGroupTV.setText("Med group: " + mMedGroup);
			mMedCatSpinner.setVisibility(View.GONE);
		} else {
			mMedCatSpinner.setVisibility(View.VISIBLE);
		}

		if (!isAdd()) { // Edit existing item
			mMedName.setText(item.getName());
		}
		
		setItem(new Medication());

		return view;
	}

	@Override
	public Medication generateItem() {
		// Check empty fields
		if (Utility.checkForEmptyInput(mMedName)) {
			return null;
		}
		// Populate object with UI
		if(mMedGroup == null)
			getItem().setGroup(mMedCatSpinner.getSelectedItem().toString());
		else
			getItem().setGroup(mMedGroup);
		
		getItem().setName(mMedName.getText().toString());

		return getItem();
	}
}
