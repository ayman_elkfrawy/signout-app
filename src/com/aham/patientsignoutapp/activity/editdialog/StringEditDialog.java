package com.aham.patientsignoutapp.activity.editdialog;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.component.EditItemDialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;


public class StringEditDialog extends EditItemDialog<String> {
	EditText mEditString;

	@Override
	public View getContentView(String item) {
		View view =  ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.simple_edit_dialog, null);
		//View view = getLayoutInflater(null).inflate(R.layout.simple_edit_dialog, null);
		mEditString = (EditText) view.findViewById(R.id.etInputString);
		if (!isAdd())
			mEditString.setText(item);
		return view;
	}

	@Override
	public String generateItem() {
		return mEditString.getText().toString();
	}

}
