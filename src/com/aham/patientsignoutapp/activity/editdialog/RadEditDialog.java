package com.aham.patientsignoutapp.activity.editdialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.component.EditItemDialog;
import com.aham.patientsignoutapp.util.Utility;
import com.aham.signout.shared.messaging.Radiology;

public class RadEditDialog extends EditItemDialog<Radiology> implements OnDateSetListener, OnItemSelectedListener {

	String[] radGroups = { Radiology.CXR, Radiology.KUB, Radiology.OBS_SERIES, Radiology.US_KIDNEY, Radiology.US_ABD,
			Radiology.US_TESTES, Radiology.CT_HEAD, Radiology.CT_ABD, Radiology.MRI_HEAD, Radiology.OTHER };
	
	Spinner mRadGroup;
	EditText mRadTitle;
	EditText mRadResult;
	EditText mRadDate;
	
	@Override
	public View getContentView(Radiology item) {
		View view = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
				R.layout.radiology_dialog, null);
		mRadGroup = (Spinner) view.findViewById(R.id.sp_rad_group);
		mRadTitle = (EditText) view.findViewById(R.id.et_rad_title);
		mRadResult = (EditText) view.findViewById(R.id.et_rad_result);
		mRadDate = (EditText) view.findViewById(R.id.et_date);
		
		mRadGroup.setAdapter(new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_spinner_dropdown_item, radGroups));
		mRadGroup.setOnItemSelectedListener(this);

		if (isAdd()) { // Add new item
			Calendar calendar = Calendar.getInstance();
			onDateSet(null, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
			
		} else { // Edit existing one
			int index = Utility.getArrayItemPos(radGroups, item.getName());
			if(index >= 0 ) { // Radiology other than "Other"
				mRadGroup.setSelection(index);
			} else { // Other radiology
				mRadGroup.setSelection(radGroups.length - 1);
				// mRadTitle.setVisibility(View.VISIBLE);
				mRadTitle.setText(item.getName());
			}
			mRadResult.setText(item.getResult());
			mRadDate.setText(Utility.formatDate(item.getDate()));
		}
		setItem(new Radiology());

		return view;
	}

	@Override
	public Radiology generateItem() {
		mRadDate.setError(null);
		try {
			getItem().setDate(Utility.parseDate(mRadDate.getText().toString()));
			// Check empty fields
			if (Utility.checkForEmptyInput(mRadResult)) {
				return null;
			}
			if (mRadTitle.getVisibility() == View.VISIBLE && Utility.checkForEmptyInput(mRadTitle)) {
				return null;
			}
			// Populate object with UI
			getItem().setResult(mRadResult.getText().toString());
			if (mRadTitle.getVisibility() == View.VISIBLE) {
				getItem().setName(mRadTitle.getText().toString());
			} else {
				getItem().setName(mRadGroup.getSelectedItem().toString());
			}
			
		} catch (ParseException e) {
			mRadDate.setError(getString(R.string.error_date));
			mRadDate.requestFocus();
			return null;
		}
		return getItem();
	}
	
	public void showDatePicker() {
		Calendar calendar = Calendar.getInstance();
		int yy = calendar.get(Calendar.YEAR);
		int mm = calendar.get(Calendar.MONTH);
		int dd = calendar.get(Calendar.DAY_OF_MONTH);
		new DatePickerDialog(getActivity(), this, yy, mm, dd).show();
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
		mRadDate.setText(dayOfMonth + "/" + monthOfYear + "/" + year);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		if (radGroups[position].equalsIgnoreCase(Radiology.OTHER)) {
			mRadTitle.setVisibility(View.VISIBLE);
		} else mRadTitle.setVisibility(View.GONE);
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}

}
