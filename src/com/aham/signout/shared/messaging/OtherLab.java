package com.aham.signout.shared.messaging;

import android.view.View;
import android.widget.EditText;

import com.aham.patientsignoutapp.R;

public class OtherLab extends Lab {
	String labName;
	String result;
	
	@Override
	public String[] getFieldsNames() {
		String[] values = {"Result"};
		return values;
	}
	@Override
	public String[] getFieldsValues() {
		String[] values = {result};
		return values;
	}
	
	public String getLabName() {
		return labName;
	}
	public void setLabName(String labName) {
		this.labName = labName;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getGroup(){
		return OTHER;
	}
	
	@Override
	public void updateView(View view) {
		((EditText) view.findViewById(R.id.et_other_title)).setText(labName);
		((EditText) view.findViewById(R.id.et_other_result)).setText(result);
	}
	@Override
	public void populateLab(View view) {
		labName = ((EditText) view.findViewById(R.id.et_other_title)).getText().toString();
		result = ((EditText) view.findViewById(R.id.et_other_result)).getText().toString();
	}
	
	
	@Override
	public void replace(Object object) {
		OtherLab lab = (OtherLab) object;
		this.date = lab.date;
		this.deleted = lab.deleted;
		this.deleteDate = lab.deleteDate;
		this.labName = lab.labName;
		this.result = lab.result;
	}
	
}
