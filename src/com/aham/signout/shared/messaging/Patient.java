package com.aham.signout.shared.messaging;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aham.patientsignoutapp.component.Replaceable;


public class Patient implements Replaceable{
	
	long id;
	
	PatientBasic patientBasicInfo;
	
	List<SignoutPage> signoutPages=new ArrayList<SignoutPage>();
	
	CoverPage coverPage;
	
	Date timeDeleted;
	
	
	
	public Date getTimeDeleted() {
		return timeDeleted;
	}

	public void setTimeDeleted(Date timeDeleted) {
		this.timeDeleted = timeDeleted;
	}

	public PatientBasic getPatientBasicInfo() {
		return patientBasicInfo;
	}

	public void setPatientBasicInfo(PatientBasic patientBasicInfo) {
		this.patientBasicInfo = patientBasicInfo;
	}

	public List<SignoutPage> getSignoutPages() {
		return signoutPages;
	}

	public void setSignoutPages(List<SignoutPage> signoutPages) {
		this.signoutPages = signoutPages;
	}

	public CoverPage getCoverPage() {
		return coverPage;
	}

	public void setCoverPage(CoverPage coverPage) {
		this.coverPage = coverPage;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public void replace(Object object) {
		Patient newPatient = (Patient) object;
		this.id = newPatient.id;		
		this.coverPage = newPatient.coverPage;		
		this.patientBasicInfo = newPatient.patientBasicInfo;		
		this.signoutPages = newPatient.signoutPages;
		this.timeDeleted = newPatient.timeDeleted;
	}
}
