package com.aham.signout.shared.messaging;


import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.util.Utility;

import android.view.View;
import android.widget.EditText;

public class CSFCxLab extends Lab {

	float value1;
	float value2;
	
	@Override
	public String[] getFieldsNames() {
		String[] values = {"Value1", "Value2"};
		return values;
	}
	@Override
	public String[] getFieldsValues() {
		String[] values = {stringOfFloat(value1), stringOfFloat(value2)};
		return values;
	}
	
	public float getValue1() {
		return value1;
	}
	public void setValue1(float value1) {
		this.value1 = value1;
	}
	public float getValue2() {
		return value2;
	}
	public void setValue2(float value2) {
		this.value2 = value2;
	}
	
	public  String getGroup(){
		return CSFCX;
	}
	@Override
	public void updateView(View view) {
		((EditText) view.findViewById(R.id.et_ucx_value1)).setText(value1 + "");
		((EditText) view.findViewById(R.id.et_ucx_value2)).setText(value2 + "");
	}
	@Override
	public void populateLab(View view) {
		value1 = Utility.parseFloat(((EditText) view.findViewById(R.id.et_ucx_value1)).getText().toString());
		value2 = Utility.parseFloat(((EditText) view.findViewById(R.id.et_ucx_value2)).getText().toString());
	}
	
	
	@Override
	public void replace(Object object) {
		CSFCxLab lab = (CSFCxLab) object;
		this.date = lab.date;
		this.deleted = lab.deleted;
		this.deleteDate = lab.deleteDate;
		this.value1= lab.value1;
		this.value2 = lab.value2;
	}

}
