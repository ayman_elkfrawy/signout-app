package com.aham.signout.shared.messaging;

import java.util.Date;


import com.aham.patientsignoutapp.component.DeletableItem;
import com.aham.patientsignoutapp.component.Replaceable;

public class Medication implements DeletableItem, Replaceable {

	long id;
	String group_;
	String name;
	String section;
	boolean deleted;
	Date deleteDate;
	Date createDate;
	
	public static final String FENGI = "FENGI";
	public static final String RESP = "Resp";
	public static final String CV = "CV";
	public static final String HEME_ONC = "Heme/Onc";
	public static final String ID = "ID";
	public static final String NEUROLOGY = "Neurology";

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getGroup() {
		return group_;
	}

	public void setGroup(String group) {
		this.group_ = group;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

	@Override
	public void replace(Object object) {
		Medication med = (Medication) object;
		this.deleted = med.deleted;
		this.deleteDate = med.deleteDate;
		this.group_ = med.group_;	
		this.name = med.name;
		this.section = med.section;
	}
	
	@Override
	public String toString() {
		return getName() + ", " + getGroup();
	}

}
