package com.aham.signout.shared.messaging;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class PatientBasic {
	long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	String edCourse;
	String firstName;
	String middleName;
	String lastName;
	Date birthDate;
	String gender;
	// Date and time this patient created
	Date hostingDate;

	List<String> pmh = new ArrayList<String>();
	List<String> pw = new ArrayList<String>();
	List<SimpleListItem> admittedFor = new ArrayList<SimpleListItem>();
	List<String> allergies = new ArrayList<String>();
	List<String> homeMedications = new ArrayList<String>();

	float weight;
	String weightScale;
	float height;
	String heightScale;
	float headCircumference;
	String circScale;
	
	// Admission Vitals
	float temp;
	float hr;
	float bp1;
	float bp2;
	float o2sat;

	List<String> admittingDiagnosis = new ArrayList<String>();

	// These two fields are not related just to this patient, but for
	// All patients in the database
	String pmd;
	String attendingOnService;


	public String getEdCourse() {
		return edCourse;
	}

	public void setEdCourse(String edCourse) {
		this.edCourse = edCourse;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getHostingDate() {
		return hostingDate;
	}

	public void setHostingDate(Date hostingDate) {
		this.hostingDate = hostingDate;
	}

	public List<String> getPmh() {
		return pmh;
	}

	public void setPmh(List<String> pmh) {
		this.pmh = pmh;
	}

	public List<String> getPw() {
		return pw;
	}

	public void setPw(List<String> pw) {
		this.pw = pw;
	}

	public List<SimpleListItem> getAdmittedFor() {
		return admittedFor;
	}

	public void setAdmittedFor(List<SimpleListItem> admittedFor) {
		this.admittedFor = admittedFor;
	}

	public List<String> getAllergies() {
		return allergies;
	}

	public void setAllergies(List<String> allergies) {
		this.allergies = allergies;
	}

	public List<String> getHomeMedications() {
		return homeMedications;
	}

	public void setHomeMedications(List<String> homeMedications) {
		this.homeMedications = homeMedications;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public String getWeightScale() {
		return weightScale;
	}

	public void setWeightScale(String weightScale) {
		this.weightScale = weightScale;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public float getHeadCircumference() {
		return headCircumference;
	}

	public void setHeadCircumference(float headCircumference) {
		this.headCircumference = headCircumference;
	}
	
	public String getHeightScale() {
		return heightScale;
	}

	public void setHeightScale(String heightScale) {
		this.heightScale = heightScale;
	}

	public String getCircScale() {
		return circScale;
	}

	public void setCircScale(String circScale) {
		this.circScale = circScale;
	}

	public float getTemp() {
		return temp;
	}

	public void setTemp(float temp) {
		this.temp = temp;
	}

	public float getHr() {
		return hr;
	}

	public void setHr(float hr) {
		this.hr = hr;
	}

	public float getBp1() {
		return bp1;
	}

	public void setBp1(float bp1) {
		this.bp1 = bp1;
	}

	public float getBp2() {
		return bp2;
	}

	public void setBp2(float bp2) {
		this.bp2 = bp2;
	}

	public float getO2sat() {
		return o2sat;
	}

	public void setO2sat(float o2sat) {
		this.o2sat = o2sat;
	}

	public List<String> getAdmittingDiagnosis() {
		return admittingDiagnosis;
	}

	public void setAdmittingDiagnosis(List<String> admittingDiagnosis) {
		this.admittingDiagnosis = admittingDiagnosis;
	}

	public String getPmd() {
		return pmd;
	}

	public  void setPmd(String pmd) {
		this.pmd = pmd;
	}

	public String getAttendingOnService() {
		return attendingOnService;
	}

	public  void setAttendingOnService(String attendingOnService) {
		this.attendingOnService = attendingOnService;
	}

}
