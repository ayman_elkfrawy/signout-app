package com.aham.signout.shared.messaging;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class SignoutPage {

	long id;

	// Final variables
	public static final String[] diets = { "NPO", "regular", "regular kosher", "clears", "NPO --> clear",
			"clears --> regular", "renal", "neutropenic", "calorie count" };
	public static final String[] ivf1List = { "none", "D5 1/2 NS", "D5 1/3 NS", "D5 1/4 NS", "LR", "NS" };
	public static final String[] ivf2List = { "none", "+ 10 KCl", "+ 20 KCl" };

	int doh; // Days of hosting

	// primary key
	Date pageDate;
	
	Date timeDeleted;

	// Vital section
	float rrMin, rrMax;
	float tempMin, tempMax;
	float hrMin, hrMax;
	float bpMin, bpMax;
	float o2satMin, o2satMax;

	// FENGI section
	String diet;
	float insOut1, insOut2;
	int hours;
	float uoNum, uoweight;
	int uoHour;
	String notes;
	String ivf1;
	String ivf2;
	float ivfNum;

	// Resp Section
	float nc;

	// CV section
	String access;
	String ekg;
	String echo;

	// Infectious Disease
	String bcx1, bcx2;
	Date bcx;
	String ucx1, ucx2;
	Date ucx;
	String wcx1, wcx2;
	Date wcx;
	String csfcx1, csfcx2;
	Date csfcx;
	String other;

	// Heme/Onc Section
	List<SimpleListItem> otherHemeOnc = new ArrayList<SimpleListItem>();

	// Neurology Section
	String veeg;

	List<Medication> allMeds = new ArrayList<Medication>();
	List<Lab> allLabs = new ArrayList<Lab>();
	List<Radiology> radiologies = new ArrayList<Radiology>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getDoh() {
		return doh;
	}

	public void setDoh(int doh) {
		this.doh = doh;
	}

	public Date getPageDate() {
		return pageDate;
	}

	public void setPageDate(Date pageDate) {
		this.pageDate = pageDate;
	}

	public float getRrMin() {
		return rrMin;
	}

	public void setRrMin(float rrMin) {
		this.rrMin = rrMin;
	}

	public float getRrMax() {
		return rrMax;
	}

	public void setRrMax(float rrMax) {
		this.rrMax = rrMax;
	}

	public float getTempMin() {
		return tempMin;
	}

	public void setTempMin(float tempMin) {
		this.tempMin = tempMin;
	}

	public float getTempMax() {
		return tempMax;
	}

	public void setTempMax(float tempMax) {
		this.tempMax = tempMax;
	}

	public float getHrMax() {
		return hrMax;
	}

	public void setHrMax(float hrMax) {
		this.hrMax = hrMax;
	}

	public float getBpMin() {
		return bpMin;
	}

	public void setBpMin(float bpMin) {
		this.bpMin = bpMin;
	}

	public float getHrMin() {
		return hrMin;
	}

	public void setHrMin(float hrMin) {
		this.hrMin = hrMin;
	}

	public float getBpMax() {
		return bpMax;
	}

	public void setBpMax(float bpMax) {
		this.bpMax = bpMax;
	}

	public float getO2satMin() {
		return o2satMin;
	}

	public void setO2satMin(float o2satMin) {
		this.o2satMin = o2satMin;
	}

	public float getO2satMax() {
		return o2satMax;
	}

	public void setO2satMax(float o2satMax) {
		this.o2satMax = o2satMax;
	}

	public String getDiet() {
		return diet;
	}

	public void setDiet(String diet) {
		this.diet = diet;
	}

	public float getInsOut1() {
		return insOut1;
	}

	public void setInsOut1(float insOut1) {
		this.insOut1 = insOut1;
	}

	public float getInsOut2() {
		return insOut2;
	}

	public void setInsOut2(float insOut2) {
		this.insOut2 = insOut2;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public float getUoNum() {
		return uoNum;
	}

	public void setUoNum(float uoNum) {
		this.uoNum = uoNum;
	}

	public int getUoHour() {
		return uoHour;
	}

	public void setUoHour(int uoHour) {
		this.uoHour = uoHour;
	}

	public float getUoweight() {
		return uoweight;
	}

	public void setUoweight(float uoweight) {
		this.uoweight = uoweight;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getIvf1() {
		return ivf1;
	}

	public void setIvf1(String ivf1) {
		this.ivf1 = ivf1;
	}

	public String getIvf2() {
		return ivf2;
	}

	public void setIvf2(String ivf2) {
		this.ivf2 = ivf2;
	}

	public float getIvfNum() {
		return ivfNum;
	}

	public void setIvfNum(float ivfNum) {
		this.ivfNum = ivfNum;
	}

	public float getNc() {
		return nc;
	}

	public void setNc(float nc) {
		this.nc = nc;
	}

	public String getAccess() {
		return access;
	}

	public void setAccess(String access) {
		this.access = access;
	}

	public String getEkg() {
		return ekg;
	}

	public void setEkg(String ekg) {
		this.ekg = ekg;
	}

	public String getEcho() {
		return echo;
	}

	public void setEcho(String echo) {
		this.echo = echo;
	}

	public String getBcx1() {
		return bcx1;
	}

	public void setBcx1(String bcx1) {
		this.bcx1 = bcx1;
	}

	public String getBcx2() {
		return bcx2;
	}

	public void setBcx2(String bcx2) {
		this.bcx2 = bcx2;
	}

	public Date getBcx() {
		return bcx;
	}

	public void setBcx(Date bcx) {
		this.bcx = bcx;
	}

	public String getUcx1() {
		return ucx1;
	}

	public void setUcx1(String ucx1) {
		this.ucx1 = ucx1;
	}

	public String getUcx2() {
		return ucx2;
	}

	public void setUcx2(String ucx2) {
		this.ucx2 = ucx2;
	}

	public Date getUcx() {
		return ucx;
	}

	public void setUcx(Date ucx) {
		this.ucx = ucx;
	}

	public String getWcx1() {
		return wcx1;
	}

	public void setWcx1(String wcx1) {
		this.wcx1 = wcx1;
	}

	public String getWcx2() {
		return wcx2;
	}

	public void setWcx2(String wcx2) {
		this.wcx2 = wcx2;
	}

	public Date getWcx() {
		return wcx;
	}

	public void setWcx(Date wcx) {
		this.wcx = wcx;
	}

	public String getCsfcx1() {
		return csfcx1;
	}

	public void setCsfcx1(String csfcx1) {
		this.csfcx1 = csfcx1;
	}

	public String getCsfcx2() {
		return csfcx2;
	}

	public void setCsfcx2(String csfcx2) {
		this.csfcx2 = csfcx2;
	}

	public Date getCsfcx() {
		return csfcx;
	}

	public void setCsfcx(Date csfcx) {
		this.csfcx = csfcx;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public List<SimpleListItem> getOtherHemeOnc() {
		return otherHemeOnc;
	}

	public void setOtherHemeOnc(List<SimpleListItem> otherHemeOnc) {
		this.otherHemeOnc = otherHemeOnc;
	}

	public String getVeeg() {
		return veeg;
	}

	public void setVeeg(String veeg) {
		this.veeg = veeg;
	}

	public List<Medication> getAllMeds() {
		return allMeds;
	}

	public void setAllMeds(List<Medication> allMeds) {
		this.allMeds = allMeds;
	}

	public List<Lab> getAllLabs() {
		return allLabs;
	}

	public void setAllLabs(List<Lab> allLabs) {
		this.allLabs = allLabs;
	}

	public List<Radiology> getRadiologies() {
		return radiologies;
	}

	public void setRadiologies(List<Radiology> radiologies) {
		this.radiologies = radiologies;
	}

	public Date getTimeDeleted() {
		return timeDeleted;
	}

	public void setTimeDeleted(Date timeDeleted) {
		this.timeDeleted = timeDeleted;
	}

	public List<Lab> getLabGroupList(String group) {
		List<Lab> list = new ArrayList<Lab>();
		for (Lab item : getAllLabs()) {
			if (item.getGroup() == group)
				list.add(item);
		}
		return list;
	}

	public void addLabList(List<Lab> list, String section) {
		for (Lab candiatedItem : list) {
			boolean isNew = true;
			for (Lab currentItem : getAllLabs()) {
				if (candiatedItem == currentItem) { // Reference the same object
					isNew = false;
					break;
				}
			}
			if (section != null)
				candiatedItem.setSection(section);
			if (isNew)
				getAllLabs().add(candiatedItem);
		}
	}

	public List<Medication> getMedsByGroup(String group) {
		List<Medication> list = new ArrayList<Medication>();
		for (Medication item : getAllMeds()) {
			if (group != null && group.equalsIgnoreCase(item.getGroup()))
				list.add(item);
		}
		return list;
	}

	public List<Medication> getMedsBySection(String section) {
		List<Medication> list = new ArrayList<Medication>();
		for (Medication item : getAllMeds()) {
			if (section != null && section.equalsIgnoreCase(item.getSection()))
				list.add(item);
		}
		return list;
	}

	public void addMedList(List<Medication> list, String medSection) {
		for (Medication candiatedItem : list) {
			boolean isNew = true;
			for (Medication currentItem : getAllMeds()) {
				if (candiatedItem == currentItem) { // Reference the same object
					isNew = false;
					break;
				}
			}
			if (medSection != null)
				candiatedItem.setSection(medSection);
			if (isNew)
				getAllMeds().add(candiatedItem);
		}
	}

	public List<Lab> getLabsBySection(String section) {
		List<Lab> labs = new ArrayList<Lab>();
		for (Lab lab : allLabs) {
			if (lab.getSection() != null && lab.getSection().equalsIgnoreCase(section))
				labs.add(lab);
		}
		return labs;
	}
	
	
}
