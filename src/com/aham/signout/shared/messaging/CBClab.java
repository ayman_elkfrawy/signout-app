package com.aham.signout.shared.messaging;


import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.util.Utility;

import android.view.View;
import android.widget.EditText;

public class CBClab extends Lab {
	
	float wbc;
	float hg;
	float hct;
	float plt;
	float n;
	float b;
	float l;
	float m;
	float e;
	float mcv;
	float rdw;
	
	@Override
	public String[] getFieldsNames() {
		String[] values = {"WBC", "Hg", "HCt", "Plt", "RDW", "MCV", "N", "B", "L", "M", "E"};
		return values;
	}
	@Override
	public String[] getFieldsValues() {
		String[] values = {stringOfFloat(wbc), stringOfFloat(hg), stringOfFloat(hct), stringOfFloat(plt), stringOfFloat(rdw),
				stringOfFloat(mcv), stringOfFloat(n), stringOfFloat(b), stringOfFloat(l), stringOfFloat(m),stringOfFloat(e)  };
		return values;
	}
	
	public float getWbc() {
		return wbc;
	}
	public void setWbc(float wbc) {
		this.wbc = wbc;
	}
	public String getGroup(){
		return CBC;
	}
	
	public float getHg() {
		return hg;
	}
	public void setHg(float hg) {
		this.hg = hg;
	}
	public float getHct() {
		return hct;
	}
	public void setHct(float hct) {
		this.hct = hct;
	}
	public float getPlt() {
		return plt;
	}
	public void setPlt(float plt) {
		this.plt = plt;
	}
	public float getN() {
		return n;
	}
	public void setN(float n) {
		this.n = n;
	}
	public float getB() {
		return b;
	}
	public void setB(float b) {
		this.b = b;
	}
	public float getL() {
		return l;
	}
	public void setL(float l) {
		this.l = l;
	}
	public float getM() {
		return m;
	}
	public void setM(float m) {
		this.m = m;
	}
	public float getE() {
		return e;
	}
	public void setE(float e) {
		this.e = e;
	}
	public float getMcv() {
		return mcv;
	}
	public void setMcv(float mcv) {
		this.mcv = mcv;
	}
	public float getRdw() {
		return rdw;
	}
	public void setRdw(float rdw) {
		this.rdw = rdw;
	}
	@Override
	public void updateView(View view) {
		((EditText) view.findViewById(R.id.et_cbc_b)).setText(Utility.numberToString(b));
		((EditText) view.findViewById(R.id.et_cbc_e)).setText(Utility.numberToString(e));
		((EditText) view.findViewById(R.id.et_cbc_hct)).setText(Utility.numberToString(hct));
		((EditText) view.findViewById(R.id.et_cbc_hg)).setText(Utility.numberToString(hg ));
		((EditText) view.findViewById(R.id.et_cbc_l)).setText(Utility.numberToString(l));
		((EditText) view.findViewById(R.id.et_cbc_m)).setText(Utility.numberToString(m));
		((EditText) view.findViewById(R.id.et_cbc_mcv)).setText(Utility.numberToString(mcv));
		((EditText) view.findViewById(R.id.et_cbc_n)).setText(Utility.numberToString(n ));
		((EditText) view.findViewById(R.id.et_cbc_plt)).setText(Utility.numberToString(plt));
		((EditText) view.findViewById(R.id.et_cbc_rdw)).setText(Utility.numberToString(rdw));
		((EditText) view.findViewById(R.id.et_cbc_wbc)).setText(Utility.numberToString(wbc));
	}
	@Override
	public void populateLab(View view) {
		b = Utility.parseFloat(((EditText) view.findViewById(R.id.et_cbc_b)).getText().toString());
		wbc = Utility.parseFloat(((EditText) view.findViewById(R.id.et_cbc_wbc)).getText().toString());
		plt = Utility.parseFloat(((EditText) view.findViewById(R.id.et_cbc_plt)).getText().toString());
		n = Utility.parseFloat(((EditText) view.findViewById(R.id.et_cbc_n)).getText().toString());
		rdw = Utility.parseFloat(((EditText) view.findViewById(R.id.et_cbc_rdw)).getText().toString());
		mcv  = Utility.parseFloat(((EditText) view.findViewById(R.id.et_cbc_mcv)).getText().toString());
		m = Utility.parseFloat(((EditText) view.findViewById(R.id.et_cbc_m)).getText().toString());
		l = Utility.parseFloat(((EditText) view.findViewById(R.id.et_cbc_l)).getText().toString());
		hg = Utility.parseFloat(((EditText) view.findViewById(R.id.et_cbc_hg)).getText().toString());
		hct = Utility.parseFloat(((EditText) view.findViewById(R.id.et_cbc_hct)).getText().toString());
		e = Utility.parseFloat(((EditText) view.findViewById(R.id.et_cbc_e)).getText().toString());
	}
	
	@Override
	public void replace(Object object) {
		CBClab lab = (CBClab) object;
		this.date = lab.date;
		this.deleted = lab.deleted;
		this.deleteDate = lab.deleteDate;
		this.b = lab.b;
		this.wbc = lab.wbc;
		this.plt = lab.plt;
		this.n = lab.n;
		this.rdw = lab.rdw;
		this.mcv = lab.mcv;
		this.m = lab.m;
		this.l = lab.l;
		this.hg = lab.hg;
		this.hct = lab.hct;
		this.e = lab.e;
	}
	
	
}
