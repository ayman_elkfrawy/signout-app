package com.aham.signout.shared.messaging;


import android.view.View;
import android.widget.EditText;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.util.Utility;

public class LFTlab extends Lab {

	float tp;
	float ap;
	float alb;
	float ast;
	float tbil;
	float alt;
	float ldh;
	
	@Override
	public String[] getFieldsNames() {
		String[] values = {"TP", "AP", "ALB", "AST", "TBIL", "ALT", "LDH"};
		return values;
	}
	@Override
	public String[] getFieldsValues() {
		String[] values = {stringOfFloat(tp), stringOfFloat(ap), stringOfFloat(alb), stringOfFloat(tbil),
				stringOfFloat(alt), stringOfFloat(ldh) };
		return values;
	}

	public float getTp() {
		return tp;
	}

	public String getGroup() {
		return LFT;
	}

	public void setTp(float tp) {
		this.tp = tp;
	}

	public float getAp() {
		return ap;
	}

	public void setAp(float ap) {
		this.ap = ap;
	}

	public float getAlb() {
		return alb;
	}

	public void setAlb(float alb) {
		this.alb = alb;
	}

	public float getAst() {
		return ast;
	}

	public void setAst(float ast) {
		this.ast = ast;
	}

	public float getTbil() {
		return tbil;
	}

	public void setTbil(float tbil) {
		this.tbil = tbil;
	}

	public float getAlt() {
		return alt;
	}

	public void setAlt(float alt) {
		this.alt = alt;
	}

	public float getLdh() {
		return ldh;
	}

	public void setLdh(float ldh) {
		this.ldh = ldh;
	}

	@Override
	public void updateView(View view) {
		((EditText) view.findViewById(R.id.et_lft_alb)).setText(Utility.numberToString(alb));
		((EditText) view.findViewById(R.id.et_lft_alt)).setText(Utility.numberToString(alt));
		((EditText) view.findViewById(R.id.et_lft_ap)).setText(Utility.numberToString(ap));
		((EditText) view.findViewById(R.id.et_lft_ast)).setText(Utility.numberToString(ast));
		((EditText) view.findViewById(R.id.et_lft_ldh)).setText(Utility.numberToString(ldh));
		((EditText) view.findViewById(R.id.et_lft_tbil)).setText(Utility.numberToString(tbil));
		((EditText) view.findViewById(R.id.et_lft_tp)).setText(Utility.numberToString(tp));
	}

	@Override
	public void populateLab(View view) {
		alb = Utility.parseFloat(((EditText) view.findViewById(R.id.et_lft_alb)).getText().toString());
		alt = Utility.parseFloat(((EditText) view.findViewById(R.id.et_lft_alt)).getText().toString());
		ap = Utility.parseFloat(((EditText) view.findViewById(R.id.et_lft_ap)).getText().toString());
		ast = Utility.parseFloat(((EditText) view.findViewById(R.id.et_lft_ast)).getText().toString());
		ldh = Utility.parseFloat(((EditText) view.findViewById(R.id.et_lft_ldh)).getText().toString());
		tbil = Utility.parseFloat(((EditText) view.findViewById(R.id.et_lft_tbil)).getText().toString());
		tp = Utility.parseFloat(((EditText) view.findViewById(R.id.et_lft_tp)).getText().toString());
	}
	
	
	@Override
	public void replace(Object object) {
		LFTlab lab = (LFTlab) object;
		this.date = lab.date;
		this.deleted = lab.deleted;
		this.deleteDate = lab.deleteDate;
		this.alb = lab.alb;
		this.alt = lab.alt;
		this.ap = lab.ap;
		this.ast = lab.ast;
		this.ldh = lab.ldh;
		this.tbil = lab.tbil;
		this.tp = lab.tp;
	}
}
