package com.aham.signout.shared.messaging;


import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.util.Utility;

import android.view.View;
import android.widget.EditText;

public class CoagultionLab extends Lab {
	
	float pt;
	float ptt;
	float inr;
	
	@Override
	public String[] getFieldsNames() {
		String[] values = {"PT", "PTT", "INR"};
		return values;
	}
	@Override
	public String[] getFieldsValues() {
		String[] values = {stringOfFloat(pt), stringOfFloat(ptt), stringOfFloat(inr)};
		return values;
	}
	
	public float getPt() {
		return pt;
	}
	public void setPt(float pt) {
		this.pt = pt;
	}
	public float getPtt() {
		return ptt;
	}
	public void setPtt(float ptt) {
		this.ptt = ptt;
	}
	public float getInr() {
		return inr;
	}
	public void setInr(float inr) {
		this.inr = inr;
	}
	public String getGroup(){
		return COAGULTION;
	}
	@Override
	public void updateView(View view) {
		((EditText) view.findViewById(R.id.et_coag_inr)).setText(Utility.numberToString(inr));
		((EditText) view.findViewById(R.id.et_coag_pt)).setText(Utility.numberToString(pt));
		((EditText) view.findViewById(R.id.et_coag_ptt)).setText(Utility.numberToString(ptt));
	}
	@Override
	public void populateLab(View view) {
		inr = Utility.parseFloat(((EditText) view.findViewById(R.id.et_coag_inr)).getText().toString());
		pt = Utility.parseFloat(((EditText) view.findViewById(R.id.et_coag_pt)).getText().toString());
		ptt = Utility.parseFloat(((EditText) view.findViewById(R.id.et_coag_ptt)).getText().toString());
	}
	
	@Override
	public void replace(Object object) {
		CoagultionLab lab = (CoagultionLab) object;
		this.date = lab.date;
		this.deleted = lab.deleted;
		this.deleteDate = lab.deleteDate;
		this.inr = lab.inr;
		this.pt = lab.pt;
		this.ptt = lab.ptt;
	}

	
	
}
