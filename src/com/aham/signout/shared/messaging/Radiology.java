package com.aham.signout.shared.messaging;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.aham.patientsignoutapp.component.DeletableItem;
import com.aham.patientsignoutapp.component.Replaceable;
import com.aham.patientsignoutapp.util.Utility;

public class Radiology implements DeletableItem, Replaceable {

	// Radiology groups
	public static final String CXR = "CXR";
	public static final String KUB = "KUB";
	public static final String OBS_SERIES = "Obstructive Series";
	public static final String US_KIDNEY = "U/S Kidney";
	public static final String US_ABD = "U/S Abd";
	public static final String US_TESTES = " U/S Testes/Scrotum";
	public static final String CT_HEAD = "CT Head";
	public static final String CT_ABD = "CT Abd";
	public static final String MRI_HEAD = "MRI Head";
	public static final String OTHER = "Other";

	long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	String name;
	String result;
	Date date;
	boolean deleted;
	Date deleteDate;
	Date createDate;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Override
	public String toString() {
		return getName() + "\t " + Utility.formatDate(getDate()) ;
	}

	@Override
	public void replace(Object object) {
		Radiology rad = (Radiology) object;
		this.date = rad.date;
		this.deleted = rad.deleted;
		this.deleteDate = rad.deleteDate;
		this.name = rad.name;
		this.result = rad.result;
	}
}
