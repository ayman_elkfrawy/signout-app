package com.aham.signout.shared.messaging;

public class User {
        public final static String DOCTOR = "Doctor";
        public final static String ADMIN = "Administrator";
        
        int id;
        String firstName;
        String lastName;
        String username;
        String password;
        String type;
        
        public User(int id, String firstName, String lastName, String username, String password, String type) {
                super();
                this.id = id;
                this.firstName = firstName;
                this.lastName = lastName;
                this.username = username;
                this.password = password;
                this.type = type;
        }
        
        // Empty constructor
        public User() {
        }
        
        public int getId() {
                return id;
        }
        public void setId(int id) {
                this.id = id;
        }
        public String getFirstName() {
                return firstName;
        }
        public void setFirstName(String firstName) {
                this.firstName = firstName;
        }
        public String getLastName() {
                return lastName;
        }
        public void setLastName(String lastName) {
                this.lastName = lastName;
        }
        public String getUsername() {
                return username;
        }
        public void setUsername(String username) {
                this.username = username;
        }
        public String getPassword() {
                return password;
        }
        public void setPassword(String password) {
                this.password = password;
        }
        public String getType() {
                return type;
        }
        public void setType(String type) {
                this.type = type;
        }
        
        @Override
        public boolean equals(Object o) {
                if (o instanceof User) {
                        return ((User) o).getId() == id;
                } else return super.equals(o);
        }

        public void copyFromUser(User tempUser) {
                id = tempUser.getId();
                firstName = tempUser.getFirstName();
                lastName = tempUser.getLastName();
                username = tempUser.getUsername();
                password = tempUser.getPassword();
                type = tempUser.getType();
        }
}