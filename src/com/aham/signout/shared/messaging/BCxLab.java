package com.aham.signout.shared.messaging;


import com.aham.patientsignoutapp.R;

import android.view.View;
import android.widget.Spinner;

public class BCxLab extends Lab {

	static final String[] values = { "SENT", "WOT-SENT" };

	String currentValue;
	
	@Override
	public String[] getFieldsNames() {
		String[] values = {"Value"};
		return values;
	}
	@Override
	public String[] getFieldsValues() {
		String[] values = {currentValue};
		return values;
	}

	public String[] getValues() {
		return values;
	}

	public String getCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(String currentValue) {
		this.currentValue = currentValue;
	}

	public String getGroup() {
		return BCX;
	}

	@Override
	public void updateView(View view) {
		if (currentValue != null) {
			((Spinner) view.findViewById(R.id.sp_bcx)).setSelection(currentValue.equalsIgnoreCase(values[0]) ? 0 : 1);
		}
	}

	@Override
	public void populateLab(View view) {
		currentValue = ((Spinner) view.findViewById(R.id.sp_bcx)).getSelectedItem().toString();
	}

	@Override
	public void replace(Object object) {
		BCxLab lab = (BCxLab) object;
		this.date = lab.date;
		this.deleted = lab.deleted;
		this.deleteDate = lab.deleteDate;
		this.currentValue = lab.currentValue;
	}

}
