package com.aham.signout.shared.messaging;

import java.util.Date;

import com.aham.patientsignoutapp.component.DeletableItem;
import com.aham.patientsignoutapp.component.Replaceable;

public class SimpleListItem implements DeletableItem, Replaceable , Cloneable{
	
	long id;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	
	boolean deleted;
	Date deleteDate;
	Date createDate;
	String text;
	String section; 
	
	
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getDeleteDate() {
		return deleteDate;
	}
	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	@Override
	public void replace(Object object) {
		SimpleListItem item = (SimpleListItem) object;
		this.deleted = item.deleted;
		this.deleteDate = item.deleteDate;
		this.text = item.text;
	}
	
	@Override
	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public String toString() {
		return text;
	}
	
}
