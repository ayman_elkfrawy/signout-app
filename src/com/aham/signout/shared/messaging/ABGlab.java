package com.aham.signout.shared.messaging;


import android.view.View;
import android.widget.EditText;

import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.util.Utility;

public class ABGlab extends Lab {
	float ph;
	float pco2;
	float po2;
	float sat;
	float be;
	float hco3;
	float f1o2;
	
	@Override
	public String[] getFieldsNames() {
		String[] values = {"PH", "PCO2", "PO2", "Sat", "BE", "HCO3", "F1O2"};
		return values;
	}
	@Override
	public String[] getFieldsValues() {
		String[] values = {stringOfFloat(ph), stringOfFloat(pco2), stringOfFloat(po2), stringOfFloat(sat),
				stringOfFloat(be), stringOfFloat(hco3), stringOfFloat(f1o2) };
		return values;
	}
	
	public float getPh() {
		return ph;
	}
	public String getGroup(){
		return ABG;
	}
	
	public void setPh(float ph) {
		this.ph = ph;
	}
	public float getPco2() {
		return pco2;
	}
	public void setPco2(float pco2) {
		this.pco2 = pco2;
	}
	public float getPo2() {
		return po2;
	}
	public void setPo2(float po2) {
		this.po2 = po2;
	}
	public float getSat() {
		return sat;
	}
	public void setSat(float sat) {
		this.sat = sat;
	}
	public float getBe() {
		return be;
	}
	public void setBe(float be) {
		this.be = be;
	}
	public float getHco3() {
		return hco3;
	}
	public void setHco3(float hco3) {
		this.hco3 = hco3;
	}
	public float getF1o2() {
		return f1o2;
	}
	public void setF1o2(float f1o2) {
		this.f1o2 = f1o2;
	}
	
	@Override
	public void updateView(View view) {
		((EditText) view.findViewById(R.id.et_abg_be)).setText(Utility.numberToString(be));
		((EditText) view.findViewById(R.id.et_abg_f1o2)).setText(Utility.numberToString(f1o2));
		((EditText) view.findViewById(R.id.et_abg_hco3)).setText(Utility.numberToString(hco3));
		((EditText) view.findViewById(R.id.et_abg_pco2)).setText(Utility.numberToString(pco2));
		((EditText) view.findViewById(R.id.et_abg_ph)).setText(Utility.numberToString(ph));
		((EditText) view.findViewById(R.id.et_abg_po2)).setText(Utility.numberToString(po2));
		((EditText) view.findViewById(R.id.et_abg_sat)).setText(Utility.numberToString(sat));
	}
	
	@Override
	public void populateLab(View view) {
		be = Utility.parseFloat(((EditText) view.findViewById(R.id.et_abg_be)).getText().toString());
		f1o2= Utility.parseFloat(((EditText) view.findViewById(R.id.et_abg_f1o2)).getText().toString());
		hco3= Utility.parseFloat(((EditText) view.findViewById(R.id.et_abg_hco3)).getText().toString());
		pco2 = Utility.parseFloat(((EditText) view.findViewById(R.id.et_abg_pco2)).getText().toString());
		ph = Utility.parseFloat(((EditText) view.findViewById(R.id.et_abg_ph)).getText().toString());
		po2= Utility.parseFloat(((EditText) view.findViewById(R.id.et_abg_po2)).getText().toString());
		sat = Utility.parseFloat(((EditText) view.findViewById(R.id.et_abg_sat)).getText().toString());
	}

	@Override
	public void replace(Object object) {
		ABGlab lab = (ABGlab) object;
		this.date = lab.date;
		this.deleted = lab.deleted;
		this.deleteDate = lab.deleteDate;
		this.be = lab.be;
		this.f1o2 = lab.f1o2;
		this.hco3 = lab.hco3;
		this.pco2 = lab.pco2;
		this.ph = lab.ph;
		this.po2 = lab.po2;
		this.sat = lab.sat;
	}
	
}
