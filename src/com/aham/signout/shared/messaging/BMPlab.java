package com.aham.signout.shared.messaging;


import com.aham.patientsignoutapp.R;
import com.aham.patientsignoutapp.util.Utility;

import android.view.View;
import android.widget.EditText;

public class BMPlab extends Lab {

	float na;
	float cl;
	float bun;
	float hco3;
	float cr;
	float gl;
	float ca;
	float mg;
	float po4;
	float k;
	
	@Override
	public String[] getFieldsNames() {
		String[] values = {"Na", "Cl", "Bun", "HCO3", "Cr", "Gl", "Ca", "mg", "PO4", "K"};
		return values;
	}
	@Override
	public String[] getFieldsValues() {
		String[] values = {stringOfFloat(na), stringOfFloat(cl), stringOfFloat(bun), stringOfFloat(hco3), stringOfFloat(cr),
				stringOfFloat(gl), stringOfFloat(ca), stringOfFloat(mg), stringOfFloat(po4), stringOfFloat(k)  };
		return values;
	}

	public float getNa() {
		return na;
	}

	public void setNa(float na) {
		this.na = na;
	}

	public float getCl() {
		return cl;
	}

	public String getGroup() {
		return BMP;
	}

	public void setCl(float cl) {
		this.cl = cl;
	}

	public float getBun() {
		return bun;
	}

	public void setBun(float bun) {
		this.bun = bun;
	}

	public float getHco3() {
		return hco3;
	}

	public void setHco3(float hco3) {
		this.hco3 = hco3;
	}

	public float getCr() {
		return cr;
	}

	public void setCr(float cr) {
		this.cr = cr;
	}

	public float getGl() {
		return gl;
	}

	public void setGl(float gi) {
		this.gl = gi;
	}

	public float getCa() {
		return ca;
	}

	public void setCa(float ca) {
		this.ca = ca;
	}

	public float getMg() {
		return mg;
	}

	public void setMg(float mg) {
		this.mg = mg;
	}

	public float getPo4() {
		return po4;
	}

	public void setPo4(float po4) {
		this.po4 = po4;
	}

	public float getK() {
		return k;
	}

	public void setK(float k) {
		this.k = k;
	}

	@Override
	public void updateView(View view) {
		((EditText) view.findViewById(R.id.et_bmp_bun)).setText(Utility.numberToString(bun));
		((EditText) view.findViewById(R.id.et_bmp_ca)).setText(Utility.numberToString(ca));
		((EditText) view.findViewById(R.id.et_bmp_cl)).setText(Utility.numberToString(hco3));
		((EditText) view.findViewById(R.id.et_bmp_cr)).setText(Utility.numberToString(cr));
		((EditText) view.findViewById(R.id.et_bmp_gl)).setText(Utility.numberToString(gl));
		((EditText) view.findViewById(R.id.et_bmp_hco3)).setText(Utility.numberToString(hco3));
		((EditText) view.findViewById(R.id.et_bmp_k)).setText(Utility.numberToString(k));
		((EditText) view.findViewById(R.id.et_bmp_mg)).setText(Utility.numberToString(mg));
		((EditText) view.findViewById(R.id.et_bmp_na)).setText(Utility.numberToString(na));
		((EditText) view.findViewById(R.id.et_bmp_po4)).setText(Utility.numberToString(po4));
	}

	@Override
	public void populateLab(View view) {
		bun = Utility.parseFloat(((EditText) view.findViewById(R.id.et_bmp_bun)).getText().toString());
		ca = Utility.parseFloat(((EditText) view.findViewById(R.id.et_bmp_ca)).getText().toString());
		cl = Utility.parseFloat(((EditText) view.findViewById(R.id.et_bmp_cl)).getText().toString());
		cr = Utility.parseFloat(((EditText) view.findViewById(R.id.et_bmp_cr)).getText().toString());
		gl = Utility.parseFloat(((EditText) view.findViewById(R.id.et_bmp_gl)).getText().toString());
		hco3 = Utility.parseFloat(((EditText) view.findViewById(R.id.et_bmp_hco3)).getText().toString());
		k = Utility.parseFloat(((EditText) view.findViewById(R.id.et_bmp_k)).getText().toString());
		mg = Utility.parseFloat(((EditText) view.findViewById(R.id.et_bmp_mg)).getText().toString());
		na = Utility.parseFloat(((EditText) view.findViewById(R.id.et_bmp_na)).getText().toString());
		po4 = Utility.parseFloat(((EditText) view.findViewById(R.id.et_bmp_po4)).getText().toString());
	}

	@Override
	public void replace(Object object) {
		BMPlab lab = (BMPlab) object;
		this.date = lab.date;
		this.deleted = lab.deleted;
		this.deleteDate = lab.deleteDate;
		this.bun = lab.bun;
		this.ca = lab.ca;
		this.cl = lab.cl;
		this.cr = lab.cr;
		this.gl = lab.gl;
		this.hco3 = lab.hco3;
		this.k = lab.k;
		this.mg = lab.mg;
		this.na = lab.na;
		this.po4 = lab.po4;
	}

}
