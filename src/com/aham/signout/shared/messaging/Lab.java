package com.aham.signout.shared.messaging;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Date;
import java.util.List;


import com.aham.patientsignoutapp.component.DeletableItem;
import com.aham.patientsignoutapp.component.Replaceable;
import com.aham.patientsignoutapp.util.Utility;
import com.google.gson.reflect.TypeToken;

import android.view.View;

public abstract class Lab implements DeletableItem, Replaceable{

	long id;

	Date date;
	boolean deleted;
	Date deleteDate;
	Date createDate;
	String section;

	final public static String ABG = "ABG";
	final public static String BCX = "BCx";
	final public static String BMP = "BMP";
	final public static String CBC = "CBC";
	final public static String COAGULTION = "Coagultion";
	final public static String CSFCX = "CSFCx";
	final public static String LFT = "LFT";
	final public static String UCX = "UCx";
	final public static String OTHER = "Other";

	///////////**** Removed the static keyword and its implementation
	public abstract String getGroup();
	
	public abstract String[] getFieldsNames();
	public abstract String[] getFieldsValues();
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Override
	public String toString() {
		return getGroup() + "\t " + Utility.formatDate(getDate());
	}
	
	public String stringOfFloat(float field) {
		return field + "";
	}
	
	public abstract void updateView(View view);
	public abstract void populateLab(View view);

	public static Type getLabTypeCollection(String requiredLab) {
		Type collectionType = null;
		if (requiredLab.equalsIgnoreCase(ABG)) {
			collectionType = new TypeToken<Collection<ABGlab>>() { }.getType();
		} else if (requiredLab.equalsIgnoreCase(BCX)) {
			collectionType = new TypeToken<Collection<BCxLab>>() { }.getType();
		} else if (requiredLab.equalsIgnoreCase(BMP)) {
			collectionType = new TypeToken<Collection<BMPlab>>() { }.getType();
		} else if (requiredLab.equalsIgnoreCase(CBC)) {
			collectionType = new TypeToken<Collection<CBClab>>() { }.getType();
		} else if (requiredLab.equalsIgnoreCase(COAGULTION)) {
			collectionType = new TypeToken<Collection<CoagultionLab>>() { }.getType();
		} else if (requiredLab.equalsIgnoreCase(CSFCX)) {
			collectionType = new TypeToken<Collection<CSFCxLab>>() { }.getType();
		} else if (requiredLab.equalsIgnoreCase(LFT)) {
			collectionType = new TypeToken<Collection<LFTlab>>() { }.getType();
		} else if (requiredLab.equalsIgnoreCase(UCX)) {
			collectionType = new TypeToken<Collection<UCxLab>>() { }.getType();
		} else if (requiredLab.equalsIgnoreCase(OTHER)) {
			collectionType = new TypeToken<Collection<OtherLab>>() { }.getType();
		}
		
		return collectionType;
	}

}
