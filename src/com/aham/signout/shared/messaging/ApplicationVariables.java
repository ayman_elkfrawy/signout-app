package com.aham.signout.shared.messaging;

import java.util.ArrayList;
import java.util.List;

public class ApplicationVariables {
	List<String> pmd = new ArrayList<String>(); 
	List<String> attendingOnService = new ArrayList<String>();
	
	public List<String> getPmd() {
		return pmd;
	}
	public void setPmd(List<String> pmd) {
		this.pmd = pmd;
	}
	public List<String> getAttendingOnService() {
		return attendingOnService;
	}
	public void setAttendingOnService(List<String> attendingOnService) {
		this.attendingOnService = attendingOnService;
	}
}
