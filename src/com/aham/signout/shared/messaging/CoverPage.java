package com.aham.signout.shared.messaging;

import java.util.ArrayList;
import java.util.List;

public class CoverPage {
	long id;
	
	String hosbitalCourse;
	
	public static final String PROBLEMS_LIST = "Problems";
	public static final String ON_EVENTS = "O/N Events";
	public static final String TODO_LIST = "To Do List";
	public static final String CONTINGENCY_PLAN = "Contingency Plan";
	
	
	List<SimpleListItem> allLists = new ArrayList<SimpleListItem>();
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getHosbitalCourse() {
		return hosbitalCourse;
	}
	public void setHosbitalCourse(String hosbitalCourse) {
		this.hosbitalCourse = hosbitalCourse;
	}
	public List<SimpleListItem> getProblems() {
		return getSectionList(PROBLEMS_LIST);
	}
	public void setProblems(List<SimpleListItem> problems) {
		addSectionList(problems, PROBLEMS_LIST);
	}
	public List<SimpleListItem> getOnEvents() {
		return getSectionList(ON_EVENTS);
	}
	public void setOnEvents(List<SimpleListItem> onEvents) {
		addSectionList(onEvents, ON_EVENTS);
	}
	public List<SimpleListItem> getToDoList() {
		return getSectionList(TODO_LIST);
	}
	public void setToDoList(List<SimpleListItem> toDoList) {
		addSectionList(toDoList, TODO_LIST);
	}
	public List<SimpleListItem> getContingencyPlan() {
		return getSectionList(CONTINGENCY_PLAN);
	}
	public void setContingencyPlan(List<SimpleListItem> contingencyPlan) {
		addSectionList(contingencyPlan, CONTINGENCY_PLAN);
	}
	
	private List<SimpleListItem> getSectionList(String section) {
		List<SimpleListItem> list = new ArrayList<SimpleListItem>();
		for(SimpleListItem item: allLists) {
			if (item.getSection() != null && item.getSection().equalsIgnoreCase(section)) 
				list.add(item);
		}
		return list;
	}
	
	private void addSectionList(List<SimpleListItem> list, String section) {
		for(SimpleListItem candiatedItem: list) {
			boolean isNew = true;
			for(SimpleListItem currentItem: allLists) {
				if(candiatedItem == currentItem) { // Reference the same object
					isNew = false;
					break;
				}
			}
			candiatedItem.setSection(section);
			if(isNew) allLists.add(candiatedItem);
		}
	}
}
